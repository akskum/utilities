#!/usr/bin/python

import sys
from random import seed
from random import random
import json


# get nLevels as input
nLevels=int(sys.argv[1])
print(nLevels)

# seed random number generator
seed(1)

# create a json file 

data = []
data.append({
    "ColorSpace" : "Step",
    "Name" : "Random_{}".format(nLevels),
    "NanColor" : [0.97647058823529409, 0.51372549019607838, 0.14117647058823529],
    "RGBPoints" : []
    })

for i in range(nLevels+1):
    j=float(i)/nLevels
    data[0]["RGBPoints"].append(j)
    value = random()
    data[0]["RGBPoints"].append(value)
    value = random()
    data[0]["RGBPoints"].append(value)
    value = random()
    data[0]["RGBPoints"].append(value)

with open("random_{}.json".format(nLevels), 'w', newline='\n') as outfile:
    json.dump(data, outfile, separators=(',', ':'))
