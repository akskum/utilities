import os

from jinja2 import Environment, FileSystemLoader, StrictUndefined, Template

cwd = os.getcwd()

env = Environment(
    loader=FileSystemLoader(cwd, encoding="utf8"),
    undefined=StrictUndefined,
    trim_blocks=True,
    lstrip_blocks=True,
)
