#!/bin/bash

# helps with cli
. /home/akucwh/git/utilities/scripts/bash/bash_scripting_functions.sh

add_to_help_options "-append" "-a" "" "Append to existing vba_array_list_file"
add_to_help_options "-new" "-n" "" "Create a new vba_array_list_file (default)"
parse_for_help

# populate default vars
overwrite="True"
arglist=""

# parse options
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    -n | -new)
        overwrite="True"
        ;;
    -a | -append)
        overwrite="False"
        ;;
    *)
        arglist+=" $1" 
        ;;
    esac
    shift
done

# [[ $CONDA_DEFAULT_ENV == "data_analysis" ]] || die "not sourced correct conda environment"

file_to_write_to="/nsa/fluidcfd/04_Personal/$USER/99_temp/powerpointVBACaseDirInput.dat"

str_to_echo=$(/home/akucwh/git/utilities/scripts/vba/generate_ppt_array.py $arglist)
[[ $overwrite == "True" ]] && echo "$str_to_echo" | tee $file_to_write_to
[[ $overwrite == "False" ]] && echo "$str_to_echo" | tee -a $file_to_write_to

echo "Wrote to $file_to_write_to"
# $EDITOR $file_to_write_to
