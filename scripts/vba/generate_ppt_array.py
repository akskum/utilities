#!/home/akucwh/git/dotfiles/setup_scripts/pypoetry_doe_toolkit/.venv/bin/python
# This scipt returns the %eval_ids by appling constraint

import os
import sys
from pathlib import Path

import click
import numpy as np
import pandas as pd


@click.command()
@click.argument("datfile", type=click.Path())
# @click.argument("root_dir", type=click.Path())
@click.option("--MB", "-mb", type=float)
@click.option("--equals-constraint", "-e", type=(str, float), multiple=True)
@click.option("--match-constraint", "-m", type=(str, str), multiple=True)
@click.option(
    "--greater-than-equals-constraint", "-ge", type=(str, float), multiple=True
)
@click.option(
    "--lesser-than-equals-constraint", "-le", type=(str, float), multiple=True
)
@click.option("--ascending", "-asc", type=str, multiple=True)
@click.option("--descending", "-desc", type=str, multiple=True)
@click.option("--reverse", "-r", is_flag=True, default=False)
@click.option(
    "--list-cols", "-l", is_flag=True, default=False, help="list available columns"
)
@click.option("--skip", "-skip", type=int)
@click.option("--linspace", "-ls", type=int)
@click.option("--tail", "-tail", type=int)
@click.option("--head", "-head", type=int)
@click.option("--label", "-label", type=str)
@click.option(
    "--annotate-mb", "-a-mb", is_flag=True, default=False, help="annotate with mb"
)
def main(
    datfile,
    # root_dir,
    mb,
    equals_constraint,
    match_constraint,
    greater_than_equals_constraint,
    lesser_than_equals_constraint,
    ascending,
    descending,
    reverse,
    list_cols,
    skip,
    linspace,
    tail,
    head,
    label,
    annotate_mb,
):

    relative_datfile_path = Path(datfile)
    absolute_datfile_path = relative_datfile_path.absolute().resolve()

    df_data = pd.read_csv(datfile, delimiter=r"\s+", converters = {'%eval_id': str})

    if list_cols:
        print(*df_data.columns)
        sys.exit(0)

    if mb:
        equals_constraint = equals_constraint + (("MB", mb),)

    if equals_constraint:
        for i in equals_constraint:
            if i[0] in df_data:
                df_data = df_data[df_data[i[0]] == i[1]]

    if match_constraint:
        for i in match_constraint:
            if i[0] in df_data:
                df_data = df_data[df_data[i[0]] == i[1]]

    if greater_than_equals_constraint:
        for i in greater_than_equals_constraint:
            if i[0] in df_data:
                df_data = df_data[df_data[i[0]] >= i[1]]

    if lesser_than_equals_constraint:
        for i in lesser_than_equals_constraint:
            if i[0] in df_data:
                df_data = df_data[df_data[i[0]] <= i[1]]

    if ascending:
        df_data = df_data.sort_values(
            by=[i for i in ascending if i in df_data], ascending=True
        )

    if descending:
        df_data = df_data.sort_values(
            by=[i for i in descending if i in df_data], ascending=False
        )

    ordered_eval_ids = list(df_data["%eval_id"])

    if label:
        ordered_labels = [label for _ in ordered_eval_ids]
    elif "label" in df_data:
        ordered_labels = list(df_data["label"])
    else:
        ordered_labels = ["" for _ in ordered_eval_ids]

    mbs = list(df_data["MB"])

    if skip:
        ordered_eval_ids = ordered_eval_ids[::skip]
        ordered_labels = ordered_labels[::skip]
        mbs = mbs[::skip]

    if tail:
        ordered_eval_ids = ordered_eval_ids[-tail:]
        ordered_labels = ordered_labels[-tail:]
        mbs = mbs[-tail:]

    if head:
        ordered_eval_ids = ordered_eval_ids[0:head]
        ordered_labels = ordered_labels[0:head]
        mbs = mbs[0:head]

    if linspace:
        ordered_eval_ids = [
            ordered_eval_ids[i]
            for i in np.linspace(
                0, len(ordered_eval_ids)-1, linspace, dtype=int, endpoint=True
            )
        ]
        ordered_labels = [
            ordered_labels[i]
            for i in np.linspace(
                0, len(ordered_labels)-1, linspace, dtype=int, endpoint=True
            )
        ]
        mbs = [
            mbs[i]
            for i in np.linspace(0, len(mbs)-1, linspace, dtype=int, endpoint=True)
        ]

    if reverse:
        ordered_eval_ids.reverse()
        ordered_labels.reverse()
        mbs.reverse()

    # print(*ordered_eval_ids)

    for id_row, eval_id in enumerate(ordered_eval_ids):
        mb_val = mbs[id_row]
        label_val = ordered_labels[id_row]

        if label_val == "" or label:
            case_dir_start = (
                absolute_datfile_path.as_posix().split("SOL")[1].split("/post/")[0]
            ).replace("/", "\\")
        else:
            label_dir_path = absolute_datfile_path.parents[0].as_posix()
            case_dir_name = [
                diri
                for diri in os.listdir(label_dir_path)
                if diri.endswith(f"__{label_val}")
            ][0]
            case_dir_path = (
                Path(f"{label_dir_path}/{case_dir_name}")
                .readlink()
                .absolute()
                .resolve()
                .as_posix()
            )
            case_dir_start = (case_dir_path.split("SOL")[1].split("/post/")[0]).replace(
                "/", "\\"
            )

        case_dir_rel_to_root = f"{case_dir_start}\\post\\images\\doe.{eval_id}\\MB{mb_val:.0f}"
        if annotate_mb:
            annotation = f"{label_val}/doe.{eval_id}/MB{mb_val:.0f}"
        else:
            annotation = f"{label_val}/doe.{eval_id}"
        print(f"{case_dir_rel_to_root} {annotation}")


if __name__ == "__main__":
    main()
