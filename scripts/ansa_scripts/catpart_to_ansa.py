#!/usr/bin/python
# PYTHON script
import numpy as np
import os
import ntpath
import sys
import getopt
import inspect
import ansa
from ansa import base
from ansa import constants


def main():
    base_name = ntpath.basename(base.DataBaseName())
    split_name = os.path.splitext(base_name)[0]

    ansa.base.All()
    ansa.base.UnFreezeVisibleFaces()

    # # set tolerances to extra fine, otherwise Topo might fail
    # ansa.base.SetNodeTolerance(0.001)
    # ansa.base.SetCurveTolerance(0.005)
    # ansa.base.Topo()

    ansa.mesh.AspacingSTL(0.1, 10, 4)
    ansa.mesh.CreateStlMesh()
    ansa.base.FreezeVisibleFaces()

    for i in range(1, 4):
        ansa.base.All()
        unmeshed_macros = ansa.mesh.UnmeshedMacros()
        if unmeshed_macros > 0:
            ansa.base.UnFreezeVisibleFaces()
            ansa.mesh.AspacingSTL(0.01 / i, 1 / i, 2 / i, 0.1 / i)
            ansa.base.All()
            ansa.mesh.CreateStlMesh()
    unmeshed_macros = ansa.mesh.UnmeshedMacros()
    print("Last unmeshed Macros : {}".format(unmeshed_macros))

    ansa.base.All()
    ansa.base.FreezeVisibleFaces()
    ansa.base.SaveAs("{}.ansa".format(split_name))


if __name__ == "__main__":
    main()
    print("Done!")
