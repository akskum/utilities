#!/bin/bash


# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE
Usage: ${0##*/} filename [OPTIONS]
options:
  -d    | -delete   <pidnames>           deletes the pids
  -hi   | -hide     <pidnames>           hides the pids
  -f    | -fine     <pidnames>           pids where fine spacing is desired
  -ef   | -exfine   <pidnames>           pids where extra fine spacing is desired
  -a    | -ansaonly                      Generate only ansa file [requires .CATPart]
  -l    | -list                          List PIDS in ansa file
  -h    | -help                          help

Generates a .ansa file from a .CATPart (with -a flag)
    [OR]
Generates a .stl file from a .ansa or a .CATPart (without -a flag)

There is no guarantee that a watertight mesh will be created. Also it is possible that some unmeshed macros do not make it to the STL. Check the log.

USAGE
    exit 1
}

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

# function to convert .CATPart to .ansa
catpart_to_ansa()
{
    $ANSA_HOME/ansa64.sh -gui "CFD" -nogui  -lm_retry 10 -exec load_script:$catpart_to_ansa_py -i $geometry_CATPart 
}

# function to convert .ansa to geo.stl
ansa_to_stl()
{
    $ANSA_HOME/ansa64.sh -gui "CFD" -nogui -lm_retry 10 -exec load_script:$ansa_to_stl_py -i $geometry_ansa
}

# function to list pids in ansa file
list_ansa_pids()
{
    $ANSA_HOME/ansa64.sh -gui "CFD" -nogui -lm_retry 10 -exec load_script:$list_ansa_pids_py -i $geometry_ansa
}

# location of python scripts
catpart_to_ansa_py="/home/akucwh/apps/utilities/scripts/ansa_scripts/catpart_to_ansa.py"
ansa_to_stl_py="/home/akucwh/apps/utilities/scripts/ansa_scripts/ansa_to_stl.py"
list_ansa_pids_py="/home/akucwh/apps/utilities/scripts/ansa_scripts/.list_pids.py"

# clear the pid lists in the script
appendToPythonList()
{
    sed -i "s;$1;$2;g" $ansa_to_stl_py
}
# clean the python lists
appendToPythonList "deleteNames = .*" "deleteNames = [ ]"
appendToPythonList "hiddenPartNames = .*" "hiddenPartNames = [ ]"
appendToPythonList "fineNames = .*" "fineNames = [ ]"
appendToPythonList "exFineNames = .*" "exFineNames = [ ]"


# parse options
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h | -help*)
        usage
        ;;
    -d | -delete)
        [ "$#" -ge 2 ] || die "'-delete' requires atleast one shell name"
        [[ "$2" != "-"* ]] || die "'-delete' requires atleast one shell name"
        while [ "$#" -gt 0 ]
        do
            if [[ "$2" == "-"* ]]
            then
                break
            else
                appendToPythonList "deleteNames = \[" "deleteNames = \[ \"$2\"\,"
                shift
            fi
        done
        ;;
    -hi | -hide)
        [ "$#" -ge 2 ] || die "'-hide' requires atleast one shell name"
        [[ "$2" != "-"* ]] || die "'-hide' requires atleast one shell name"
        while [ "$#" -gt 0 ]
        do
            if [[ "$2" == "-"* ]]
            then
                break
            else
                appendToPythonList "hiddenPartNames = \[" "hiddenPartNames = \[ \"$2\"\,"
                shift
            fi
        done
        ;;
    -f | -fine)
        [ "$#" -ge 2 ] || die "'-fine' requires atleast one shell name"
        [[ "$2" != "-"* ]] || die "'-fine' requires atleast one shell name"
        while [ "$#" -gt 0 ]
        do
            if [[ "$2" == "-"* ]]
            then
                break
            else
                appendToPythonList "fineNames = \[" "fineNames = \[ \"$2\"\,"
                shift
            fi
        done
        ;;
    -ef | -exfine)
        [ "$#" -ge 2 ] || die "'-exfine' requires atleast one shell name"
        [[ "$2" != "-"* ]] || die "'-exfine' requires atleast one shell name"
        while [ "$#" -gt 0 ]
        do
            if [[ "$2" == "-"* ]]
            then
                break
            else
                appendToPythonList "exFineNames = \[" "exFineNames = \[ \"$2\"\,"
                shift
            fi
        done
        ;;
    -a)
        generate_ansa_only=true
        ;;
    -l)
        generate_ansa_pid_list=true
        ;;
    *)
        [ -f $1 ] && inputfile=$1
        ;;
    esac
    shift
done

# location of ansa executable
VER=`echo $_LMFILES_ | tr ':' '\n' | grep "/ansa/" | rev | cut -f1 -d'/' | rev`
[[ $VER == "" ]] && die "No ansa module loaded. Load it first"
ANSA_HOME=/grp/techsim/crash/apps/BETA_CAE_Systems/ansa_$VER/

# # license servers
# LIC_SERVER=n14310
# LIC_SERVER=n14312
# LIC_SERVER=n14311

[ ! $inputfile ] && die "input file not specified."
filename=$(basename -- "$inputfile")
extension="${filename##*.}"
filename="${filename%.*}"

[ $extension == "ansa" ] || [ $extension == "CATPart" ] || die "Either .ansa or .CATPart must be specified"

if [ $extension == "CATPart" ]
then
    geometry_CATPart=$filename".CATPart"
    catpart_to_ansa
fi
geometry_ansa=$filename".ansa"

if [ $generate_ansa_pid_list ] 
then
    list_ansa_pids
    exit 0
fi

if [ ! $generate_ansa_only ]
then
    ansa_to_stl    
else
    list_ansa_pids
fi
