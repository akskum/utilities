#!/bin/bash

for i in {1..<N>}
do
(
    if [ -d "$i" ]; then
	cd $i
        if [ ! -f geometry.ansa ]; then
	    echo $i
            ../ansa_from_Catpart.sh >& ansa.log
        fi
    fi
)
done
