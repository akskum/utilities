import os
import time
import ansa
from ansa import base
from ansa import mesh
from ansa import constants

deck = constants.NASTRAN


@ansa.session.defbutton("Volumes", "List")
def vol_list():
    base.DefineVolumeFrom(0, "part")
    pass


@ansa.session.defbutton("Test", "Test")
def test():
    ents_visible = base.CollectEntities(
        deck, None, "__ALL_ENTITIES__", filter_visible=True
    )
    for ent in ents_visible:
        print(ent.card_fields(deck))
    pass


def main():
    print(0)


if __name__ == "__main__":
    main()
