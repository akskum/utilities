#!/bin/bash

# location of ansa executable
VER=`echo $_LMFILES_ | tr ':' '\n' | grep "/ansa/" | rev | cut -f1 -d'/' | rev`
[[ $VER == "" ]] && die "No ansa module loaded. Load it first"
ANSA_HOME=/grp/techsim/crash/apps/BETA_CAE_Systems/ansa_$VER/

# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE
Usage: ${0##*/} filename [OPTIONS]
options:
  -h    | -help                          help
  -i    | -input                         input CATPart/ansa file
  -s    | -script                        input script

Tests the the given ansa script on an input.

USAGE
    exit 1
}

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

# function to run ansa and load script
runansa()
{
    $ANSA_HOME/ansa64.sh -gui "CFD" -nogui  -lm_retry 10 -exec load_script:$inputScriptFile -i $inputCADFile
}


# parse options
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h | -help)
        usage
        ;;
    -i | -input)
        [ "$#" -ge 2 ] || die "'-input' requires a CATPart or ANSA file input"
        inputCADFile=$2
        shift
        ;;
    -s | -script)
        [ "$#" -ge 2 ] || die "'-script' requires a .py file input"
        inputScriptFile=$2
        shift
        ;;
    -*)
        die "Unknown input"
    esac
    shift
done

[[ ! $inputCADFile ]] && die "input CAD file not specified."
filename=$(basename -- "$inputCADFile")
extension="${filename##*.}"
filename="${filename%.*}"
[[ $extension == "ansa" ]] || [[ $extension == "CATPart" ]] || die "Either .ansa or .CATPart must be specified"

[[ ! $inputScriptFile ]] && die "input script file not specified."
filename=$(basename -- "$inputScriptFile")
extension="${filename##*.}"
filename="${filename%.*}"
[[ $extension == "py" ]]  || die "Input script must be a .py file"

echo "Loading $inputCADFile and running ansa script $inputScriptFile"
runansa
