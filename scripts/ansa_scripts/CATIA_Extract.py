# PYTHON script
import sys
from timeit import default_timer as timer

import ansa
from ansa import base, constants, utils, mesh

m = utils.Messenger()
deck = constants.NASTRAN

# HARDCODED CONSTANTS
UNITED_SHELL_NAME = "MAIN_UNITED_SURFACE"

SET_SIMULATION_SURFACES_NAME = "set_simulation_surfaces"
SET_NAMEREFERENCE_SURFACES_NAME = "set_namereference_surfaces"


def createDefaultSets():
    global set_simulation_surfaces
    global set_namereference_surfaces

    set_simulation_surfaces = getOrCreateNamedEntity(
        SET_SIMULATION_SURFACES_NAME, "SET"
    )
    set_namereference_surfaces = getOrCreateNamedEntity(
        SET_NAMEREFERENCE_SURFACES_NAME, "SET"
    )


def time_function_call(function_call, function_desc, start_time):
    print(f"\n==>{function_desc}...")
    function_call()
    end_time = timer()
    print(f"=>Took {end_time - start_time} seconds")
    return end_time


def refinePerims():
    mesh.AspacingSTL(0.025, 5, 5, 0.25)


def renumber_shells():
    shells = base.CollectEntities(deck, None, "PSHELL")
    for counter, pshell in enumerate(shells):
        base.SetEntityCardValues(deck, pshell, {"PID": counter + 1})


def fixGeom():
    base.CheckAndFixGeometry(0, ["UNCHECKED FACES", "NEEDLE FACES"], [1, 1])


def getNamedEntities(name, ent_type):
    ents = []
    try:
        for entity in base.NameToEnts(name, deck, constants.ENM_EXACT):
            if base.GetEntityType(deck, entity) == ent_type:
                ents.append(entity)
        if ents:
            return ents
    except Exception:
        pass


def getNamedEntity(name, ent_type):
    try:
        ents = getNamedEntities(name, ent_type)
        if ents:
            return ents[0]
    except Exception:
        pass


def getOrCreateNamedEntity(name, ent_type):
    try:
        entity = getNamedEntity(name, ent_type)
        if entity:
            return entity
    except Exception:
        pass
    entity = base.CreateEntity(deck, ent_type, {"Name": name})
    return entity


def checkIfPSHELLexists(name=""):
    if not (name and name != ""):
        print("Please provide a name!")
    else:
        if getNamedEntity(name, "PSHELL"):
            print(f"PSHELL {name} exists!")
            return True
        else:
            print(f"PSHELL {name} does not exist!")
    return False


def getDblSets():
    init_set_list = base.CollectEntities(deck, None, "SET", filter_visible=False)
    base.RmdblSameSide(0.1, 80, "both", "set")
    updated_set_list = base.CollectEntities(deck, None, "SET", filter_visible=False)
    newly_added_set_list = [id for id in updated_set_list if id not in init_set_list]
    if newly_added_set_list and len(newly_added_set_list) > 1:
        return [newly_added_set_list[0], newly_added_set_list[1]]


def splitDblSet(faces1=[], faces2=[]):
    base.Or(faces1)
    base.And(faces2)
    [dbl_set_A, dbl_set_B] = getDblSets()
    double_faces1 = [
        face
        for face in base.CollectEntities(deck, dbl_set_A, "FACE")
        + base.CollectEntities(deck, dbl_set_B, "FACE")
        if face in faces1
    ]
    double_faces2 = [
        face
        for face in base.CollectEntities(deck, dbl_set_A, "FACE")
        + base.CollectEntities(deck, dbl_set_B, "FACE")
        if face in faces2
    ]
    base.DeleteEntity([dbl_set_A, dbl_set_B], True)
    return [double_faces1, double_faces2]


def getDblSetPairs(bigPid_faces, smallPid_faces):
    # base.Or(smallPid_faces)
    # base.Near(
    #     radius=0.1,
    #     dense_search=False,
    # )
    # near_bigPid_faces = [
    #     face
    #     for face in base.CollectEntities(deck, None, "FACE", filter_visible=True)
    #     if face in bigPid_faces
    # ]

    [bigPid_double_faces, smallPid_double_faces] = splitDblSet(
        faces1=bigPid_faces, faces2=smallPid_faces
    )

    return [bigPid_double_faces, smallPid_double_faces]


def appendDblSetPairs(
    face_list_A, face_list_B, Dbl_combined_set_A=None, Dbl_combined_set_B=None
):
    [Dbl_list_A, Dbl_list_B] = getDblSetPairs(face_list_A, face_list_B)
    if Dbl_combined_set_A:
        base.AddToSet(Dbl_combined_set_A, Dbl_list_A)
    if Dbl_combined_set_B:
        base.AddToSet(Dbl_combined_set_B, Dbl_list_B)


def seedWallSets():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    init_shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
    united_shell = getNamedEntity(UNITED_SHELL_NAME, "PSHELL")
    united_faces = base.CollectEntities(deck, united_shell, "FACE", filter_visible=True)
    other_shell_list = [shell for shell in init_shell_list if shell != united_shell]

    for other_shell in other_shell_list:
        Set_duplicates_in_united = getOrCreateNamedEntity(
            f"wall_{other_shell._name}", "SET"
        )
        try:
            base.Or(init_vis_faces)
            other_faces = base.CollectEntities(
                deck, other_shell, "FACE", filter_visible=True
            )
            appendDblSetPairs(
                united_faces,
                other_faces,
                Set_duplicates_in_united,
                None,
            )
        except Exception:
            pass

    base.Or(init_vis_faces)


def expensiveSeedWallSets():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    init_shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
    united_shell = getNamedEntity(UNITED_SHELL_NAME, "PSHELL")
    united_faces = base.CollectEntities(deck, united_shell, "FACE", filter_visible=True)

    base.Or(united_shell)
    base.Near(radius=0.1, dense_search=False)
    base.Neighb(str(3))
    refinePerims()
    base.Or(init_vis_faces)

    base.Or(set_namereference_surfaces)
    nameref_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    other_shell_list = [shell for shell in init_shell_list if shell != united_shell]

    for face in united_faces:
        base.Or(face)
        base.Near(radius=0.1, dense_search=False)
        base.Neighb(str(3))
        neighb_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
        neighb_faces_namerefPIDS = [facei for facei in neighb_faces if facei in nameref_faces]
        base.Or(neighb_faces_namerefPIDS)
        neighb_shells_namerefPIDS = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
        nMapShells = 0
        nearDistValue = 0.001
        Set_duplicates_in_united = None
        while nMapShells == 0 and nearDistValue <=10:
            for shelli in neighb_shells_namerefPIDS:
                faces_in_shelli = [facei for facei in base.CollectEntities(deck, shelli, "FACE") if facei in neighb_faces_namerefPIDS]
                flanges = base.IsolateFlangesProximity(faces_in_shelli, face, "distance", nearDistValue, nearDistValue, nearDistValue)
                if type(flanges) == list:
                    nMapShells = nMapShells + 1
                    Set_duplicates_in_united = getOrCreateNamedEntity(
                        f"wall_{shelli._name}", "SET"
                    )
            nearDistValue = nearDistValue * 4
        if nMapShells == 1 and Set_duplicates_in_united != None:
            base.AddToSet(Set_duplicates_in_united, face)
        if nMapShells > 2:
            print("Two mappable shells!")
        
    base.Or(init_vis_faces)


def rename_vis_faces(
    input_shells=None,
    remove_pre_string="",
    remove_post_string="",
    add_pre_string="",
    add_post_string="",
    replaceFullName=False,
):
    shell_list = []
    if not input_shells:
        input_shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)

    for shell in input_shells:
        if replaceFullName:
            remove_pre_string = shell._name
            remove_post_string = ""

        trimmed_shell_name = shell._name.replace(remove_pre_string, "").replace(
            remove_post_string, ""
        )
        new_shell_name = add_pre_string + trimmed_shell_name + add_post_string
        new_shell = getOrCreateNamedEntity(new_shell_name, "PSHELL")
        new_shell_id = new_shell._id
        shell_list.append(new_shell)
        for face in base.CollectEntities(deck, shell, "FACE", filter_visible=True):
            base.SetEntityCardValues(deck, face, {"PID": new_shell_id})
    return shell_list


def convertSetsToPIDS():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    init_set_list = base.CollectEntities(deck, None, "SET", filter_visible=False)
    for setI in init_set_list:
        if setI != set_simulation_surfaces and setI != set_namereference_surfaces:
            base.Or(setI)
            rename_vis_faces(add_pre_string=setI._name, replaceFullName=True)
    base.Or(init_vis_faces)


def shrinkPIDS(pid_to_shrink, neighb_val=1):
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    shrink_pids_shell = getNamedEntity(pid_to_shrink, "PSHELL")
    shrink_pids_faces = base.CollectEntities(
        deck, shrink_pids_shell, "FACE", filter_visible=True
    )
    for face in shrink_pids_faces:
        base.Or(face)
        base.Neighb(str(neighb_val))
        vis_shell_names_list = [
            shell._name
            for shell in base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
            if shell._name != pid_to_shrink
        ]
        if len(vis_shell_names_list) == 1:
            base.AddToSet(getNamedEntity(vis_shell_names_list[0], "SET"), face)
    base.Or(init_vis_faces)


def filterPSHELLSIntoSets(filterPShellNames, passSet, failSet):
    shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=False)
    for shell in shell_list:
        if shell._name in filterPShellNames:
            base.AddToSet(passSet, shell)
        else:
            base.AddToSet(failSet, shell)


def removeEntsInSet(set_to_remove):
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    base.Or(set_to_remove)
    base.DeleteEntity(
        base.CollectEntities(deck, None, "FACE", filter_visible=True), True
    )
    base.Or(init_vis_faces)


@ansa.session.defbutton("CATIA_Extract", "Extract_Walls")
def Extract_Walls():
    # global set_simulation_surfaces, set_namereference_surfaces
    print("\n\n##################################")
    print("######### BEGIN WALL EXTRACTION #########")
    print("##################################\n")

    start_time = timer()
    running_time = start_time
    base.BlockRedraws(True)
    base.All()

    running_time = time_function_call(
        lambda: base.Compress(""), "Compressing", running_time
    )
    running_time = time_function_call(
        lambda: createDefaultSets(), "Creating default SETS", running_time
    )
    running_time = time_function_call(lambda: base.Orient(), "Orienting", running_time)
    running_time = time_function_call(
        lambda: fixGeom(), "Fixing geom pass 1", running_time
    )
    running_time = time_function_call(
        lambda: checkIfPSHELLexists(name=UNITED_SHELL_NAME),
        f"Checking if PSHELL with name {UNITED_SHELL_NAME} exists",
        running_time,
    )
    running_time = time_function_call(
        lambda: filterPSHELLSIntoSets(
            filterPShellNames=[UNITED_SHELL_NAME],
            passSet=set_simulation_surfaces,
            failSet=set_namereference_surfaces,
        ),
        "Separating Inner volume PID from reference PID",
        running_time,
    )
    running_time = time_function_call(
        lambda: seedWallSets(), "Seeding walls", running_time
    )
    running_time = time_function_call(
        lambda: convertSetsToPIDS(), "Converting sets to PIDS", running_time
    )
    running_time = time_function_call(
        lambda: expensiveSeedWallSets(), "Seeding walls (expensive)", running_time
    )
    running_time = time_function_call(
        lambda: convertSetsToPIDS(), "Converting sets to PIDS", running_time
    )
    running_time = time_function_call(
        lambda: shrinkPIDS(pid_to_shrink=UNITED_SHELL_NAME, neighb_val=1),
        "Adding wall adjacent faces to wall sets",
        running_time,
    )
    running_time = time_function_call(
        lambda: convertSetsToPIDS(), "Converting sets to PIDS", running_time
    )
    running_time = time_function_call(
        lambda: removeEntsInSet(set_namereference_surfaces),
        "Removing name reference PSHELLS",
        running_time,
    )
    running_time = time_function_call(
        lambda: base.Compress(""), "Compressing", running_time
    )
    running_time = time_function_call(
        lambda: renumber_shells(), "Renumbering shells", running_time
    )
    base.All()
    base.BlockRedraws(False)
    print("\n\nEntire operation took {} seconds".format(running_time - start_time))


def main():
    pass


if __name__ == "__main__":
    main()
