# PYTHON script
import os
import ansa
import time
from ansa import base
from ansa import mesh
from ansa import constants
from ansa import utils
from itertools import combinations
from timeit import default_timer as timer


deck = constants.NASTRAN
m = utils.Messenger()

Set_united_name = "Set_united"
Set_walls_name = "Set_walls"
Set_non_walls_name = "Set_non_walls"
Set_interfaces_name = "Set_simulation_interfaces"
Set_dbl_in_pids_name = "Set_dbl_in_pids"
Set_non_dbl_name = "Set_non_dbl"


def fixGeom():
    base.CheckAndFixGeometry(0, ["UNCHECKED FACES", "NEEDLE FACES"], [1, 1])


def refinePerims():
    mesh.AspacingSTL(0.025, 5, 5, 0.25)
    # mesh.AspacingSTL(0.1, 10, 20, 1)


def renumber_shells():
    shells = base.CollectEntities(deck, None, "PSHELL")
    for counter, pshell in enumerate(shells):
        base.SetEntityCardValues(deck, pshell, {"PID": counter + 1})


def getNamedEntities(name, ent_type):
    ents = []
    try:
        for entity in base.NameToEnts(name, deck, constants.ENM_EXACT):
            if base.GetEntityType(deck, entity) == ent_type:
                ents.append(entity)
        if ents:
            return ents
    except:
        pass


def getNamedEntity(name, ent_type):
    try:
        ents = getNamedEntities(name, ent_type)
        if ents:
            return ents[0]
    except:
        pass


def getOrCreateNamedEntity(name, ent_type):
    try:
        entity = getNamedEntity(name, ent_type)
        if entity:
            return entity
    except:
        pass
    entity = base.CreateEntity(deck, ent_type, {"Name": name})
    return entity


def createDefaultSets():
    global Set_united
    global Set_walls
    global Set_non_walls
    global Set_interfaces
    global Set_dbl_in_pids
    global Set_non_dbl

    Set_united = getOrCreateNamedEntity(Set_united_name, "SET")
    Set_walls = getOrCreateNamedEntity(Set_walls_name, "SET")
    Set_non_walls = getOrCreateNamedEntity(Set_non_walls_name, "SET")
    Set_interfaces = getOrCreateNamedEntity(Set_interfaces_name, "SET")
    Set_dbl_in_pids = getOrCreateNamedEntity(Set_dbl_in_pids_name, "SET")
    Set_non_dbl = getOrCreateNamedEntity(Set_non_dbl_name, "SET")


def removeEntitiesExcept(keep_ent_list, ent_type, filter_visible=False):
    ent_list = base.CollectEntities(deck, None, ent_type, filter_visible=filter_visible)
    remove_ent_list = [ent for ent in ent_list if ent not in keep_ent_list]
    base.DeleteEntity(remove_ent_list, True)


def getDblSets():
    init_set_list = base.CollectEntities(deck, None, "SET", filter_visible=False)
    base.RmdblSameSide(0.05, 95, "both", "set")
    updated_set_list = base.CollectEntities(deck, None, "SET", filter_visible=False)
    newly_added_set_list = [id for id in updated_set_list if id not in init_set_list]
    if newly_added_set_list:
        return [newly_added_set_list[0], newly_added_set_list[1]]


def splitDblSet(faces1=[], faces2=[]):
    base.Or(faces1)
    base.And(faces2)
    [dbl_set_A, dbl_set_B] = getDblSets()
    double_faces1 = [
        face
        for face in base.CollectEntities(deck, dbl_set_A, "FACE")
        + base.CollectEntities(deck, dbl_set_B, "FACE")
        if face in faces1
    ]
    double_faces2 = [
        face
        for face in base.CollectEntities(deck, dbl_set_A, "FACE")
        + base.CollectEntities(deck, dbl_set_B, "FACE")
        if face in faces2
    ]
    return [double_faces1, double_faces2]


def getDblSetPairs(bigPid_faces, smallPid_faces):
    base.Or(smallPid_faces)
    base.Near(
        radius=0.1,
        dense_search=False,
    )
    near_bigPid_faces = [
        face
        for face in base.CollectEntities(deck, None, "FACE", filter_visible=True)
        if face in bigPid_faces
    ]

    [bigPid_double_faces, smallPid_double_faces] = splitDblSet(
        faces1=near_bigPid_faces, faces2=smallPid_faces
    )

    return [bigPid_double_faces, smallPid_double_faces]


def appendDblSetPairs(face_list_A, face_list_B, Dbl_combined_set_A, Dbl_combined_set_B):
    [Dbl_list_A, Dbl_list_B] = getDblSetPairs(face_list_A, face_list_B)
    base.AddToSet(Dbl_combined_set_A, Dbl_list_A)
    base.AddToSet(Dbl_combined_set_B, Dbl_list_B)


def rename_vis_faces(
    input_shells=None, pre_string="", post_string="", remove_string=""
):
    shell_list = []
    if not input_shells:
        input_shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)

    for shell in input_shells:
        new_shell_name = (pre_string + shell._name + post_string).replace(
            remove_string, ""
        )
        new_shell = getOrCreateNamedEntity(new_shell_name, "PSHELL")
        new_shell_id = new_shell._id
        shell_list.append(new_shell)
        for face in base.CollectEntities(deck, shell, "FACE", filter_visible=True):
            base.SetEntityCardValues(deck, face, {"PID": new_shell_id})
    return shell_list


def combineSameNameShells():
    rename_vis_faces()
    base.Compress("")
    pass


def getFilteredCurveList(filter_str):
    return [
        curve
        for curve in base.CollectEntities(deck, None, "CURVE", filter_visible=False)
        if curve._name.startswith(filter_str)
    ]


def getCurvesGroupedByName(filter_str):
    dictionary = {}
    for curve in getFilteredCurveList("Intersect"):
        name = curve._name
        if name in dictionary:
            dictionary[name].append(curve)
        else:
            dictionary[name] = [curve]
    pass
    return dictionary


def processIntersectCurveName(name):
    return name.replace("Intersect_", "").split("_")


def projectCurvesOnFaces():
    for curveList in getCurvesGroupedByName("Intersect").values():
        curve = curveList[0]
        pid_names = processIntersectCurveName(curve._name)
        base.And(curveList)
        base.Near(
            radius=0.1,
            dense_search=False,
        )
        for name in pid_names:
            shell = getNamedEntity(name, "PSHELL")
            faces = base.CollectEntities(deck, shell, "FACE")
            try:
                base.ConsProjectNormal(
                    curveList,
                    faces,
                    max_distance=0.01,
                    split_original=False,
                    connect_with_faces=False,
                    nearest_target=False,
                )
            except:
                pass


def nearCurves():
    curveList = []
    for curve in getFilteredCurveList("Intersect"):
        curveList.append(curve)
    base.Or(curveList)
    try:
        base.Near(
            radius=0.1,
            dense_search=False,
        )
        base.Neighb("2")
    except:
        pass


def getNumFacesDict(shells=None):
    if not shells:
        shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=False)
    dictionary = {}
    for shell in shells:
        fields = shell._cardFields(deck)
        dictionary[shell._name] = {
            "faces": base.CollectEntities(deck, shell, "FACE"),
            "length": len(base.CollectEntities(deck, shell, "FACE")),
        }
    return dictionary


def seedWalls():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    init_shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
    united_shell = getNamedEntity("United", "PSHELL")
    united_faces = base.CollectEntities(deck, united_shell, "FACE", filter_visible=True)
    base.AddToSet(Set_united, united_shell)
    other_shell_list = [shell for shell in init_shell_list if shell != united_shell]

    for other_shell in other_shell_list:
        try:
            base.Or(init_vis_faces)
            other_faces = base.CollectEntities(
                deck, other_shell, "FACE", filter_visible=True
            )
            appendDblSetPairs(united_faces, other_faces, Set_united, Set_walls)
        except:
            pass

    base.Or(init_vis_faces)


def releaseCONSBetweenPIDS(shell_list_A, shell_list_B):
    shell_list_A_ids = [shell._id for shell in shell_list_A]
    shell_list_B_ids = [shell._id for shell in shell_list_B]
    base.Or(shell_list_A + shell_list_B)
    cons_to_release = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            == 2
        ):
            faces = base.GetFacesOfCons(cons=con)
            if (
                base.GetEntityCardValues(deck, faces[0], ["PID"])["PID"]
                in shell_list_A_ids
                and base.GetEntityCardValues(deck, faces[1], ["PID"])["PID"]
                in shell_list_B_ids
            ):
                cons_to_release.append(con)
            elif (
                base.GetEntityCardValues(deck, faces[1], ["PID"])["PID"]
                in shell_list_A_ids
                and base.GetEntityCardValues(deck, faces[0], ["PID"])["PID"]
                in shell_list_B_ids
            ):
                cons_to_release.append(con)

    base.ReleaseCons(cons_to_release)


def getWallsAndNonWalls():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    base.Or(Set_walls)
    init_wall_shell_list = rename_vis_faces(pre_string="init_wall_")

    base.Or(init_vis_faces)
    base.Not([Set_walls, Set_united])
    init_non_wall_shell_list = rename_vis_faces(pre_string="init_non_wall_")

    releaseCONSBetweenPIDS(init_wall_shell_list, init_non_wall_shell_list)

    base.Or(init_wall_shell_list)
    base.Neighb("All")
    wall_shell_list = rename_vis_faces(pre_string="wall_", remove_string="init_wall_")
    base.Or(init_non_wall_shell_list)
    base.Neighb("All")
    non_wall_shell_list = rename_vis_faces(
        pre_string="non_wall_", remove_string="init_non_wall_"
    )

    base.Or(wall_shell_list + non_wall_shell_list)
    topoRedCons()

    base.AddToSet(Set_walls, wall_shell_list)
    base.AddToSet(Set_non_walls, non_wall_shell_list)

    base.Or(init_vis_faces)


def getSimulationInterfaces():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    nonDblWithUnited_shells = base.CollectEntities(deck, Set_non_walls, "PSHELL")
    face_len_dict = getNumFacesDict(nonDblWithUnited_shells)
    nonDblWithUnited_shells_sorted = sorted(
        nonDblWithUnited_shells,
        key=lambda x: face_len_dict[x._name]["length"],
        reverse=True,
    )
    base.Or(Set_non_walls)
    for shellA, shellB in combinations(nonDblWithUnited_shells_sorted, 2):
        try:
            [interface_faces, other_Dbl_faces] = getDblSetPairs(
                face_len_dict[shellA._name]["faces"],
                face_len_dict[shellB._name]["faces"],
            )
            if interface_faces and other_Dbl_faces:
                base.Or(interface_faces)
                rename_vis_faces(
                    pre_string="interface_",
                    post_string="_" + shellB._name,
                    remove_string="non_wall_",
                )
                base.AddToSet(Set_interfaces, interface_faces)
                base.AddToSet(Set_dbl_in_pids, other_Dbl_faces)
        except:
            pass

    base.Or(init_vis_faces)


def topoRedCons(ent_list=[], near_non_red=False):
    if not ent_list:
        ent_list = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    base.Or(ent_list)

    red_cons = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            == 1
        ):
            red_cons.append(con)

    try:
        if not near_non_red:
            base.Topo(
                cons=red_cons, paste_with_frozen_faces=False, paste_different_pids=True
            )
        else:
            base.ConsMatchingDistance(0.01)
            base.Near(
                radius=0.1,
                dense_search=False,
            )
            base.Topo(paste_with_frozen_faces=False, paste_different_pids=False)
            base.ConsMatchingDistance(0.2)

    except:
        pass


def cleanUpAndPerformTopo():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    removeEntitiesExcept(
        [
            Set_united,
            Set_walls,
            Set_interfaces,
            Set_dbl_in_pids,
            Set_non_dbl,
        ],
        "SET",
    )

    base.Not(
        [
            Set_united,
            Set_walls,
            Set_interfaces,
            Set_dbl_in_pids,
        ]
    )
    base.AddToSet(
        Set_non_dbl, base.CollectEntities(deck, None, "FACE", filter_visible=True)
    )

    base.Or([Set_united, Set_dbl_in_pids])
    base.DeleteEntity(
        base.CollectEntities(deck, None, "FACE", filter_visible=True), True
    )

    base.Or([Set_interfaces, Set_walls, Set_non_dbl])

    for shell in base.CollectEntities(deck, None, "PHSELL", filter_visible=True):
        base.GeoTranslate(
            "MOVE",
            0,
            "SAME PART",
            "NONE",
            0,
            0,
            0,
            shell,
            keep_connectivity=True,
            draw_results=False,
        )

    topoRedCons()

    base.Or([Set_walls, Set_interfaces, Set_non_dbl])


def removeSelfSimilar():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    similar_groups = base.IsolateSimilarGroups(
        entities=0,
        separate_at_blue_bounds=1,
        separate_at_pid_bounds=1,
        similarity_factor=90,
        distance=0.2,
        search_in_same_position=True,
    )
    for sim_group_name, sim_group in similar_groups.items():
        for conn_group_name, con_group in sim_group.items():
            print()
    pass


def removeSelfDuplicates():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    init_shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
    other_shell_list = [shell for shell in init_shell_list if shell._name != "United"]

    for other_shell in other_shell_list:
        try:
            other_faces = base.CollectEntities(
                deck, other_shell, "FACE", filter_visible=False
            )
            # IMPROVED PERFORMANCE?#base.Or(face for face in init_vis_faces if face in other_faces)
            base.Or(other_faces)
            len_vis_faces = len(
                base.CollectEntities(deck, None, "FACE", filter_visible=True)
            )
            for i in range(100):
                base.RmdblSameSide(0.1, 90, "positive", "delete")
                if (
                    len(base.CollectEntities(deck, None, "FACE", filter_visible=True))
                    == len_vis_faces
                ):
                    break
                len_vis_faces = len(
                    base.CollectEntities(deck, None, "FACE", filter_visible=True)
                )
            topoRedCons(near_non_red=True)
            removeSelfSimilar()
        except:
            pass

    base.Or(init_vis_faces)


@ansa.session.defbutton("CATProcessing", "NoProject_Extract")
def NoProject_Extract():
    print("\nStarting operation...")
    start = timer()
    base.BlockRedraws(True)
    base.ConsMatchingDistance(0.2)
    base.Compress("")
    renumber_shells()
    base.All()

    print("\n    Orienting... ")
    base.Orient()
    print("\n    Fixing geom...")
    fixGeom()
    print("\n    Merging pids with same name...")
    combineSameNameShells()
    print("\n    Performing self suplication check...")
    removeSelfDuplicates()
    # print("\n    Creating default sets...")
    # createDefaultSets()
    # print("\n    Bringing to visible Near of Intersect Curves...")
    # nearCurves()
    ## print("\n    Refining perims pass 1...")
    ## refinePerims()
    # print("\n    Seeding walls... ")
    # seedWalls()
    # print("\n    Extracting all walls and non walls... ")
    # getWallsAndNonWalls()
    # print("\n    Get simulation interfaces... ")
    # getSimulationInterfaces()
    # print("\n    Clean up and topo... ")
    # cleanUpAndPerformTopo()
    # print("\nRefining perims...")
    # refinePerims()
    # print("\nFixing geom...")
    # fixGeom()

    # base.All()
    renumber_shells()
    base.Compress("")
    base.ConsMatchingDistance(0.2)
    base.BlockRedraws(False)
    end = timer()
    print("\nOperation took {} seconds".format(end - start))


@ansa.session.defbutton("CATProcessing", "Project_Extract")
def Project_Extract():
    start = timer()

    base.BlockRedraws(True)
    base.Compress("")
    renumber_shells()
    createDefaultSets()
    base.All()

    print("\nOrienting...")
    base.Orient()
    print("\nFixing geom pass 1...")
    fixGeom()
    # print("\nRefining perims pass 1... ")    # Too expensive... not needed maybe?
    # refinePerims()                           #
    base.Or(None)
    print("\nProjecting Curves...")
    projectCurvesOnFaces()
    print("\nRefining perims pass 1...")
    refinePerims()
    base.All()
    print("\nFixing geom pass 2...")
    fixGeom()
    print("\nGet simulation walls... ")
    seedWalls()
    print("\nGet simulation interfaces... ")
    getSimulationInterfaces()
    print("\nClean up and topo... ")
    cleanUpAndPerformTopo()

    base.All()
    base.BlockRedraws(False)

    end = timer()
    m.print("Operation took {} seconds".format(end - start))


@ansa.session.defbutton("CATProcessing", "UnblockRedraw")
def UnblockRedraw():
    base.BlockRedraws(False)


def main():
    pass


if __name__ == "__main__":
    main()
