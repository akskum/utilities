import os
import time
import ansa
from ansa import base
from ansa import mesh
from ansa import constants

deck = constants.NASTRAN


@ansa.session.defbutton("Freeze operations", "Unfreeze.All")
def unfreeze_all():
    visible_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    base.All()
    base.UnFreezeVisibleFaces()
    base.Or(visible_faces)
    pass


@ansa.session.defbutton("Freeze operations", "Unfreeze.Neighb1")
def unfreeze_neighb1():
    faces_to_unfreeze = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    base.Neighb("1")
    base.Not(faces_to_unfreeze)
    buffer_faces_to_check_for_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    additional_faces_to_unfreeze = []
    for face in buffer_faces_to_check_for_frozen:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "YES":
            additional_faces_to_unfreeze.append(face)
    base.Or(faces_to_unfreeze)
    base.And(additional_faces_to_unfreeze)
    base.UnFreezeVisibleFaces()
    pass


@ansa.session.defbutton("Freeze operations", "PID.Freeze.and.Proceed")
def pid_freeze_and_proceed():
    faces_to_freeze_initially = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    base.FreezeVisibleFaces()

    base.Neighb("1")
    base.Not(faces_to_freeze_initially)
    nb_faces_to_initially_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    nb_shells_to_initially_frozen = []
    for face in nb_faces_to_initially_frozen:
        pid = base.GetEntityCardValues(deck, face, ("PID",))
        shell = base.GetEntity(deck, "PSHELL", pid["PID"])
        nb_shells_to_initially_frozen.append(shell)
    base.Or(nb_shells_to_initially_frozen)

    nb_faces_in_shells_to_initially_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    faces_to_unfreeze = []
    for face in nb_faces_in_shells_to_initially_frozen:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "NO":
            faces_to_unfreeze.append(face)

    base.Or(faces_to_unfreeze)
    base.Neighb("1")
    base.Not(faces_to_unfreeze)
    buffer_faces_to_check_for_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    additional_faces_to_unfreeze = []
    for face in buffer_faces_to_check_for_frozen:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "YES":
            additional_faces_to_unfreeze.append(face)
    base.Or(faces_to_unfreeze)
    base.And(additional_faces_to_unfreeze)
    base.UnFreezeVisibleFaces()
    base.StoreLockView("CurrentSection", True)
    pass


@ansa.session.defbutton("Freeze operations", "Neighb5.Freeze.and.Proceed")
def neighb5_freeze_and_proceed():
    faces_to_freeze_initially = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    base.FreezeVisibleFaces()

    base.Neighb("5")
    base.Not(faces_to_freeze_initially)
    nb_faces_to_initially_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )

    faces_to_unfreeze = []
    for face in nb_faces_to_initially_frozen:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "NO":
            faces_to_unfreeze.append(face)

    base.Or(faces_to_unfreeze)
    base.Neighb("1")
    base.Not(faces_to_unfreeze)
    buffer_faces_to_check_for_frozen = base.CollectEntities(
        deck, None, "FACE", filter_visible=True
    )
    additional_faces_to_unfreeze = []
    for face in buffer_faces_to_check_for_frozen:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "YES":
            additional_faces_to_unfreeze.append(face)
    base.Or(faces_to_unfreeze)
    base.And(additional_faces_to_unfreeze)
    base.UnFreezeVisibleFaces()
    base.StoreLockView("CurrentSection", True)
    pass


@ansa.session.defbutton("Checks", "Surface.Mesh.Check")
def surfaceMeshCheck():
    base.ReadCheckTemplatesFromDefaultStorage()
    base.ExecuteCheckTemplate("Surface_Mesh_Checks")
    pass


@ansa.session.defbutton("View", "Store")
def storeLock():
    base.StoreLockView("CurrentSection", True)
    pass


@ansa.session.defbutton("View", "Load")
def loadLock():
    base.LoadStoredLockView("CurrentSection")
    base.ZoomAll()
    pass


@ansa.session.defbutton("View", "Show Frozen")
def showFrozen():
    all_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    frozen_faces = []
    for face in all_faces:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "YES":
            frozen_faces.append(face)
    base.Or(frozen_faces)
    pass


@ansa.session.defbutton("View", "Show UnFrozen")
def showUnFrozen():
    all_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    unfrozen_faces = []
    for face in all_faces:
        frozen_flag = base.GetEntityCardValues(deck, face, ("Frozen",))
        flag = frozen_flag["Frozen"]
        if flag == "NO":
            unfrozen_faces.append(face)
    base.Or(unfrozen_faces)
    pass


@ansa.session.defbutton("Mesh", "Routine.Free.Visible")
def free_visible():
    neighb5_freeze_and_proceed()
    mesh.CreateFreeMesh()
    surfaceMeshCheck()
    pass


def main():
    print(0)


if __name__ == "__main__":
    main()
