#!/usr/bin/python
# PYTHON script
import os
import ntpath
import sys
import getopt
import inspect
import ansa
from ansa import base
from ansa import constants


def main():
    deck = ansa.constants.NASTRAN
    numParts = 100
    deleteNames = [ ]
    hiddenPartNames = [ ]
    fineNames = [ ]
    exFineNames = [ ]

    psDelete = []
    psHidden = []
    psFine = []
    psExFine = []

    ansa.base.All()
    for idx in range(numParts):
        shell = ansa.base.GetEntity(deck, "PSHELL", idx)
        if shell != None:
            if shell._name in deleteNames:
                psDelete.append(shell)
            if shell._name in hiddenPartNames:
                psHidden.append(shell)
            if shell._name in fineNames:
                psFine.append(shell)
            if shell._name in exFineNames:
                psExFine.append(shell)

            # remove blanks from names, since these cause problems in snappyHexMesh
            oldName = shell._name
            field = {"Name": oldName.replace(" ", "")}
            ansa.base.SetEntityCardValues(deck, shell, field)

    ansa.base.All()
    ansa.base.UnFreezeVisibleFaces()

    # # set tolerances to extra fine, otherwise Topo might fail
    # ansa.base.SetNodeTolerance(0.001)
    # ansa.base.SetCurveTolerance(0.005)
    # ansa.base.Topo()

    if len(psDelete):
        ansa.base.DeleteFaces(psDelete)

    ansa.base.Or(psHidden)
    ansa.base.FreezeVisibleFaces()
    ansa.base.Not(psHidden)

    ansa.base.Or(psExFine)
    ansa.mesh.AspacingSTL(0.025, 5, 2)
    ansa.mesh.CreateStlMesh()
    ansa.base.FreezeVisibleFaces()

    ansa.base.Or(psFine)
    ansa.mesh.AspacingSTL(0.05, 7, 3)
    ansa.mesh.CreateStlMesh()
    ansa.base.FreezeVisibleFaces()

    ansa.base.All()
    # ansa.mesh.AspacingSTL(0.1, 10, 4)
    ansa.mesh.CreateStlMesh()

    for i in range(1, 4):
        ansa.base.All()
        unmeshed_macros = ansa.mesh.UnmeshedMacros()
        ansa.base.UnFreezeVisibleFaces()
        if unmeshed_macros > 0:
            ansa.mesh.AspacingSTL(0.01 / i, 1 / i, 2 / i, 0.1 / i)
            ansa.base.All()
            ansa.base.Not(psHidden)
            ansa.mesh.CreateStlMesh()
    unmeshed_macros = ansa.mesh.UnmeshedMacros()
    print("Last unmeshed Macros : {}".format(unmeshed_macros))

    ansa.base.All()
    ansa.base.Not(psHidden)

    base_name = ntpath.basename(base.DataBaseName())
    split_name = os.path.splitext(base_name)[0]
    ansa.base.OutputStereoLithography(
        filename="{}.stl".format(split_name), mode="visible"
    )


if __name__ == "__main__":
    main()
    print("Done!")
