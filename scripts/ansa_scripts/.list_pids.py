#!/usr/bin/python
# PYTHON script
import os
import sys
import getopt
import inspect
import ansa
from ansa import base


def main():
    deck = ansa.constants.NASTRAN
    numParts = len(base.CollectEntities(deck, None, "PSHELL", filter_visible=False))

    print("The PIDs in the ansa file are listed below")
    print("----------------------------------------")
    for idx in range(100):
        shell = base.GetEntity(deck, "PSHELL", idx)
        if shell != None:
            print(shell._name)
    print("________________________________________")


if __name__ == "__main__":
    main()
    print("Done!")
