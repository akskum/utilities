# PYTHON script
import os
import time
import ansa
from ansa import base
from ansa import constants

deck = constants.NASTRAN

# normal methods
def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


# buttons
@ansa.session.defbutton("CleanUp", "Release hanging")
def release_hanging():
    model = base.GetCurrentAnsaModel()
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    cons_to_release = []
    for con in vis_cons:
        connected_faces = base.GetFacesOfCons(con)
        connected_visible_faces = intersection(connected_faces, vis_faces)
        if len(connected_visible_faces) < len(connected_faces):
            cons_to_release.append(con)
    base.ReleaseCons(cons_to_release)


@ansa.session.defbutton("CleanUp", "CyanToYellow")
def cyan_to_yellow():
    model = base.GetCurrentAnsaModel()
    initial_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    vis_faces = initial_vis_faces
    cyan_cons = [con for con in vis_cons if len(base.GetFacesOfCons(con)) > 2]
    faces_to_show = []
    for con in cyan_cons:
        connected_faces = base.GetFacesOfCons(con)
        visible_connected_faces = intersection(connected_faces, vis_faces)
        faces_to_show += visible_connected_faces
    base.Or(faces_to_show)

    pickedFaces = base.PickEntities(deck, "FACE")
    base.Or(pickedFaces)
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    cyan_cons = [con for con in vis_cons if len(base.GetFacesOfCons(con)) > 2]
    base.ReleaseCons(cyan_cons)
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    red_cons = [con for con in vis_cons if len(base.GetFacesOfCons(con)) < 2]
    base.Topo(red_cons)
    base.Or(initial_vis_faces)


@ansa.session.defbutton("CleanUp", "Check intersections")
def check_intersections():
    model = base.GetCurrentAnsaModel()
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    initial_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    ret_val = base.CheckIntersections(True, False, False)
    intersection_eligible_faces = []
    if ret_val != None:
        for entry in ret_val:
            intersection_eligible_faces.append(entry)
    base.Or(intersection_eligible_faces)


@ansa.session.defbutton("CleanUp", "Intersect and hide reds")
def intersect_hide_reds():
    print("Start")
    print("")
    model = base.GetCurrentAnsaModel()
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    initial_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    ret_val = base.CheckIntersections(True, False, True)
    if ret_val != None:
        for faces_by_region in ret_val:
            base.Or(faces_by_region)
            loopflag = 1
            while loopflag:
                vis_faces = base.CollectEntities(
                    deck, None, "FACE", filter_visible=True
                )
                len_vis_faces = len(vis_faces)
                for i in range(len_vis_faces):
                    for j in range(len_vis_faces):
                        if i != j:
                            status = base.Intersection(
                                True, [vis_faces[i]], [vis_faces[j]], True, 0
                            )
                            if status != None:
                                if len(status[2]) > 0:
                                    break
                vis_faces = base.CollectEntities(
                    deck, None, "FACE", filter_visible=True
                )
                if len(vis_faces) == len_vis_faces:
                    loopflag = 0
        base.Or(initial_vis_faces)


def main():
    print(0)


if __name__ == "__main__":
    main()
