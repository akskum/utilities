#!/usr/bin/python
# PYTHON script
import ansa
from ansa import base, constants
deck = constants.NASTRAN

def main():
    print("Main")
    export_each_pid_as_stl()


def export_each_pid_as_stl(pre="", post=""):
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    shell_list = base.CollectEntities(deck, None, "PSHELL", filter_visible=False)

    for shell in shell_list:
        base.Or(shell)
        now_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
        base.Or([face for face in init_vis_faces if face in now_vis_faces])
        now_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
        if now_vis_faces:
            base.OutputStereoLithography(filename="./{}{}{}.stl".format(pre, shell._name, post), mode="visible")


if __name__ == "__main__":
    main()
    print("Done!")
