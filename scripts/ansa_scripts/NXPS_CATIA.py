# PYTHON script
import os
import ansa
import time
from ansa import base
from ansa import mesh
from ansa import constants
from ansa import utils
from itertools import combinations
from timeit import default_timer as timer


deck = constants.NASTRAN
m = utils.Messenger()

BOOL_ADDED_EXTRACT_NAME = "United"


def printAllCardFieldsOfEntity(entity):
    fields = entity._cardFields(deck)
    print(base.GetEntityCardValues(deck, entity, fields))


def createDefaultSets():
    global Set_walls
    global Set_interfaces
    global Set_uncategorized

    Set_walls = getOrCreateNamedEntity(Set_walls, "SET")
    Set_interfaces = getOrCreateNamedEntity(Set_interfaces, "SET")
    Set_uncategorized = getOrCreateNamedEntity(Set_uncategorized, "SET")


def fixGeomErrors():
    base.CheckAndFixGeometry(0, ["UNCHECKED FACES", "NEEDLE FACES"], [1, 1])


def getNamedEntities(name, ent_type):
    ents = []
    try:
        for entity in base.NameToEnts(name, deck, constants.ENM_EXACT):
            if base.GetEntityType(deck, entity) == ent_type:
                ents.append(entity)
        if ents:
            return ents
    except:
        pass


def getNamedEntity(name, ent_type):
    try:
        ents = getNamedEntities(name, ent_type)
        if ents:
            return ents[0]
    except:
        pass


def getOrCreateNamedEntity(name, ent_type):
    try:
        entity = getNamedEntity(name, ent_type)
        if entity:
            return entity
    except:
        pass
    entity = base.CreateEntity(deck, ent_type, {"Name": name})
    return entity


def offsetVisFacePropName(
    input_shells=None, pre_string="", post_string="", remove_string=""
):
    shell_list = []
    if not input_shells:
        input_shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)

    for shell in input_shells:
        new_shell_name = (pre_string + shell._name + post_string).replace(
            remove_string, ""
        )
        new_shell = getOrCreateNamedEntity(new_shell_name, "PSHELL")
        new_shell_id = new_shell._id
        shell_list.append(new_shell)
        for face in base.CollectEntities(deck, shell, "FACE", filter_visible=True):
            base.SetEntityCardValues(deck, face, {"PID": new_shell_id})
    return shell_list


def mergePIDSWithSameName():
    offsetVisFacePropName()
    base.Compress("")
    pass


def getFilteredCurveList(filter_str):
    return [
        curve
        for curve in base.CollectEntities(deck, None, "CURVE", filter_visible=False)
        if curve._name.startswith(filter_str)
    ]


def nearCurves():
    curveList = []
    for curve in getFilteredCurveList("Intersect"):
        curveList.append(curve)
    base.Or(curveList)
    try:
        base.Near(
            radius=0.1,
            dense_search=False,
        )
        base.Neighb("3")
    except:
        pass


def topoRedCons(cons=[], near_non_red=False, paste_different_pids=False):
    if not cons:
        cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)

    red_cons = []
    for con in cons:
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            == 1
        ):
            red_cons.append(con)

    try:
        base.Topo(
            cons=red_cons,
            paste_with_frozen_faces=False,
            paste_different_pids=paste_different_pids,
        )
        if near_non_red:
            base.ConsMatchingDistance(0.01)
            base.Near(
                radius=0.1,
                dense_search=False,
            )
            base.Neighb("2")
            base.Topo(
                paste_with_frozen_faces=False, paste_different_pids=paste_different_pids
            )
            base.ConsMatchingDistance(0.2)

    except:
        pass


def removeDuplicatesInSamePID():
    init_vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)

    all_vis_shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
    all_named_shells = [
        shell for shell in all_vis_shells if shell._name != BOOL_ADDED_EXTRACT_NAME
    ]

    for shells in all_named_shells:
        try:
            faces = base.CollectEntities(deck, shells, "FACE", filter_visible=False)
            base.Or(faces)
            len_vis_faces = len(
                base.CollectEntities(deck, None, "FACE", filter_visible=True)
            )
            for i in range(100):
                base.RmdblSameSide(0.1, 90, "positive", "delete")
                if (
                    len(base.CollectEntities(deck, None, "FACE", filter_visible=True))
                    == len_vis_faces
                ):
                    break
                len_vis_faces = len(
                    base.CollectEntities(deck, None, "FACE", filter_visible=True)
                )
            topoRedCons(near_non_red=False)
        except:
            pass

    base.Or(init_vis_faces)


def performTopoNearIntersect():
    nearCurves()
    base.Topo(paste_different_pids=True)
    base.ConsMatchingDistance(0.01)
    base.Topo(paste_different_pids=True)
    base.ConsMatchingDistance(0.2)


def getPIDSOfCon(con=None):
    faces = base.GetFacesOfCons(con)
    PIDs_of_con = []
    for face in faces:
        pid = base.GetEntityCardValues(deck, face, ["PID"])["PID"]
        if pid not in PIDs_of_con:
            PIDs_of_con.append(pid)
    return PIDs_of_con


def releaseMultiPIDCons():
    cons_to_release = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            > 2
        ):
            PIDs_of_con = getPIDSOfCon(con)
            if len(PIDs_of_con) > 2:
                cons_to_release.append(con)
    base.ReleaseCons(cons_to_release)


def releaseAndTopoTripleConsOnly():
    base.Neighb("1")
    existing_red_cons = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            == 1
        ):
            existing_red_cons.append(con)

    cons_to_release = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            > 2
        ):
            cons_to_release.append(con)
    base.ReleaseCons(cons_to_release)

    cons_to_topo = []
    for con in base.CollectEntities(deck, None, "CONS", filter_visible=True):
        if (
            base.GetEntityCardValues(deck, con, ["Number of Pasted Cons"])[
                "Number of Pasted Cons"
            ]
            == 1
        ):
            if con not in existing_red_cons:
                cons_to_topo.append(con)
            
    topoRedCons(cons=cons_to_topo, paste_different_pids=False)


@ansa.session.defbutton("NXPS_Tools", "Make_PIDS")
def Make_PIDS():
    print("\nStarting operation...")
    start = timer()
    base.BlockRedraws(True)
    base.ConsMatchingDistance(0.2)
    base.Compress("")
    base.All()

    print("\n    Orienting... ")
    base.Orient()
    print("\n    Fixing geom...")
    fixGeomErrors()
    print("\n    Merging pids with same name...")
    mergePIDSWithSameName()
    # print("\n    Creating default sets...")
    # createDefaultSets()
    print("\n    Removing duplicate faces in same PID...")
    removeDuplicatesInSamePID()
    print("\n    Performing Topo near Intersect Curves")
    performTopoNearIntersect()
    print("\n    Releasing CONS connected to 3 or more PIDS")
    releaseMultiPIDCons()
    print("\n    Releasing and Performing Topo on Triple Cons within PIDS")
    releaseAndTopoTripleConsOnly()
    ## print("\n    Refining perims pass 1...")
    ## refinePerims()
    # print("\n    Seeding walls... ")
    # seedWalls()
    # print("\n    Extracting all walls and non walls... ")
    # getWallsAndNonWalls()
    # print("\n    Get simulation interfaces... ")
    # getSimulationInterfaces()
    # print("\n    Clean up and topo... ")
    # cleanUpAndPerformTopo()
    # print("\nRefining perims...")
    # refinePerims()
    # print("\nFixing geom...")
    # fixGeomErrors()

    # base.All()
    base.Compress("")
    base.ConsMatchingDistance(0.2)
    base.BlockRedraws(False)
    end = timer()
    print("\nOperation took {} seconds".format(end - start))


@ansa.session.defbutton("CATProcessing", "UnblockRedraw")
def UnblockRedraw():
    base.BlockRedraws(False)


def main():
    pass


if __name__ == "__main__":
    main()
