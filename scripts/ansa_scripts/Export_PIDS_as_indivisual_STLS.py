# PYTHON script
import os
import ansa
from ansa import base
from ansa import constants
import time

deck = constants.NASTRAN


def main():
	# Need some documentation? Run this with F5
	input_shells = base.CollectEntities(deck, None, "PSHELL", filter_visible=True)
	for shell in input_shells:
		base.Or(shell)
		base.OutputStereoLithography(filename="{}.stl".format(shell._name), mode="visible", scale=1.0)
		print(shell._name)

if __name__ == '__main__':
	main()


