# PYTHON script
import os
import ansa
from ansa import base
from ansa import constants
from ansa import morph
from ansa import calc

deck = constants.NASTRAN

def main():	
	#ref_face = base.GetEntity(constants.NASTRAN, "FACE", 18416)
	#ref_point = base.GetEntity(constants.NASTRAN, "POINT", 6)
	
	#FLAGS
	convert_to_m = 1
	create_delete_both = 1 # 0, 1, 2
	offset_from_point_mm = 1 # +ve = towards gray side, -ve = yellow side (KEEP YELLOW OUTSIDE (+ve value) WHEN GETTING INJECTION DIR)
	
	ref_face = base.PickEntities(deck, "FACE")
	try:
		ref_point = base.PickEntities(deck, "POINT")
		ref_point = ref_point[0]
	except:
		base.Or(ref_face[0])
		arg1 = base.Cog(ref_face[0])
		base.PointsRelative(relative=arg1)
		ref_point = base.CollectEntities(deck, None, "POINT", filter_visible=True)
		ref_point = ref_point[0]
	
	if convert_to_m:
		scaling = 1000
	else:
		scaling = 1
	ref_face_normal = [x/scaling for x in base.GetFaceOrientation(ref_face)]
	ref_point_coords_m = [x/scaling for x in ref_point.position]

	new_point_coords = [ref_point_coords_m[0] + offset_from_point_mm*ref_face_normal[0], ref_point_coords_m[1] + offset_from_point_mm*ref_face_normal[1], ref_point_coords_m[2] + offset_from_point_mm*ref_face_normal[2]]
	
	print("direction (" + str(ref_face_normal[0]*scaling) + " " + str(ref_face_normal[1]*scaling) + " " + str(ref_face_normal[2]*scaling) + ");\n" + ("position (" + str(new_point_coords[0]) + " " + str(new_point_coords[1]) + " " + str(new_point_coords[2]) + ");"))
	
    if create_delete_both == 0:	
		base.CreateEntity(deck, "POINT", {"X":new_point_coords[0]*scaling, "Y":new_point_coords[1]*scaling, "Z":new_point_coords[2]*scaling})	
    elif create_delete_both == 1:	
		base.DeleteEntity(ref_point)
	elif create_delete == 2:
		base.CreateEntity(deck, "POINT", {"X":new_point_coords[0]*scaling, "Y":new_point_coords[1]*scaling, "Z":new_point_coords[2]*scaling})
		base.DeleteEntity(ref_point)
	
		
		
if __name__ == '__main__':
	main()
