# PYTHON script
import os
import ansa
import math
from ansa import base
from ansa import constants
from ansa import morph
from ansa import calc
from ansa import guitk

deck = constants.NASTRAN


def get_offset(offset_from_point, ref_point_coords, ref_face_normal, scaling):
    new_point_coords = [
        ref_point_coords[0] + offset_from_point * ref_face_normal[0],
        ref_point_coords[1] + offset_from_point * ref_face_normal[1],
        ref_point_coords[2] + offset_from_point * ref_face_normal[2],
    ]
    created_point = base.CreateEntity(
        deck,
        "POINT",
        {
            "X": new_point_coords[0] * scaling,
            "Y": new_point_coords[1] * scaling,
            "Z": new_point_coords[2] * scaling,
        },
    )
    return created_point, new_point_coords


def common_hardvalues():
    convert_to_m_hardvalues = [0, 1, 1]
    offset_from_point_mm_hardvalues = [5.0, 11.0, 1.0]
    return convert_to_m_hardvalues, offset_from_point_mm_hardvalues


def get_pt(out_type):
    convert_to_m, offset_from_point_mm = [i[out_type] for i in common_hardvalues()]

    if convert_to_m:
        scaling = 1000
    else:
        scaling = 1

    try:
        while True:
            ref_face = base.PickEntities(deck, "FACE")
            ref_face_normal = [x / scaling for x in base.GetFaceOrientation(ref_face)]

            try:
                ref_point = base.PickEntities(deck, "POINT")
                ref_point = ref_point[0]
                ref_point_coords_m = [x / scaling for x in ref_point.position]
            except:
                ref_point = base.PickPointOnSurface(ref_face)
                ref_point_coords_m = [x / scaling for x in ref_point[0]]

            while True:
                try:
                    answer = guitk.UserInput(
                        "ENTER: offset in mm [" + str(offset_from_point_mm) + "]", ""
                    )
                    if answer != "":
                        offset_from_point_mm = float(answer)
                except:
                    continue
                break
            created_point, new_point_coords = get_offset(
                offset_from_point_mm, ref_point_coords_m, ref_face_normal, scaling
            )
            container = base.CreateEntity(deck, "HIGHLIGHT_CONTAINER")
            base.AddToHighlight(container, created_point, ("GREEN"))
            base.RedrawAll()
            response = 0
            while not response:
                response = guitk.UserQuestion("Is the direction correct?")
                if not response:
                    base.DeleteEntity(created_point, True)
                    offset_from_point_mm = -offset_from_point_mm
                    created_point, new_point_coords = get_offset(
                        offset_from_point_mm,
                        ref_point_coords_m,
                        ref_face_normal,
                        scaling,
                    )
                    base.AddToHighlight(container, created_point, ("GREEN"))
                    base.RedrawAll()
            base.DeleteEntity(created_point, True)
    except:
        pass
    return (
        new_point_coords,
        scaling,
        [i * offset_from_point_mm for i in ref_face_normal],
    )


def locInMesh(new_pt_coords):
    try:
        while True:
            answer = guitk.UserInput("ENTER: Name", "")
            if answer != "":
                break
    except:
        pass
    text = (
        "\n	( ( "
        + str(new_pt_coords[0])
        + " "
        + str(new_pt_coords[1])
        + " "
        + str(new_pt_coords[2])
        + " ) "
        + answer
        + " )"
    )
    return text


def Inj(new_pt_coords, injdir):
    injdir = [
        i / (math.sqrt(injdir[0] ** 2 + injdir[1] ** 2 + injdir[2] ** 2))
        for i in injdir
    ]
    text = (
        "direction ("
        + str(injdir[0])
        + " "
        + str(injdir[1])
        + " "
        + str(injdir[2])
        + ");\n"
        + (
            "position ("
            + str(new_pt_coords[0])
            + " "
            + str(new_pt_coords[1])
            + " "
            + str(new_pt_coords[2])
            + ");"
        )
    )
    return text


def topo(new_pt_coords, scaling, injdir):
    thickness_mm = 5
    while True:
        try:
            answer = guitk.UserInput(
                "ENTER: thicknes of topoSet in mm [" + str(thickness_mm) + "]", ""
            )
            if answer != "":
                thickness_mm = float(answer)
        except:
            continue
        break
    topodir = [
        i
        * thickness_mm
        / (math.sqrt(injdir[0] ** 2 + injdir[1] ** 2 + injdir[2] ** 2) * scaling)
        for i in injdir
    ]
    text = (
        "\n\t\t\tp1\t("
        + str(new_pt_coords[0] + topodir[0])
        + " "
        + str(new_pt_coords[1] + topodir[1])
        + " "
        + str(new_pt_coords[2] + topodir[2])
        + ");\n\t\t\tp2\t("
        + str(new_pt_coords[0] - topodir[0])
        + " "
        + str(new_pt_coords[1] - topodir[1])
        + " "
        + str(new_pt_coords[2] - topodir[2])
        + ");"
    )
    return text


def main():
    out_type = 0
    lim_text = ""
    injdir_text = ""
    topo_text = ""
    try:
        while True:
            answer = guitk.UserInput(
                "ENTER: '0' for locationInMesh, '1' for topoSetDict, '2' for Injection",
                "",
            )
            if answer == "0" or answer == "1" or answer == "2":
                out_type = int(answer)
                new_pt_coords, scaling, inj_dir = get_pt(out_type)
                if answer == "0":
                    lim_text += locInMesh(new_pt_coords)
                elif answer == "1":
                    topo_text += topo(new_pt_coords, scaling, inj_dir)
                else:
                    injdir_text = Inj(new_pt_coords, inj_dir)
            elif answer == "":
                break
    except:
        pass
    print(lim_text)
    print(injdir_text)
    print(topo_text)


if __name__ == "__main__":
    main()
