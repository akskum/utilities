# PYTHON script
# Step1 - MODEL ID
##          OLD/REF - 0
##          NEW - 1
# Step2 - Window
##          'Window 1' 'Window2'

import os
import random
import time

import ansa
from ansa import base, constants

deck = constants.NASTRAN


@ansa.session.defbutton("Select.All", "SelectConnectedWithinPID.All")
def pid_selected_connected_within_pid_all():
    new_model = base.GetCurrentAnsaModel()
    base.SetCurrentAnsaModel(new_model)
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    base.Or(pid1_picker)
    for i in range(1000):
        old_len_visible_faces = len(
            base.CollectEntities(deck, None, "FACE", filter_visible=True)
        )
        base.Neighb("1")
        vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
        faces_in_pid = []
        for face in vis_faces:
            face_pid = base.GetEntityCardValues(deck, face, ("PID",))
            if face_pid == pid1:
                faces_in_pid.append(face)
        base.Or(faces_in_pid)
        vis_faces2 = base.CollectEntities(deck, None, "FACE", filter_visible=True)
        if (
            len(base.CollectEntities(deck, None, "FACE", filter_visible=True))
            == old_len_visible_faces
        ):
            break


@ansa.session.defbutton("Select.All", "SelectConnectedWithRedAndYellowCONS.All")
def pid_selected_connected_with_red_yellow_cons_all():
    new_model = base.GetCurrentAnsaModel()
    base.SetCurrentAnsaModel(new_model)
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    base.Or(pid1_picker)
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    end_loop = False
    for j in range(100):
        old_visible_faces = base.CollectEntities(
            deck, None, "FACE", filter_visible=True
        )
        old_len_visible_faces = len(old_visible_faces)
        vis_consA = base.CollectEntities(deck, None, "CONS", filter_visible=True)
        connected_faces = []
        for i in range(len(vis_cons)):
            connected_faces_iter = base.GetFacesOfCons(vis_cons[i])
            if len(connected_faces_iter) < 3:
                old_visible_faces = list(set(old_visible_faces + connected_faces_iter))
        # 			else:
        # 				end_loop = True
        # 				base.StoreLockView("lock", True)
        # 				base.ZoomInEnt(connected_faces_iter)
        # 				base.Or(connected_faces_iter)
        # 				break
        # 		if(end_loop):
        # 			break
        base.Or(old_visible_faces)
        new_visible_faces = base.CollectEntities(
            deck, None, "FACE", filter_visible=True
        )
        # base.Not(old_visible_faces)
        # vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
        # base.Or(new_visible_faces)
        vis_consB = base.CollectEntities(deck, None, "CONS", filter_visible=True)
        vis_cons = list(set(vis_consB) - set(vis_consA))
        if (
            len(base.CollectEntities(deck, None, "FACE", filter_visible=True))
            == old_len_visible_faces
        ):
            break


@ansa.session.defbutton("Select.Single", "SelectConnectedWithinPID.Single")
def pid_selected_connected_within_pid_single():
    new_model = base.GetCurrentAnsaModel()
    base.SetCurrentAnsaModel(new_model)
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    base.Or(pid1_picker)
    old_len_visible_faces = len(
        base.CollectEntities(deck, None, "FACE", filter_visible=True)
    )
    base.Neighb("1")
    vis_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    faces_in_pid = []
    for face in vis_faces:
        face_pid = base.GetEntityCardValues(deck, face, ("PID",))
        if face_pid == pid1:
            faces_in_pid.append(face)
    base.Or(faces_in_pid)
    vis_faces2 = base.CollectEntities(deck, None, "FACE", filter_visible=True)


@ansa.session.defbutton("Select.Single", "SelectConnectedWithRedAndYellowCONS.Single")
def pid_selected_connected_with_red_yellow_cons_single():
    new_model = base.GetCurrentAnsaModel()
    base.SetCurrentAnsaModel(new_model)
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    base.Or(pid1_picker)
    vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    end_loop = False
    old_visible_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    old_len_visible_faces = len(old_visible_faces)
    vis_consA = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    connected_faces = []
    for i in range(len(vis_cons)):
        connected_faces_iter = base.GetFacesOfCons(vis_cons[i])
        if len(connected_faces_iter) < 3:
            old_visible_faces = list(set(old_visible_faces + connected_faces_iter))
    # 			else:
    # 				end_loop = True
    # 				base.StoreLockView("lock", True)
    # 				base.ZoomInEnt(connected_faces_iter)
    # 				base.Or(connected_faces_iter)
    # 				break
    # 		if(end_loop):
    # 			break
    base.Or(old_visible_faces)
    new_visible_faces = base.CollectEntities(deck, None, "FACE", filter_visible=True)
    # base.Not(old_visible_faces)
    # vis_cons = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    # base.Or(new_visible_faces)
    vis_consB = base.CollectEntities(deck, None, "CONS", filter_visible=True)
    vis_cons = list(set(vis_consB) - set(vis_consA))


@ansa.session.defbutton("PID", "GetAndSet")
def pid_get_and_set():
    old_model = base.GetEntity(deck, "MODEL", 0)
    new_model = base.GetEntity(deck, "MODEL", 1)
    base.SetCurrentAnsaModel(old_model)
    pshells_in_old = base.CollectEntities(deck, None, "PSHELL")
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    base.Or(shell)
    pid1_name = shell._name
    base.SetCurrentAnsaModel(new_model)
    pid_exists = 0
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    pid2_id = len(pshells_in_new) + 1
    for pshell in pshells_in_new:
        if pshell._name == pid1_name:
            pid_exists = 1
            pid2 = pshell
            pid2_id = pid2._id
    if not pid_exists:
        pid2 = base.CreateEntity(deck, "PSHELL", {"PID": pid2_id, "Name": pid1_name})
    pid2_picker = base.PickEntities(deck, "FACE")
    for pid in pid2_picker:
        base.SetEntityCardValues(deck, pid, {"PID": pid2_id})
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    ansa.base.SetCurrentAnsaWindow(base.GetAnsaWindow("Window 1"))
    base.All()
    ansa.base.SetCurrentAnsaWindow(base.GetAnsaWindow("Window 2"))
    base.All()
    ents = []
    for pshell_new in pshells_in_new:
        for pshell_old in pshells_in_old:
            if pshell_new._name == pshell_old._name:
                ents.append(pshell_new)
                ents.append(pshell_old)
    base.Not(ents)


@ansa.session.defbutton("PID", "GetAndSetAll")
def pid_get_and_set_all():
    for i in range(1000):
        pid_get_and_set()


@ansa.session.defbutton("PID", "DefaultAll")
def pid_default_all():
    new_model = base.GetEntity(deck, "MODEL", 1)
    base.SetCurrentAnsaModel(new_model)
    default_pid = base.CreateEntity(deck, "PSHELL", {"PID": 672351, "Name": "default"})
    face_in_new = base.CollectEntities(deck, None, "FACE")
    for face in face_in_new:
        base.SetEntityCardValues(deck, face, {"PID": 672351})
    base.Compress("")


@ansa.session.defbutton("PID", "RedoID")
def pid_redo_idl():
    # new_model = base.GetEntity(deck, "MODEL", 1)
    # base.SetCurrentAnsaModel(new_model)
    pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
    for counter, pshell in enumerate(pshells_in_new):
        base.SetEntityCardValues(deck, pshell, {"PID": counter + 1})
    base.Compress("")


@ansa.session.defbutton("PID", "copyPicked")
def copy_visible():
    old_model = base.GetEntity(deck, "MODEL", 0)
    new_model = base.GetEntity(deck, "MODEL", 1)

    base.SetCurrentAnsaModel(old_model)
    picked_faces = base.PickEntities(deck, "FACE")

    rand_id = str(random.randint(1, 100000))
    to_copy_part = base.NewPart(rand_id, rand_id)
    base.SetEntityPart(picked_faces, to_copy_part)

    base.CopyPartsToAnsaModel(to_copy_part, new_model)
    base.SetCurrentAnsaModel(new_model)


@ansa.session.defbutton("PID", "AppendTo")
def pid_append():
    # new_model = base.GetEntity(deck, "MODEL", 1)
    # base.SetCurrentAnsaModel(new_model)
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1_dict = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell1 = base.GetEntity(deck, "PSHELL", pid1_dict["PID"])
    base.Not(shell1)
    pid1 = shell1._id
    pid2_picker = base.PickEntities(deck, "FACE")
    for pid in pid2_picker:
        base.SetEntityCardValues(deck, pid, {"PID": pid1})
    base.Not(pid2_picker)
    base.RedrawAll()


# @ansa.session.defbutton('PID', 'Match')
# def pid_match():
# 	old_model = base.GetEntity(deck, 'MODEL',  0)
# 	base.SetCurrentAnsaModel(old_model)
# 	pshells_in_old = base.CollectEntities(deck, None, "PSHELL")
# 	new_model = base.GetEntity(deck, 'MODEL',  1)
# 	base.SetCurrentAnsaModel(new_model)
# 	pshells_in_new = base.CollectEntities(deck, None, "PSHELL")
# 	ansa.base.SetCurrentAnsaWindow(base.GetAnsaWindow("Window 1"))
# 	base.Or([])
# 	ansa.base.SetCurrentAnsaWindow(base.GetAnsaWindow("Window 2"))
# 	base.Or([])
# 	ents = []
# 	for pshell_new in pshells_in_new:
# 		for pshell_old in pshells_in_old:
# 			if pshell_new._name == pshell_old._name:
# 				base.Or(ents)
# 				base.Or(ents)
# 				time.sleep(5)
# 				base.Or([])


@ansa.session.defbutton("PID", "Name")
def pid_name():
    old_model = base.GetEntity(deck, "MODEL", 0)
    base.SetCurrentAnsaModel(old_model)
    pid1_picker = base.PickEntities(deck, "FACE")
    pid1 = base.GetEntityCardValues(deck, pid1_picker[0], ("PID",))
    shell = base.GetEntity(deck, "PSHELL", pid1["PID"])
    pid1_name = shell._name
    print(pid1_name)


def main():
    print(0)


if __name__ == "__main__":
    main()
