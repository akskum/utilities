#!/bin/bash

VER=`echo $_LMFILES_ | tr ':' '\n' | grep "/ansa/" | rev | cut -f1 -d'/' | rev`
[[ $VER == "" ]] && die "No ansa module loaded. Load it first"
ANSA_HOME=/grp/techsim/crash/apps/BETA_CAE_Systems/ansa_$VER/

$ANSA_HOME/ansa64.sh -nogui -foregr -lm_retry 10 -i $1 -exec 'RunAllTasks();'

