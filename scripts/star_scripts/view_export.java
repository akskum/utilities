// STAR-CCM+ macro: view_export.java
// Written by Akshay Kumar akucwh
// Exports current view to CAMxxxxx format

package macro;

import java.util.*;
import java.io.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class view_export extends StarMacro {

  public void execute() {
    try {
    	execute0();
    } catch(IOException e){
    	//
    }
  }

  private void execute0() throws IOException{

    //GetCamera
    Simulation simulation_0 = 
      getActiveSimulation();

    Scene scene_0 = 
      simulation_0.getSceneManager().getSelectedScene();

    CurrentView currentView_0 = 
      scene_0.getCurrentView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));
      
    DoubleVector originalViewUp = currentView_0.getViewUpCoordinate().getValue();
    
    double originalViewUp_0 = originalViewUp.getComponent(0);
    double originalViewUp_1 = originalViewUp.getComponent(1);
    double originalViewUp_2 = originalViewUp.getComponent(2);    
    
    DoubleVector originalFocalPoint = currentView_0.getFocalPointCoordinate().getValue();
    
    double originalFocalPoint_0 = originalFocalPoint.getComponent(0);
    double originalFocalPoint_1 = originalFocalPoint.getComponent(1);
    double originalFocalPoint_2 = originalFocalPoint.getComponent(2);
    
    DoubleVector originalPosition = currentView_0.getPositionCoordinate().getValue();
    
    double originalPosition_0 = originalPosition.getComponent(0);
    double originalPosition_1 = originalPosition.getComponent(1);
    double originalPosition_2 = originalPosition.getComponent(2);
    
    boolean isPar = currentView_0.isParallelProjection();
    
    double angle_scale;
    
    if (isPar){
    	angle_scale = currentView_0.getParallelScale().getValue();
    } else {
    	angle_scale = currentView_0.getViewAngle().getValue();
    }

    //Read
    BufferedReader chosenTemplate;
    if(isPar){
        chosenTemplate = new BufferedReader(new FileReader("/home/akucwh/apps/utilities/scripts/star_scripts/view_maker_parallel.java.txt"));
    } else {
    	chosenTemplate = new BufferedReader(new FileReader("/home/akucwh/apps/utilities/scripts/star_scripts/view_maker_perspective.java.txt"));
    }
    
    StringBuilder subTemplate = new StringBuilder();
    String subTemplateLine;
    while ((subTemplateLine = chosenTemplate.readLine()) != null) {
	    subTemplate.append(subTemplateLine);
            subTemplate.append(System.lineSeparator());
    }
    chosenTemplate.close();
    
    String subTemplateString = subTemplate.toString();
    String subTemplateFormatted = String.format(subTemplateString, angle_scale);
    
    int num = 0;
    String camName = "CAM" + 0;
    File file = new File("/home/akucwh/apps/utilities/scripts/star_scripts/views_star/", camName +".java");
    while(file.exists()) {
       camName = "CAM" + (num++);
       file = new File("/home/akucwh/apps/utilities/scripts/star_scripts/views_star/", camName +".java"); 
    }
    
    
    BufferedReader fileReader = new BufferedReader(new FileReader("/home/akucwh/apps/utilities/scripts/star_scripts/view_maker.java.txt"));
    
    StringBuilder sb = new StringBuilder();

    String line;
    while ((line = fileReader.readLine()) != null) {
	    sb.append(line);
            sb.append(System.lineSeparator());
    }
    
    String template = sb.toString();

    //Write
    FileWriter fileWriter = new FileWriter("/home/akucwh/apps/utilities/scripts/star_scripts/views_star/" + camName + ".java");
    
    PrintWriter printWriter = new PrintWriter(fileWriter);  
     
    printWriter.printf(template, camName, originalFocalPoint_0, originalFocalPoint_1, originalFocalPoint_2, originalPosition_0, originalPosition_1, originalPosition_2, originalViewUp_0, originalViewUp_1, originalViewUp_2, subTemplateFormatted, camName);
    
    printWriter.close();
  }
    
}
