// Simcenter STAR-CCM+ macro: createSceneDisplayersByGeometryPart.java
// Written by Simcenter STAR-CCM+ 15.04.008
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.base.query.*;
import star.vis.*;
import star.meshing.*;

public class createSceneDisplayersByGeometryPart extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation = 
      getActiveSimulation();

    Scene scene = 
      simulation.getSceneManager().getScene("Geometry Scene 1");

    Vector meshParts_all = 
        ((Vector) simulation.get(SimulationPartManager.class).getParts());

    for(int i=0; i<meshParts_all.size(); i++)
    {
        MeshPart meshPart = (MeshPart) meshParts_all.get(i);
        String meshPartName = meshPart.toString();

        PartDisplayer partDisplayer = 
          scene.getDisplayerManager().createPartDisplayer("Surface", -1, 1);

        partDisplayer.initialize();

        partDisplayer.setColorMode(PartColorMode.DR);

        PartRepresentation partRepresentation = 
          ((PartRepresentation) simulation.getRepresentationManager().getObject("Geometry"));

        partDisplayer.setRepresentation(partRepresentation);

        partDisplayer.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new TypePredicate(TypeOperator.Is, PartSurface.class), new InheritedPartPredicate(InheritedPartOperator.Inherits, Arrays.<ClientServerObject>asList(meshPart)))), Query.STANDARD_MODIFIERS));

        partDisplayer.setPresentationName(meshPartName);
    }
  }
}
