// STAR-CCM+ macro: star_replace.java
// Written by STAR-CCM+ 14.02.010
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.meshing.*;

public class star_make_existing_old extends StarMacro {

    public void execute() {
        execute0();
    }

    private void execute0() {

        Simulation simulation_0 = 
            getActiveSimulation();

        simulation_0.println("********************");
        simulation_0.println("   Starting Macro   ");
        simulation_0.println("********************");
        simulation_0.println("");
        Units units_0 = 
            simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        Vector meshParts_all = 
            ((Vector) simulation_0.get(SimulationPartManager.class).getParts());
        Vector regions_all = 
            ((Vector) simulation_0.getRegionManager().getRegions());

        for(int i=0; i<meshParts_all.size(); i++)
        {
            MeshPart meshPart = (MeshPart) meshParts_all.get(i);
            String meshPartName = meshPart.toString();
            meshPart.setPresentationName(meshPartName + "_old");
        }
        for(int i=0; i<regions_all.size(); i++)
        {
            Region region = (Region) regions_all.get(i);
            String regionName = region.toString();
            region.setPresentationName(regionName + "_old");
        }
    }
}

