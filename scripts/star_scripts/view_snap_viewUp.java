// STAR-CCM+ macro: view_snap_viewUp.java
// Written by Akshay kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class view_snap_viewUp extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    Scene scene_0 = 
      simulation_0.getSceneManager().getSelectedScene();

    CurrentView currentView_0 = 
      scene_0.getCurrentView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));
      
    DoubleVector originalViewUp = currentView_0.getViewUpCoordinate().getValue();
    
    double originalViewUp_0 = originalViewUp.getComponent(0);
    double originalViewUp_1 = originalViewUp.getComponent(1);
    double originalViewUp_2 = originalViewUp.getComponent(2);
    
    double newViewUp_0 = 0;
    double newViewUp_1 = 0;
    double newViewUp_2 = 0;
    
    if (originalViewUp_0 > 0.8) {
    	newViewUp_0 = 1;
    } else if (originalViewUp_0 < -0.8) {
    	newViewUp_0 = -1;
    }
    
    if (originalViewUp_1 > 0.8) {
    	newViewUp_1 = 1;
    } else if (originalViewUp_1 < -0.8) {
    	newViewUp_1 = -1;
    }
    
    if (originalViewUp_2 > 0.8) {
    	newViewUp_2 = 1;
    } else if (originalViewUp_2 < -0.8) {
    	newViewUp_2 = -1;
    }
    
    currentView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {newViewUp_0, newViewUp_1, newViewUp_2}));
    
  }
}
