// STAR-CCM+ macro: star_replace.java
// Written by STAR-CCM+ 14.02.010
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.meshing.*;

public class star_replace extends StarMacro {

    public void execute() {
        execute0();
    }

    private void execute0() {

        Simulation simulation_0 = 
            getActiveSimulation();

        Units units_0 = 
            simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        Vector meshPart_all = 
            ((Vector) simulation_0.get(SimulationPartManager.class).getParts());

        for(int i=0; i<meshPart_all.size(); i++)
        {
            MeshPart meshPart_a = (MeshPart) meshPart_all.get(i);
            String part_a = meshPart_a.toString(); 
            meshPart_a.setPresentationName(part_a + "_old");
        }

        PartImportManager partImportManager_0 = 
            simulation_0.get(PartImportManager.class);
        partImportManager_0.importStlPart(resolvePath("./geo.stl"), "OneSurfacePerPatch", units_0, true, 1.0E-5);

        meshPart_all = 
            ((Vector) simulation_0.get(SimulationPartManager.class).getParts());
        MeshPart meshPart_0 = (MeshPart) meshPart_all.get(0);

        for(int i=0; i<meshPart_all.size(); i++)
        {
            MeshPart meshPart_a = (MeshPart) meshPart_all.get(i);
            String part_a = meshPart_a.toString(); 
            if(!part_a.contains("_old"))
            {
                meshPart_0 = meshPart_a;
            }
        }
        String stlname = meshPart_0.toString();

        for(int i=0; i<meshPart_all.size(); i++)
        {
            MeshPart meshPart_a = (MeshPart) meshPart_all.get(i);
            String part_a = meshPart_a.toString();
            Vector surface_part_a_all = 
                ((Vector) meshPart_a.getPartSurfaceManager().getPartSurfaces());

            if(part_a != stlname)
            {
                MeshPart meshPart_b = 
                    (MeshPart) meshPart_0.duplicatePart(simulation_0.get(SimulationPartManager.class));
                Vector surface_part_b_all = 
                    ((Vector) meshPart_b.getPartSurfaceManager().getPartSurfaces());

                for(int j=0; j<surface_part_b_all.size(); j++)
                {
                    PartSurface surface_part_bb = (PartSurface) surface_part_b_all.get(j);
                    String surface_bb = surface_part_bb.toString().split("\\.")[1];
                    int flag = 1;
                    for(int k=0; k<surface_part_a_all.size(); k++)
                    {
                        PartSurface surface_part_ab = (PartSurface) surface_part_a_all.get(k);
                        String surface_ab = surface_part_ab.toString().split("\\.")[1];
                        if(surface_bb.equals(surface_ab))
                        {
                            flag = 0;
                        }
                    }
                    if(flag == 1)
                        meshPart_b.deleteMeshPartSurfaces(new NeoObjectVector(new Object[] {surface_part_bb}));
                }
                meshPart_b.setPresentationName(part_a.substring(0, part_a.length() - 4));
            }
        }
        simulation_0.println("");
        simulation_0.println("EXTRA IN OLD:");
        for(int i=0; i<meshPart_all.size(); i++)
        {
            MeshPart meshPart_a = (MeshPart) meshPart_all.get(i);
            String part_a = meshPart_a.toString();
            Vector surface_part_a_all =
                ((Vector) meshPart_a.getPartSurfaceManager().getPartSurfaces());
            
            if(part_a.contains("_old"))
            {
                simulation_0.print("\t");
                for(int spaces = part_a.length(); spaces<20; spaces++)
                    simulation_0.print(" ");
                simulation_0.print((part_a.substring(0, part_a.length() - 4)).toUpperCase() + ":");
                MeshPart meshPart_b = 
                    ((MeshPart) simulation_0.get(SimulationPartManager.class).getPart(part_a.substring(0, part_a.length() - 4)));
                Vector surface_part_b_all =
                    ((Vector) meshPart_b.getPartSurfaceManager().getPartSurfaces());
                for(int j=0; j<surface_part_a_all.size(); j++)
                {
                    int exists = 0;
                    PartSurface surface_part_ab = (PartSurface) surface_part_a_all.get(j);
                    String surface_ab = surface_part_ab.toString().split("\\.")[1];
                    for(int k=0; k<surface_part_b_all.size(); k++)
                    {
                        PartSurface surface_part_bb = (PartSurface) surface_part_b_all.get(k);
                        String surface_bb = surface_part_bb.toString().split("\\.")[1];
                        if(surface_bb.equals(surface_ab))
                            exists = 1;
                    }
                    if(exists!=1)
                        simulation_0.print(" " + surface_ab);
                }
                simulation_0.println("");
            }
        }
        simulation_0.println("");
        simulation_0.println("EXTRA IN NEW:");
        Vector surface_part_0_all =
             ((Vector) meshPart_0.getPartSurfaceManager().getPartSurfaces());
        for(int j=0; j<surface_part_0_all.size(); j++)
        {
            int exists = 0;
            PartSurface surface_part_0b = (PartSurface) surface_part_0_all.get(j);
            String surface_0b = surface_part_0b.toString().split("\\.")[1];
            for(int i=0; i<meshPart_all.size(); i++)
            {
                MeshPart meshPart_a = (MeshPart) meshPart_all.get(i);
                String part_a = meshPart_a.toString();
                Vector surface_part_a_all =
                    ((Vector) meshPart_a.getPartSurfaceManager().getPartSurfaces());
                if(part_a.contains("_old"))
                {
                    for(int k=0; k<surface_part_a_all.size(); k++)
                    {
                        PartSurface surface_part_ab = (PartSurface) surface_part_a_all.get(k);
                        String surface_ab = surface_part_ab.toString().split("\\.")[1];
                        if(surface_0b.equals(surface_ab))
                            exists = 1;
                    }
                }
            }
            if(exists!=1)
                simulation_0.print(" " + surface_0b);
        }
    }
}
