// STAR-CCM+ macro: star_replace.java
// Written by STAR-CCM+ 14.02.010
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.meshing.*;

public class star_add_substrate_parts extends StarMacro {

    public void execute() {
        execute0();
    }

    private void execute0() {

        Simulation simulation_0 = 
            getActiveSimulation();

        simulation_0.println("********************");
        simulation_0.println("   Starting Macro   ");
        simulation_0.println("********************");
        simulation_0.println("");
        Units units_0 = 
            simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        Vector meshParts_all = 
            ((Vector) simulation_0.get(SimulationPartManager.class).getParts());

        for(int i=0; i<meshParts_all.size(); i++)
        {
            MeshPart meshPart = (MeshPart) meshParts_all.get(i);
            String meshPartName = meshPart.toString();
            if(!meshPartName.contains("_old" ))
            {
                simulation_0.println(meshPartName);
                Vector partSurface_all = 
                    ((Vector) meshPart.getPartSurfaceManager().getPartSurfaces());
                for(int j=0; j<partSurface_all.size(); j++)
                {
                    PartSurface partSurface = (PartSurface) partSurface_all.get(j);
                    String[] partSurfaceSplitName = partSurface.toString().split("\\.");
                    String partSurfaceName = partSurfaceSplitName[partSurfaceSplitName.length - 1];
                    simulation_0.println(partSurfaceName);
                    switch (partSurfaceName){
                        case "wall_scr_1a":
                            meshPart.setPresentationName("SCR1a");
                            break;
                        case "wall_scr_1b":
                            meshPart.setPresentationName("SCR1b");
                            break;
                        case "wall_scr_2":
                            meshPart.setPresentationName("SCR2");
                            break;
                        case "wall_scr_3":
                            meshPart.setPresentationName("SCR3");
                            break;
                        case "wall_asc_1":
                            meshPart.setPresentationName("ASC1");
                            break;
                        case "wall_asc_2":
                            meshPart.setPresentationName("ASC2");
                            break;
                        case "wall_dpf":
                            meshPart.setPresentationName("DPF");
                            break;
                        case "wall_flanges_inner":
                            meshPart.setPresentationName("Flanges");
                            break;
                        case "ref_plane_outlet":
                            meshPart.setPresentationName("Reference_PlaneOutlet");
                            break;
                    }
                }
            }
        }
    }
}
