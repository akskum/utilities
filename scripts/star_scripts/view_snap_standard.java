// STAR-CCM+ macro: view_snap_standard.java
// Written by Akshay Kumar akucwh
// Snaps to nearest orthogonal view

package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class view_snap_standard extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    Scene scene_0 = 
      simulation_0.getSceneManager().getSelectedScene();

    CurrentView currentView_0 = 
      scene_0.getCurrentView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));
      
    DoubleVector originalViewUp = currentView_0.getViewUpCoordinate().getValue();
    
    double originalViewUp_0 = originalViewUp.getComponent(0);
    double originalViewUp_1 = originalViewUp.getComponent(1);
    double originalViewUp_2 = originalViewUp.getComponent(2);
    
    double newViewUp_0 = 0;
    double newViewUp_1 = 0;
    double newViewUp_2 = 0;
    
    if (originalViewUp_0 > 0.8) {
    	newViewUp_0 = 1;
    } else if (originalViewUp_0 < -0.8) {
    	newViewUp_0 = -1;
    }
    
    if (originalViewUp_1 > 0.8) {
    	newViewUp_1 = 1;
    } else if (originalViewUp_1 < -0.8) {
    	newViewUp_1 = -1;
    }
    
    if (originalViewUp_2 > 0.8) {
    	newViewUp_2 = 1;
    } else if (originalViewUp_2 < -0.8) {
    	newViewUp_2 = -1;
    }
    
    currentView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {newViewUp_0, newViewUp_1, newViewUp_2}));
    
    
    DoubleVector originalFocalPoint = currentView_0.getFocalPointCoordinate().getValue();
    
    double originalFocalPoint_0 = originalFocalPoint.getComponent(0);
    double originalFocalPoint_1 = originalFocalPoint.getComponent(1);
    double originalFocalPoint_2 = originalFocalPoint.getComponent(2);
    
    DoubleVector originalPosition = currentView_0.getPositionCoordinate().getValue();
    
    double originalPosition_0 = originalPosition.getComponent(0);
    double originalPosition_1 = originalPosition.getComponent(1);
    double originalPosition_2 = originalPosition.getComponent(2);
    
    double diff_0 = Math.abs((originalFocalPoint_0 - originalPosition_0)/originalFocalPoint_0);
    double diff_1 = Math.abs((originalFocalPoint_1 - originalPosition_1)/originalFocalPoint_1);
    double diff_2 = Math.abs((originalFocalPoint_2 - originalPosition_2)/originalFocalPoint_2);
    
    double newFocalPoint_0 = 0;
    double newFocalPoint_1 = 0;
    double newFocalPoint_2 = 0;
    
    if (diff_0 >= diff_1 && diff_0 >= diff_2) {
    	newFocalPoint_0 = originalFocalPoint_0;
	newFocalPoint_1 = originalPosition_1;
	newFocalPoint_2 = originalPosition_2;
    } else if (diff_1 >= diff_2 && diff_1 >= diff_0) {
    	newFocalPoint_0 = originalPosition_0;
	newFocalPoint_1 = originalFocalPoint_1;
	newFocalPoint_2 = originalPosition_2;
    } else if (diff_2 >= diff_0 && diff_2 >= diff_1) {
    	newFocalPoint_0 = originalPosition_0;
	newFocalPoint_1 = originalPosition_1;
	newFocalPoint_2 = originalFocalPoint_2;
    }
    
    currentView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {newFocalPoint_0, newFocalPoint_1, newFocalPoint_2}));
    
    scene_0.resetCamera();
  }
}
