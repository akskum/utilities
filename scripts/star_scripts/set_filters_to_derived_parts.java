// STAR-CCM+ macro: temp.java
// Written by STAR-CCM+ 15.02.007
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.base.query.*;
import star.vis.*;

public class temp extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    simulation_0.println("********************************************************************************");
    simulation_0.println("********************************* BEGIN MACRO **********************************");
    simulation_0.println("********************************************************************************");
    simulation_0.println("");

    Vector derivedParts_all = (Vector) simulation_0.getPartManager().getChildren();
    //simulation_0.println("** Derived Parts are **");
    //simulation_0.println("-----------------------");
    for  (int i = 0; i < derivedParts_all.size(); i++) {
       simulation_0.println(derivedParts_all.get(i).toString());
    } 
    simulation_0.println("");

    Vector arbitrarySection_all = new Vector<>();
    Vector constrainedPlaneSection_all = new Vector<>();
    Vector isoPart_all = new Vector<>();
    Vector planeSection_all = new Vector<>();
    Vector pointPart_all = new Vector<>();
    Vector streamPart_all = new Vector<>();
    Vector thresholdPart_all = new Vector<>();

    for (int i = 0; i < derivedParts_all.size(); i++) {
        Part derivedPart = (Part) derivedParts_all.get(i);
        String derivedPartClass = derivedPart.getClass().getSimpleName();
        switch(derivedPartClass) {
            case "ArbitrarySection":
                arbitrarySection_all.addElement(derivedPart.toString());
                break;
            case "ConstrainedPlaneSection":
                constrainedPlaneSection_all.addElement(derivedPart.toString());
                break;
            case "IsoPart":
                isoPart_all.addElement(derivedPart.toString());
                break;
            case "PlaneSection":
                planeSection_all.addElement(derivedPart.toString());
                break;
            case "PointPart":
                pointPart_all.addElement(derivedPart.toString());
                break;
            case "StreamPart":
                streamPart_all.addElement(derivedPart.toString());
                break;
            case "ThresholdPart":
                thresholdPart_all.addElement(derivedPart.toString());
                break;
        }
    }

    Vector<String> searchParts = new Vector<>();
    searchParts.addElement("CutPlane_Box2_Longitudinal");
    searchParts.addElement("CutPlane_EvapPipe_Longitudinal1");
    searchParts.addElement("CutPlane_EvapPipe_Longitudinal2");
    searchParts.addElement("CutPlane_EvapPipe_TransverseSlices");
    searchParts.addElement("CutPlane_MidPipe_TransverseSlices");
    searchParts.addElement("MFA_Ptot_AcousticInsertIn");
    searchParts.addElement("MFA_Ptot_AcousticInsertOut");
    searchParts.addElement("MFA_Ptot_EvapPipeIn");
    searchParts.addElement("MFA_Ptot_EvapPipeOut");
    searchParts.addElement("MFA_Ptot_ExhaustBrake");
    searchParts.addElement("MFA_Ptot_InletPipeEnd");
    searchParts.addElement("MFA_Ptot_MidPipeIn");
    searchParts.addElement("MFA_Ptot_MidPipeOut");
    searchParts.addElement("MFA_Ptot_Outlet");
    searchParts.addElement("MFA_Ptot_OutletPipeIn");
    searchParts.addElement("Point_UreaInjection");
    searchParts.addElement("Streamline _UreaInjection");
    searchParts.addElement("Threshold_2ndBoxEntrance");
    //searchParts.addElement("CutPlane_Substrate_SCR2_11mm");
    //searchParts.addElement("CutPlane_Substrate_SCR3_11mm");
    
    String regionName = "exhaust";
    
    for (int i = 0; i<searchParts.size(); i++) {
        String searchPart = searchParts.get(i);
        if (constrainedPlaneSection_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is a ConstrainedPlaneSection.");
            ConstrainedPlaneSection constrainedPlaneSection = ((ConstrainedPlaneSection) simulation_0.getPartManager().getObject(searchPart));
            constrainedPlaneSection.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
        if (planeSection_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is a PlaneSection.");
            PlaneSection planeSection = ((PlaneSection) simulation_0.getPartManager().getObject(searchPart));
            planeSection.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
        if (arbitrarySection_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is an ArbitrarySection.");
            ArbitrarySection arbitrarySection = ((ArbitrarySection) simulation_0.getPartManager().getObject(searchPart));
            arbitrarySection.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
        if (pointPart_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is a PointPart.");
            PointPart pointPart = ((PointPart) simulation_0.getPartManager().getObject(searchPart));
            pointPart.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
        if (streamPart_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is a StreamPart.");
            StreamPart streamPart = ((StreamPart) simulation_0.getPartManager().getObject(searchPart));
            streamPart.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
        if (thresholdPart_all.contains(searchPart)) {
            simulation_0.println(searchPart + " is a ThresholdPart.");
            ThresholdPart thresholdPart = ((ThresholdPart) simulation_0.getPartManager().getObject(searchPart));
            thresholdPart.getInputParts().setQuery(new Query(new CompoundPredicate(CompoundOperator.And, Arrays.<QueryPredicate>asList(new NamePredicate(NameOperator.Matches, regionName), new TypePredicate(TypeOperator.Is, Region.class))), Query.STANDARD_MODIFIERS));
        }
    }

  }
}
