// STAR-CCM+ macro: star_replace.java
// Written by STAR-CCM+ 14.02.010
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.meshing.*;

public class star_sync_surfaces_boundaries_list extends StarMacro {

    public void execute() {
        execute0();
    }

    private void execute0() {

        Simulation simulation_0 = 
            getActiveSimulation();

        simulation_0.println("********************");
        simulation_0.println("   Starting Macro   ");
        simulation_0.println("********************");
        simulation_0.println("");
        Units units_0 = 
            simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

        Vector meshParts_all = 
            ((Vector) simulation_0.get(SimulationPartManager.class).getParts());
        Vector<String> meshPartNames_all = new Vector<>(); 
        Vector<String> extraMeshPartNames = new Vector<>(); 

        Vector regions_all = 
            ((Vector) simulation_0.getRegionManager().getRegions());
        Vector<String> regionNames_all = new Vector<>(); 
        Vector<String> extraRegionNames = new Vector<>(); 

        Vector<String> commonPartRegionNames = new Vector<>(); 

        for(int i=0; i<meshParts_all.size(); i++)
        {
            MeshPart meshPart = (MeshPart) meshParts_all.get(i);
            String meshPartName = meshPart.toString();
            meshPartNames_all.addElement(meshPartName);
        }
        for(int i=0; i<regions_all.size(); i++)
        {
            Region region = (Region) regions_all.get(i);
            String regionName = region.toString();
            regionNames_all.addElement(regionName);
        }
        extraMeshPartNames = (Vector) meshPartNames_all.clone();
        extraRegionNames = (Vector) regionNames_all.clone();
        commonPartRegionNames = (Vector) meshPartNames_all.clone(); 

        extraMeshPartNames.removeAll(regionNames_all);
        extraRegionNames.removeAll(meshPartNames_all);
        commonPartRegionNames.retainAll(regionNames_all);


        simulation_0.println("There are " + extraMeshPartNames.size() + " parts that do not have a region with same name");
        for(int i=0; i<extraMeshPartNames.size(); i++)
        {
            simulation_0.println(extraMeshPartNames.get(i));
        }
        simulation_0.println("");
        simulation_0.println("There are " + extraRegionNames.size() + " regions that do not have a part with same name");
        for(int i=0; i<extraRegionNames.size(); i++)
        {
            simulation_0.println(extraRegionNames.get(i));
        }
        simulation_0.println("");
        simulation_0.println("There are " + commonPartRegionNames.size() + " pairs of parts-regions with the same name");
        for(int i=0; i<commonPartRegionNames.size(); i++)
        {
            simulation_0.println(commonPartRegionNames.get(i));
        }
        simulation_0.println("");
        simulation_0.println("The part surfaces assigned to boundaries with different names are:");
        for(int i=0; i<commonPartRegionNames.size(); i++)
        {
            MeshPart meshPart = 
                ((MeshPart) simulation_0.get(SimulationPartManager.class).getPart(commonPartRegionNames.get(i)));
            String meshPartName = meshPart.toString();
            Vector partSurface_all = 
                ((Vector) meshPart.getPartSurfaceManager().getPartSurfaces());
            simulation_0.println(meshPartName + ":");
            for(int j=0; j<partSurface_all.size(); j++)
            {
                PartSurface partSurface = (PartSurface) partSurface_all.get(j);
                String partSurfaceName = partSurface.toString().split("\\.")[1];
                Boundary boundary = (Boundary) partSurface.getBoundary();
                String boundaryName = boundary.toString().split("\\: ")[1];
                if (!partSurfaceName.equals(boundaryName))
                    simulation_0.println("      " + partSurfaceName);
            }
        }
        //simulation_0.println("");
        //simulation_0.println("The boundaries with empty assignments are:");
        //for(int i=0; i<commonPartRegionNames.size(); i++)
        //{
        //    Region region = 
        //        ((Region) simulation_0.getRegionManager().getRegion(commonPartRegionNames.get(i)));
        //    String regionName = region.toString();
        //    Vector boundary_all = 
        //        ((Vector) region.getBoundaryManager().getBoundaries());
        //    simulation_0.println(regionName + ":");
        //    for(int j=0; j<boundary_all.size(); j++)
        //    {
        //        Boundary boundary = (Boundary) boundary_all.get(j);
        //        String boundaryName = boundary.toString().split("\\: ")[1];
        //        PartSurfaceGroup partSurfaceGroup = (PartSurfaceGroup) boundary.getManager().getPartSurfacesPropertyGroupInput();
        //        String partSurfaceGroupName = partSurfaceGroup.toString();
        //        simulation_0.println(partSurfaceGroupName);
        //    //    String boundaryName = boundary.toString().split("\\: ")[1];
        //    //    if (!partSurfaceName.equals(boundaryName))
        //    //        simulation_0.println("      " + partSurfaceName);
        //    }
        //}
    }
}
