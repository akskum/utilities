// STAR-CCM+ macro template: view_maker.java.txt
// Written by Akshay Kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class CAM5 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    VisView visView_0 = 
      simulation_0.getViewManager().createView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));

    visView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.963088, 0.121937, 0.750852}));

    visView_0.getPositionCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {-6.648681, 9.094787, 5.359293}));

    visView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.235826, -0.278173, 0.931132}));

    visView_0.setProjectionMode(VisProjectionMode.PARALLEL);
ParallelScale parallelScale_0 = 
visView_0.getParallelScale();
parallelScale_0.setValue(1.785574);



    visView_0.setPresentationName("CAM5");
  }
}
