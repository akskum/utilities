// STAR-CCM+ macro template: view_maker.java.txt
// Written by Akshay Kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class CAM3 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    VisView visView_0 = 
      simulation_0.getViewManager().createView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));

    visView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.618539, 0.869488, 1.226929}));

    visView_0.getPositionCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {4.883351, -1.557951, 4.452893}));

    visView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {-0.479090, 0.268764, 0.835607}));

    visView_0.setProjectionMode(VisProjectionMode.PARALLEL);
ParallelScale parallelScale_0 = 
visView_0.getParallelScale();
parallelScale_0.setValue(0.249985);



    visView_0.setPresentationName("CAM3");
  }
}
