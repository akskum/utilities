// STAR-CCM+ macro template: view_maker.java.txt
// Written by Akshay Kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class CAM1 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    VisView visView_0 = 
      simulation_0.getViewManager().createView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));

    visView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {-0.605150, 3.624253, 0.835826}));

    visView_0.getPositionCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {3.351260, 1.571247, 1.744810}));

    visView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.000000, 0.000000, 1.000000}));

    visView_0.setProjectionMode(VisProjectionMode.PARALLEL);
ParallelScale parallelScale_0 = 
visView_0.getParallelScale();
parallelScale_0.setValue(0.137736);



    visView_0.setPresentationName("CAM1");
  }
}
