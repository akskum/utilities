// STAR-CCM+ macro template: view_maker.java.txt
// Written by Akshay Kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class CAM2 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    VisView visView_0 = 
      simulation_0.getViewManager().createView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));

    visView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.709507, 0.785421, 0.847542}));

    visView_0.getPositionCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {5.052455, -1.686492, 4.132610}));

    visView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {-0.479090, 0.268764, 0.835607}));

    visView_0.setProjectionMode(VisProjectionMode.PARALLEL);
ParallelScale parallelScale_0 = 
visView_0.getParallelScale();
parallelScale_0.setValue(0.261161);



    visView_0.setPresentationName("CAM2");
  }
}
