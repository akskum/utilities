// STAR-CCM+ macro template: view_maker.java.txt
// Written by Akshay Kumar akucwh
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;

public class CAM6 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    VisView visView_0 = 
      simulation_0.getViewManager().createView();

    Units units_0 = 
      ((Units) simulation_0.getUnitsManager().getObject("m"));

    visView_0.getFocalPointCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {0.599922, 1.042337, -0.154820}));

    visView_0.getPositionCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {6.253631, -0.872527, -1.353205}));

    visView_0.getViewUpCoordinate().setCoordinate(units_0, units_0, units_0, new DoubleVector(new double[] {-0.311091, -0.949117, 0.048974}));

    visView_0.setProjectionMode(VisProjectionMode.PARALLEL);
ParallelScale parallelScale_0 = 
visView_0.getParallelScale();
parallelScale_0.setValue(0.117407);



    visView_0.setPresentationName("CAM6");
  }
}
