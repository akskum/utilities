#! /bin/bash
# Generates Foam PDF reports given yaml files

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    # echo
    # echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# ---------------
# Checking inputs
# ---------------

[[ "$PWD" == "ppt" ]] && die "Please execute from ppt dir"
[[ "$#" -eq 0 ]] && die "Please specify at least one yaml file"
inputs=$@

for input in $inputs
do
    [[ $input == "-grid" ]] && grid="--grid" 
    [[ $input == "-unlimited" ]] && unlimited="--unlimited"
done

for input in $inputs
do
    [[ $input == "-grid" ]] && break
    [[ -f $input ]] || die "Please specify a valid yaml file"
    yaml_file=$input
    pdf_file_temp=PDF__${input##*/}
    pdf_file=${pdf_file_temp%%.*}.pdf

    python /home/akucwh/git/utilities/scripts/python/foamPDFReportGen.py $yaml_file $grid $unlimited
    evince $pdf_file
    windows $pdf_file
done
