#! /bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    # echo
    # echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# ---------------
# Checking inputs
# ---------------
[[ "$#" -eq 2 ]] || die "Please give two dirs"

inp_dir1="$1"
[[ -d $inp_dir1 ]] || die "inp_dir1 is not a dir"

inp_dir2="$2"
[[ -d $inp_dir2 ]] || die "inp_dir2 is not a dir"

dir1=`readlink -f $inp_dir1`
dir2=`readlink -f $inp_dir2`


for files in $(diff -rq $dir1 $dir2 | grep 'differ$' | sed "s/^Files //g;s/ differ$//g;s/ and /:/g"); do 
    /home/akucwh/miniconda3/bin/vim -d ${files%:*} ${files#*:}; 
done

for files in $(diff -rq $dir1 $dir2 | grep '^Only' | sed "s/^Only in //g;s/: /\//g"); do 
    echo ${files}
    fileindir1=${files#*$dir1}
    fileindir2=${files#*$dir2}

    [[ $files != $fileindir1 ]] && fileInDir="$fileindir1" && existsDir="$dir1" && notExistsDir="$dir2"
    [[ $files != $fileindir2 ]] && fileInDir="$fileindir2" existsDir="$dir2" && notExistsDir="$dir1"
    # echo $fileInDir
    # echo $existsDir
    # echo $notExistsDir
    /home/akucwh/miniconda3/bin/vim -d $existsDir$fileInDir $notExistsDir$fileInDir
done
