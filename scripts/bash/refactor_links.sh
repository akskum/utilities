#! /bin/bash


make_no_sync="false"
make_abs="false"
exec_dir=$PWD


locate_links_folder()
{
    cd $exec_dir
    while [[ ! $links_dir ]]
    do
        [[ -d "links" ]] && links_dir=$PWD/links || cd ../
        [[ $PWD == "/" ]] && die "No links dir found"
    done
    [[ $links_dir ]] || die "No links dir found"

    cd $exec_dir
}


get_paths()
{
    src_dir_relpath=`dirname $input_linkFile`
    cd $src_dir_relpath
    src_dir_abspath=$PWD

    target_file_relpath=`ls -l $input_linkFile | awk '{print $NF}'`
    target_file_basename=`basename $target_file_relpath`
    target_dir_relpath=`dirname $target_file_relpath`
    cd $target_dir_relpath
    target_file_abspath=$PWD/$target_file_basename
    target_file_relpath=`realpath --relative-to="$links_dir" "$target_file_abspath"`

    cd $exec_dir
}

# ----------------
# Feedback to user
# ----------------

# print help info
usage () {
    exec 1>&2
    while [[ "$#" -ge 1 ]]; do echo "$1"; shift; done
    cat <<USAGE
Usage: ${0##*/} filename [OPTIONS]
options:
  -ns   | -nosync                        refactor into a noSync link
  -a    | -abs                           points to absolute link instead
  -h    | -help                          prints help message

Takes the supplied filename (must be a symlink) and refactors it to the closest 'links' folder

USAGE
    exit 1
}

# die message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [[ "$#" -ge 1 ]]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# ---------------
# Input from user
# ---------------

# parse options
unset optDebug optEnvName optStrip optVerbose
while [[ "$#" -gt 0 ]]
do
    case "$1" in
    -h | -help*)
        usage
        ;;
    -ns | -nosync)
        make_no_sync="true"
        ;;
    -a | -abs)
        make_abs="true"
        ;;
    *)
        [[ ! $input_linkFile ]] || die "Too many filenames supplied!"
        [[ -f $1 ]] && input_linkFile=$1
        link_file_supplied="true"
        ;;
    esac
    shift
done

[[ $input_linkFile ]] || die "Please specify a link file"

[[ -L $input_linkFile ]] || die "Please specify a valid link file"

locate_links_folder
get_paths

[[ $make_abs == "true" ]] && new_target_path=$target_file_abspath || new_target_path=$target_file_relpath
[[ $make_no_sync == "true" ]] && link1=$target_file_basename".nosync" || link1=$target_file_basename
link2=$target_file_basename

local_target_file_relpath=`realpath --relative-to="$src_dir_relpath" "$links_dir"`/$link2

cd $links_dir
ln -s $new_target_path $link1
[[ $link1 != $link2 ]] && ln -sf $link1 $link2
cd $src_dir_abspath
ln -sf $local_target_file_relpath $link2

cd $exec_dir
