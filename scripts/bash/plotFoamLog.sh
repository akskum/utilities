#! /bin/bash


# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE
Usage: ${0##*/} logfile [OPTIONS]

options:
  -gp               <pattern>           pattern to grep for
  -last             <number>            only use last <number> rows

Does stuff.

USAGE
    exit 1
}

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


grepPattern=""


[[ "$#" -gt 0 ]] || die "args not supplied"
# parse options
unset optDebug optEnvName optStrip optVerbose
while [[ "$#" -gt 0 ]]
do
    case "$1" in
    -h | -help*)
        usage
        ;;
    -gp )
        [ "$#" -ge 2 ] || die "'-gp' requires atleast one pattern"
        [[ "$2" != "-"* ]] || die "'-gp' requires atleast one pattern"
        while [ "$#" -gt 0 ]
        do
            if [[ "$2" == "-"* ]] || [[ -f "$2" ]]
            then
                break
            else
                grepPattern="$grepPattern "\"$2\"
                shift
            fi
        done
        ;;
    -last )
        [ "$#" -ge 2 ] || die "'-last' requires one argument"
        [[ "$2" != "-"* ]] || die "'-last' requires one argument"
        [[ "$2" -gt 0 ]] || die "argument to '-last' must be > 0"
        last=$2
        shift
        ;;
    *)
        [[ -f "$logfile" ]] && die "unknown option"
        [[ -f "$1" ]] || die "supplied filename not valid"
        logfile=$1
        ;;
    esac
    shift
done

[[ -f "$logfile" ]] || die "filename not supplied!"

declare -a "patterns=($( echo "$grepPattern" | sed 's/[][`~!@#$%^&*():;<>.,?/\|{}=+-]/\\&/g' ))"

XOPTLIST=(cat $logfile)
YOPTLIST=(cat $logfile)

# simpleReactingParcelFoam.log | grep "^Time = [0-9]*$|uniformity(sampled.transverse_slice_3) of Uav" | grep -A1 "^Time = [0-9]*$" | grep -B1 "uniformity(sampled.transverse_sl/simpleReactingParcelFoam.log | grep "^Time = [0-9]*$\|uniformity(sampled.transverse_slice_3) of Uav" | grep -A1 "^Time = [0-9]*$" | grep -B1 "uniformity(sampled.transverse_slice_3) of Uav" | grep -o "Uav = [0-9\.\-]*" | grep -o "[0-9\.\-]*" | wc -l


for pat in "${patterns[@]}"
do
    if [[ $pat != "" ]]
    then
        pat2=`echo $pat | sed 's/\\\\\\\\/!/g'`
        pat2=`echo \"$pat2\" | sed 's/\\\\//g'`
        pat2=`echo $pat2 | sed 's/!/\\\\/g'`

        if [[ "$pat" == "${patterns[0]}" ]]
        then
            pat3=`echo $pat | sed 's/\\\\\\\\/!/g'`
            pat3=`echo \"^Time = [0-9]*$"!|"$pat3\" | sed 's/\\\\//g'`
            pat3=`echo $pat3 | sed 's/!/\\\\/g'`
            XOPTLIST+=( \| grep $pat3 \-o )
            XOPTLIST+=( \| grep -A1 \"^Time = [0-9]*$\" )
            XOPTLIST+=( \| grep -B1 $pat2 )
            XOPTLIST+=( \| grep \"^Time = [0-9]*$\" \-o )
            XOPTLIST+=( \| grep \"[0-9]*$\" \-o )
        fi

        YOPTLIST+=( \| grep $pat2 \-o )
    fi
done


[[ $last -gt 0 ]] && YOPTLIST+=( \| tail -n $last )
[[ $last -gt 0 ]] && XOPTLIST+=( \| tail -n $last )

echo "${XOPTLIST[@]}"
echo "${YOPTLIST[@]}"
eval "${XOPTLIST[@]}" > temp_time.dat \
    && eval "${YOPTLIST[@]}" > temp_y.dat \
    && gnuplot -e "plot '<paste temp_time.dat temp_y.dat' ; pause -1" \
    && rm temp_time.dat temp_y.dat
