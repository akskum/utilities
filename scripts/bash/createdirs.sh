#! /bin/ksh
# A little script for creating directory structures
umask 0003
typeset Utility
UserName=$(whoami)
mkdir "$UserName"
mkdir "$UserName/PRE" "$UserName/SOL" "$UserName/POST" "$UserName/DOC"
mkdir "$UserName/DOC/REPORT" "$UserName/DOC/REFERENCES" "$UserName/DOC/EMAIL" "$UserName/DOC/PRESENTATIONS"
