#!/bin/bash

grep endsolid $1 | sort > oldstl
grep endsolid $2 | sort > newstl

diff -q newstl oldstl 1>/dev/null
if [[ $? == "0" ]]; then
        echo "same"
else
        echo "different"
fi
rm newstl
rm oldstl
