#! /bin/bash

n_compare=4
du_summary_dir="/nsa/fluidcfd/04_Personal/akucwh/10_NXPS/disk-usage/past_summaries"
colors="blue red green purple"

rm $du_summary_dir/tmp*
echo

RAW_USERS=()
for file in $(ls -tr $du_summary_dir | grep "summary" | grep -v "latest" | grep -v "tmp" | grep -v "svg" | tail -n $n_compare); do
    for row in $(cat $file | grep "<td>[0-9a-zA-Z_]*</td><td>[0-9A-Z\.]*</td><td>TOTAL</td>" -o | sed "s/<td>/ /g;s/<\/td>/ /g;s/^\s\+//g;s/\s\+$//g;s/ TOTAL$//g;s/^\s\+//g;s/\s\+$//g;s/\s\+/:/g"); do
        user=${row%:*}
        RAW_USERS+=($user)
    done
done
USERS=($(printf "%s\n" "${RAW_USERS[@]}" | LC_COLLATE=C sort -u))

for file in $(ls -tr $du_summary_dir | grep "summary" | grep -v "latest" | grep -v "tmp" | grep -v "svg" | tail -n $n_compare); do
    tmpfile="$du_summary_dir/tmp.$file"
    printf "" > $tmpfile
    # for row in $(cat $file | grep "<td>[0-9a-zA-Z_]*</td><td>[0-9A-Z\.]*</td><td>TOTAL</td>" -o | sed "s/<td>/ /g;s/<\/td>/ /g;s/^\s\+//g;s/\s\+$//g;s/ TOTAL$//g;s/^\s\+//g;s/\s\+$//g;s/\s\+/:/g"); do
    for user in "${USERS[@]}"; do
        row=$(cat $file | grep "<td>$user</td><td>[0-9A-Z\.]*</td><td>TOTAL</td>" -o | sed "s/<td>/ /g;s/<\/td>/ /g;s/^\s\+//g;s/\s\+$//g;s/ TOTAL$//g;s/^\s\+//g;s/\s\+$//g;s/\s\+/:/g")
        [[ $row != "" ]] && size=${row#*:} && echo "$(echo $user | sed "s/_/-/g") $(echo $size | numfmt --to=iec-i --from=si --to=none | awk '{print $1/(1024^4)}')" >> $tmpfile
        [[ $row == "" ]] && echo "$(echo $user | sed "s/_/-/g") 0" >> $tmpfile
    done
done

svgfilename=`echo ${file%.*}.svg`
[[ -f $du_summary_dir/$svgfilename ]] && rm $du_summary_dir/$svgfilename

GNUPLOT_CMD=(gnuplot)
GNUPLOT_CMD+=(-e)
GNUPLOT_CMD+=(\")
GNUPLOT_CMD+=(set term svg\;)
GNUPLOT_CMD+=(set output \\\"$du_summary_dir/$svgfilename\\\"\;)
GNUPLOT_CMD+=(set boxwidth 0.9 absolute\;)
GNUPLOT_CMD+=(set style fill solid 1.00 border lt -1\;)
GNUPLOT_CMD+=(set style data histograms\;)
GNUPLOT_CMD+=(set style fill solid 1.00 border lt -1 \;)
GNUPLOT_CMD+=(set key fixed right top vertical Right noreverse noenhanced autotitle nobox\;)
GNUPLOT_CMD+=(set xtics border in scale 0,0 nomirror rotate by -270\;)
GNUPLOT_CMD+=(set xtics norangelimit\;)
GNUPLOT_CMD+=(set ylabel \\\"TiB\\\"\;)

GNUPLOT_CMD+=(plot )
set $colors
for file in $(ls -tr $du_summary_dir | grep "tmp" | tail -n $n_compare); do
    key=$(echo $file | sed "s/tmp.summary.//g;s/.html//g;s/_/-/g")
    GNUPLOT_CMD+=(\\\"$file\\\" using \(\\\$2\)\:xtic\(1\) title \\\"$key\\\" lc \\\"$1\\\"\, )
    shift
done
GNUPLOT_CMD+=(\")
eval "${GNUPLOT_CMD[@]}"

rm $du_summary_dir/tmp*
echo
