#! /bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    # echo
    # echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# ---------------
# Checking inputs
# ---------------
[[ "$#" -eq 1 ]] || die "Please specify a link file"

input_linkFile=$1
[[ -L $input_linkFile ]] || die "Please specify a valid link file"

if [[ `echo $input_linkFile | grep ".nosync" | wc -l` -eq 0 ]] # && echo "this is not a nosyncfile" || echo "this is a nosync file"
then
    noSyncFile=$input_linkFile".nosync"
    [[ -L $noSyncFile ]] && die "Nosync file already exists"

    mv $input_linkFile $noSyncFile
    ln -s $noSyncFile $input_linkFile

else
    noSyncFile=$input_linkFile
    trimmedFile=${noSyncFile%%.nosync*}

    mv $noSyncFile $trimmedFile
fi
