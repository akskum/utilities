#! /bin/ksh
# Script to generate directory structure for week
umask 0003
typeset MySelf='createdirs'
typeset Utility
week="$1"
if [[ $1 == "" ]] then
    echo "Enter a week number!"
else
    userName=$(whoami)
    for (( ; ; ))
    do
        dirName=`basename "$PWD"`
        if [[ $dirName == $userName ]] then
            break
        else
            cd ../
        fi
    done
    for i in CATIA ANSA SURFACE_MESH; do mkdir -p "PRE/$1/$i"; done
    for i in RUN MESH; do mkdir -p "SOL/$i/$1"; done
    for i in REPORT REFERENCES EMAIL PRESENTATIONS; do mkdir -p "DOC/$i/$1"; done
    mkdir -p "POST/$1"
fi
