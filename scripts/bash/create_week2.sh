#! /bin/ksh
# Script to generate directory structure for week
umask 0003
typeset MySelf='createdirs'
typeset Utility
week="$1"
if [[ $1 == "" ]] then
    echo "Enter a week number!"
else
    userName=$(whoami)
    for i in CATIA ANSA SURFACE_MESH; do mkdir -p "$1/PRE/$i"; done
    for i in RUN MESH; do mkdir -p "$1/SOL/$i"; done
    for i in REPORT REFERENCES EMAIL PRESENTATIONS; do mkdir -p "$1/DOC/$i"; done
    mkdir -p "$1/POST"
fi
