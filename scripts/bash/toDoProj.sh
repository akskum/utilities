#!/bin/bash

. ~/bin/bash_scripting_functions.sh

# populate help message
add_to_help_options "-h" "-help" "" "print help message"
help_footer "This script makes/opens the todo list in the most upstream folder
with the same name as the user."

parse_for_help $@

# routine
cwd=$PWD
toDoFile=""

while true ; do
	toDoFileTest=$(readlink -f ./TODO.todo.md)
	owner_of_toDoFile=`stat -c '%U' $toDoFileTest 2>/dev/null`
	owner_of_dir=`stat -c '%U' $PWD 2>/dev/null`

	[[ -e ./TODO.todo.md ]] && [[ $owner_of_toDoFile == $USER ]] && toDoFile=$toDoFileTest && break
	[[ -d .git ]] && [[ $owner_of_dir == $USER ]] && toDoFile=$toDoFileTest && break
    [[ $(basename $PWD) == $USER ]] && [[ $owner_of_dir == $USER ]] && toDoFile=$toDoFileTest && break

	cd ../
	[[ $(dirname $PWD) == "/" ]] && break
done
cd $cwd

[[ $toDoFile != "" ]] && $EDITOR $toDoFile
