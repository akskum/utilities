#!/bin/bash

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "ERROR ENCOUNTERED:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# add to help message
help_args=""
help_footer=""
help_options=""

help_args()
{
    help_args=$1
}

help_footer()
{
    help_footer="$1"
}

add_to_help_options()
{
    help_args="$help_args [OPTIONS]"
    [[ $help_options == "" ]] && help_options="OPTIONS:"
    help_options="$help_options
    $1 | $2 | $3 | $4"
}

help_footer()
{
    help_footer="DESCRIPTION:
$1"
}


# print the help info
usage()
{
    usage_message="USAGE:
${0##*/} $help_args
    "
    [[ $help_options == "" ]] || usage_message="$usage_message
$help_options
"
    [[ $help_footer == "" ]] || usage_message="$usage_message
$help_footer
    "

    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE

$usage_message
USAGE
    exit 1
}


parse_for_help()
{
    help_options=`echo "$help_options" | column -t -s "|" -o"|"`
    for inp in "$@"; do
        [[ $inp == "-help" ]] && usage
    done
}
