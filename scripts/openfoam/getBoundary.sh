list=$(foamDictionary constant/polyMesh/boundary -entry entry0 -keywords)

echo ""
echo "Number of boundaries : "$(echo $list | wc -w)

echo ""

echo "boundaries are: "
for i in $list
do
    echo "    "$i
done
