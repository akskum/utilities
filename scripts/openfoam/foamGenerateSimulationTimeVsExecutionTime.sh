#! /bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}


# ---------------
# Checking inputs
# ---------------

[[ $(basename $PWD) == "logs" ]] || die "must be in logs directory"
[[ "$#" -eq 1 ]] || die "Please specify a solution log file"

input_logFile=$1
output_logFile=TimeVsExecutionTime.$(basename $input_logFile)
output_pngFile=${output_logFile%%.log*}.png
casedir=$(readlink -f $PWD/../)

casedir=${casedir##*SOL/}
casedir=${casedir##*sim/}
casedir=${casedir##*akucwh/}

[[ -f $input_logFile ]] || die "Please specify a valid solution log file"

sed -n "/^Time = /,/^ExecutionTime = /p" $input_logFile | grep -o '^Time = [0-9.e\-]*\|^ExecutionTime = [0-9.]*' | sed 's/^Time = //g' | sed -z 's/\nExecutionTime = / /g' | grep "[0-9.e\-]* [0-9.e\-]*" > $output_logFile

# gnuplot -e " set term png \
#     ;set title '$casedir, twoPhaseEulerFoam.log'  \
#     ;set xlabel 'ExecutionTime [s]' \
#     ;set ylabel 'Time in simulation' \
#     ;unset key \
#     ; plot '$output_logFile' using 2:1 w l" -p \
#     > $output_pngFile
#
# echo $output_pngFile

gnuplot -e "set title '$casedir, twoPhaseEulerFoam.log'  \
    ;set xlabel 'ExecutionTime [s]' \
    ;set ylabel 'Time in simulation' \
    ;unset key \
    ; plot '$output_logFile' using 2:1 w l" -p 
