#!/bin/bash

# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE

Usage: ${0##*/} [OPTIONS] ... <PROJECT_NAME> <APPLICATION> <FROM> <TO> <PATH_TO_ROOT_FOLDER>
options:
  -h    | -help                         help

Generates cases for other operating conditions given project name, application, from and to oprating conditions, and root path. See list for available conditions below.
USAGE
    echo ""
    echo ""
    cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 "\t" $2 "\t" $3}' | uniq
    echo ""
    exit 1
}

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

checkExists()
{
    echo "Checking if $1 $2 $3 exists in database"
    [ $(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3}' | grep "$1 $2 $3" | wc -l) -eq 1 ]\
    || die " $1 $2 $3 does not exist  in database" && echo "Exists!"
    echo""

    echo "Checking if $1 $2 $4 exists in database"
    [ $(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3}' | grep "$1 $2 $4" | wc -l) -eq 1 ]\
    || die " $1 $2 $4 does not exist  in database" && echo "Exists!"
    echo""
}

echo""; echo "********************************************************************************"
echo "Starting script"; echo "-----------------"

project=""
application=""
from=""
to=""

# parse options
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h | -help*)
        usage
        ;;
    *)
        break
        ;;
    esac
    shift
done


# checking inputs
[ "$#" -eq 5 ] || die "not enough arguments (expected 5)"

project=$1
application=$2
from=$3
to=$4

checkExists $project $application $from $to


# extracting data from db
fromMBValue=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $from" | awk '{print $4}')
fromCaseFolder=$from
fromExhaustMassFlow=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $from" | awk '{print $5}')
fromExhaustTemp=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $from" | awk '{print $6}')
fromInjMassFlow=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $from" | awk '{print $7}')
fromRPM=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $from" | awk '{print $8}')
echo "$fromCaseFolder $fromMBValue $fromExhaustMassFlow $fromExhaustTemp $fromInjMassFlow $fromRPM"
echo""

toMBValue=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $to" | awk '{print $4}')
toCaseFolder=$to
toExhaustMassFlow=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $to" | awk '{print $5}')
toExhaustTemp=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $to" | awk '{print $6}')
toInjMassFlow=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $to" | awk '{print $7}')
toRPM=$(cat ~/git/utilities/resources/operating_conditions.dat | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7}' |  grep "$project $application $to" | awk '{print $8}')
echo "$toCaseFolder $toMBValue $toExhaustMassFlow $toExhaustTemp $toInjMassFlow $toRPM"
echo ""

# locating folders
currentDir=$PWD
rootInputPath=$5

if [[ "$rootInputPath" = /* ]]
then
    cd "$rootInputPath"
    rootPath=$PWD
else
    cd "$currentDir/$rootInputPath"
    rootPath=$PWD
fi

#changing Routine
if [ -d $rootPath/$fromCaseFolder ]; then
    echo "Found case $rootPath/$fromCaseFolder"
    cd $rootPath
    echo "Creating case $rootPath/$toCaseFolder"

    mkdir -p $toCaseFolder/0.orig $toCaseFolder/constant $toCaseFolder/system
    touch $toCaseFolder/k.foam

    cp $fromCaseFolder/0.orig/* $toCaseFolder/0.orig/.
    sed -i "/massFlowRate/{s/$fromExhaustMassFlow/$toExhaustMassFlow/}" $toCaseFolder/0.orig/U
    sed -i "/internalField/{s/$fromExhaustTemp/$toExhaustTemp/}" $toCaseFolder/0.orig/T
    sed -i "/value/{s/$fromExhaustTemp/$toExhaustTemp/}" $toCaseFolder/0.orig/T

    cp $fromCaseFolder/constant/* $toCaseFolder/constant/. >/dev/null 2>&1
    cp $fromCaseFolder/constant/triSurface $toCaseFolder/constant/. -r >/dev/null 2>&1
    ln -s ../../$fromCaseFolder/constant/polyMesh $toCaseFolder/constant/.
    sed -i "/massFlowRate/{s/$fromInjMassFlow/$toInjMassFlow/}" $toCaseFolder/constant/reactingCloud1Properties

    cp $fromCaseFolder/system/* $toCaseFolder/system/.

    echo "Created case $rootPath/$toCaseFolder successfully. List of files changed are:"
    [ -f $rootPath/$toCaseFolder/0.orig/U ] && echo "vi -d $rootPath/$fromCaseFolder/0.orig/U $rootPath/$toCaseFolder/0.orig/U"
    [ -f $rootPath/$toCaseFolder/0.orig/T ] && echo "vi -d $rootPath/$fromCaseFolder/0.orig/T $rootPath/$toCaseFolder/0.orig/T"
    [ -f $rootPath/$toCaseFolder/constant/reactingCloud1Properties ] && echo "vi -d $rootPath/$fromCaseFolder/constant/reactingCloud1Properties $rootPath/$toCaseFolder/constant/reactingCloud1Properties"
    echo ""

else
    die "$rootPath/$fromCaseFolder does not exist!"
fi
echo "-------------";echo "End script";
echo""; echo "********************************************************************************"
