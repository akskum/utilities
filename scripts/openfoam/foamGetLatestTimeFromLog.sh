#! /bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

# ---------------
# Checking inputs
# ---------------

[[ $(basename $PWD) == "LOGS" ]] || die "must be in LOGS directory"
[[ "$#" -eq 1 ]] || die "Please specify a solution log file"

input_logFile=$1
[[ -f $input_logFile ]] || die "Please specify a valid solution log file"

tail -n 1000 $input_logFile | grep "^Time = " | tail -n 1
