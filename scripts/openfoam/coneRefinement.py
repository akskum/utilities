import numpy as np
import os
import sys
import re

inj_pos = []
inj_dir = []
with open("inj.dat") as f:
    for line in f:
        if ("position" in line):
            m = re.search('\((.+?)\)', line).group(1)
            m = m.split(" ")
            inj_pos = [float(temp)*1000 for temp in m]
        if ("direction" in line):
            m = re.search('\((.+?)\)', line).group(1)
            m = m.split(" ")
            inj_dir = [float(temp) for temp in m]

cone_start = [temp1 - temp2 for temp1, temp2 in zip(inj_pos, inj_dir)]
cone_end = []
cone_end.append([temp1 + 1000*temp2 for temp1, temp2 in zip(inj_pos, inj_dir)])
cone_end.append([temp1 + 100*temp2 for temp1, temp2 in zip(inj_pos, inj_dir)])
cone_end.append([temp1 + 10*temp2 for temp1, temp2 in zip(inj_pos, inj_dir)])
#print(cone_end)
for i in range(3):
    print("cone_lvl_{}\n{{\n\ttype\t\tsearchableCone;".format(i+2))
    print("\tpoint1\t\t({} {} {});".format(cone_start[0], cone_start[1],cone_start[2]))
    print("\tradius1\t\t10;")
    print("\tinnerRadius1\t0;")
    print("\tpoint2\t\t({} {} {});".format(cone_end[i][0], cone_end[i][1],cone_end[i][2]))
    print("\tradius2\t\t{};".format(10**(i+1)))
    print("\tinnerRadius2\t0;")
    print("}")

for i in range(3):
    print("cone_lvl_{}\n{{".format(i+2))
    print("\tlevels\t\t((1e15 {}));".format(i+2))
    print("\tmode\t\tinside\n};")
