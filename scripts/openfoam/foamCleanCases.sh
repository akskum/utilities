#! /bin/bash

function askAndExecute()
{
    promptToUser="$1"
    commandIfYes="$2"
    commandIfNo="$3"

    read -p "$1 (y/N)?" choice
    case "$choice" in 
        y|Y )
            eval "$commandIfYes"
            return 0
            ;;
        n|N | "" )
            eval "$commandIfNo"
            return 1
            ;;
        * )
            printf "Invalid option! Asking again... "
            askAndExecute "$promptToUser" "$commandIfYes" "$commandIfNo"
            ;;
    esac
}

function removeEntities()
{
    entityType="$1"
    findCMD="$2"

    askAndExecute "Look for $entityType" "true" "return 1"
    [[ "$?" -eq 0 ]] || return 2

    foundEntities=`eval $findCMD`
    [[ `echo $foundEntities | wc -w` -gt 0 ]] || return 2

    echo "Found `echo $foundEntities | wc -w` $entityType:"
    du -sch $foundEntities

    askAndExecute "Remove $entityType" 'rm -rf `eval "$findCMD"`' "return 1"

    echo
}

echo "**********************************************************"
printf "Size before cleaning: "; du -sh
echo "**********************************************************"
removeEntities 'time dirs' 'find . -type d -not -path "*/\.git/*" | grep -e "/[0-9\.]*$" | grep -v -e "/0$" | grep -v -e "/postProcessing/" | grep -v -e "/VTK/"'
removeEntities 'polyMesh dirs' 'find . -type d | grep -e "/polyMesh$"'
removeEntities 'VTK dirs' 'find . -type d | grep -e "/VTK$"'
removeEntities 'postProcessing dirs' 'find . -type d | grep -e "/postProcessing$"'
removeEntities 'core files' 'find . -type f | grep -e "core$"'
removeEntities 'dynamicCode dirs' 'find . -type d | grep -e "dynamicCode"'
removeEntities 'processor* dirs' 'find . -type d | grep -e "processor"'
removeEntities '__pycache__ dirs' 'find . -type d | grep -e "__pycache__"'
removeEntities '*.stl files' 'find . -type f | grep -e ".*\.stl$"'
removeEntities '*.CATPart files' 'find . -type f | grep -e ".*\.CATPart$"'
removeEntities '*.ansa files' 'find . -type f | grep -e ".*\.ansa$"'
removeEntities '*.vtp files' 'find . -type f | grep -e ".*\.vtp$"'
removeEntities '*.vtm files' 'find . -type f | grep -e ".*\.vtm$"'
removeEntities '*.vtk files' 'find . -type f | grep -e ".*\.vtk$"'
removeEntities '*.vtu files' 'find . -type f | grep -e ".*\.vtu$"'
removeEntities 'T*.dat files' 'find . -type f | grep -e "T.*\.dat$"'
removeEntities '*.log files' 'find . -type f | grep -e ".*\.log$"'
removeEntities '*.bak* files' 'find . -type f | grep -e ".*\.bak.*$"'
removeEntities '*.out files' 'find . -type f | grep -e ".*\.out$"'
removeEntities '*.png files' 'find . -type f | grep -e ".*\.png$"'
removeEntities '*.jpg files' 'find . -type f | grep -e ".*\.jpg$"'
removeEntities '*.pdf files' 'find . -type f | grep -e ".*\.pdf$"'
removeEntities '*.eps files' 'find . -type f | grep -e ".*\.eps$"'
removeEntities 'broken symlinks' 'find . -type l -exec test ! -e {} \; -print'
echo "**********************************************************"
printf "Size after cleaning: "; du -sh
echo "**********************************************************"
