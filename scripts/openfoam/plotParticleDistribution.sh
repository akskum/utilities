#!/bin/bash

# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE
Usage: ${0##*/} files

Given dictionary / subdict files with general distribution table for particle size, plots them

USAGE
    exit 1
}

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

[[ "$#" -gt 0 ]] || die "input files not supplied"
inputs=$@

FILES=()
for input in $inputs; do
    [[ -f $input ]] || die "input file $input does not exist"
    FILES+=(`readlink -f $input`)
done

id=0
PLOT_CMD=("gnuplot -e \"plot ")
for file in "${FILES[@]}"; do
    i=$((i+1))
    CMD="grep -Eo '^\s*\(\s*[0-9\.]*[e][+-][0-9]* [0-9\.]*[e][+-][0-9]*\s*\)\s*$' $file | grep -Eo '[0-9\.]*[e][+-][0-9]* [0-9\.]*[e][+-][0-9]*'"
    eval "$CMD" | awk '{print $1*1e6 " " $2}' > tempGnuPlot__$i.dat
    PLOT_CMD+="'tempGnuPlot__$i.dat' w l, "
    echo $i
done
PLOT_CMD+="; pause -1\""
eval "$PLOT_CMD"
rm tempGnuPlot__*dat
