list=$(grep -a -B 2 "type\s\+faceZone;" constant/polyMesh/faceZones | grep -v "{" | grep -v "type\s\+faceZone;" | grep -v "^--$")

echo ""
echo "Number of faceZones : "$(echo $list | wc -w)

echo ""

echo "faceZones are: "
for i in $list
do
    echo "    "$i
done
