list=$(grep -a -B 2 "type\s\+cellZone;" constant/polyMesh/cellZones | grep -v "{" | grep -v "type\s\+cellZone;" | grep -v "^--$")

echo ""
echo "Number of cellZones : "$(echo $list | wc -w)

echo ""

echo "cellZones are: "
for i in $list
do
    echo "    "$i
done
