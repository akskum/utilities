#!/bin/bash

for i in 124
do
(
    dir=$i
    if [ -d $dir ]; then
        echo $i
        cd $dir
        cp -r ../m700_template m700
        sed -e "s/CASENAME/$i/g" ../meshAp_template.sh > m700/meshAp.sh
        sed -e "s/CASENAME/$i/g" ../runAp_template.sh > m700/runAp.sh
    fi
)
done
