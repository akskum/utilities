#! /bin/bash

# Inputs
special_patches="stl_interface_asc_2_out stl_interface_asc_3_out stl_outlet_pressure"
generic_patch="stl_wall_outlet_pipe"

# No need to change here
currentdir_name=$(basename $PWD)
[[ ! $currentdir_name -eq "0" ]] && echo "not in 0 dir!" && exit 1

# Create changeDictionaryDict
changedictFile="../system/changeDictionaryDict"
[[ -f $changedictFile ]] && echo "changeDictionaryDict already exists" && exit 1

{
echo '/*--------------------------------*- C++ -*----------------------------------*\'
echo '| =========                 |                                                 |'
echo '| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |'
echo '|  \\    /   O peration     | Version:  v2106                                 |'
echo '|   \\  /    A nd           | Website:  www.openfoam.com                      |'
echo '|    \\/     M anipulation  |                                                 |'
echo '\*---------------------------------------------------------------------------*/'
echo 'FoamFile'
echo '{'
echo '    version     2.0;'
echo '    format      ascii;'
echo '    class       dictionary;'
echo '    object      changeDictionaryDict;'
echo '}'
echo '// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //'
    for file in `ls`
    do
        echo
        echo ${file%%.j2*}
        echo "{"
        sed -n "/^\s*internalField/p" $file | sed "s/^/    /g"
        echo "boundaryField" | sed "s/^/    /g"
        echo "{" | sed "s/^/    /g"
        for spl_patch in $special_patches
        do
            sed -n "/^\s*$spl_patch\s*$/,/^\s*}\s*$/p" $file | sed "s/^/    /g"
        done
        echo "}" | sed "s/^/    /g"
        echo "}"
        echo
    done
echo
echo '// ************************************************************************* //'
} > $changedictFile

# Create 0.generic
generic_zero_folder="../0.generic"
[[ -d $generic_zero_folder ]] && echo "0.generic already exists" && exit 1 
mkdir $generic_zero_folder

for file in `ls`
do
    case $file in
        U|U\.*)
            foamClass="volVectorField"
            ;;
        *)
            foamClass="volScalarField"
            ;;
    esac
    {
        echo '/*--------------------------------*- C++ -*----------------------------------*\'
        echo '| =========                 |                                                 |'
        echo '| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |'
        echo '|  \\    /   O peration     | Version:  v2106                                 |'
        echo '|   \\  /    A nd           | Website:  www.openfoam.com                      |'
        echo '|    \\/     M anipulation  |                                                 |'
        echo '\*---------------------------------------------------------------------------*/'
        echo 'FoamFile'
        echo '{'
        echo '    version     2.0;'
        echo '    format      ascii;'
        echo "    class       $foamClass;"
        echo '    location    "0";'
        echo "    object      ${file%%.j2*};"
        echo '}'
        echo '// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //'
        echo
        sed -n "/^\s*dimensions/p" $file
        echo
        sed -n "/^\s*internalField/p" $file
        echo
        echo "boundaryField"
        echo "{"
        sed -n "/^\s*$generic_patch\s*$/,/^\s*}\s*$/p" $file | sed "s/$generic_patch/\"\.\*\"/g"
        echo "}"
        echo
        echo
        echo '// ************************************************************************* //'
    } > $generic_zero_folder/$file
done
