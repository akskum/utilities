#!/bin/bash
. ~sssnos/tensorflow/virtualenv/bin/activate
#SCRIPT_DIR=~/tensorflow/dev/bitbucket/utilities/python/openfoam/gui
SCRIPT_DIR=/home/sssnos/OpenFOAM/bitbucket/utilities/python/openfoam/gui
#SCRIPT_DIR=/Users/niklasnordin/tensorflow/dev/bitbucket/utilities/python/openfoam/gui
export PYTHONPATH=/home/sssnos/tensorflow/virtualenv/lib/python2.7/site-packages
#export PYTHONPATH=/Users/niklasnordin/tensorflow/lib/python2.7/site-packages
python $SCRIPT_DIR/openfoamCaseSetup.py $1

