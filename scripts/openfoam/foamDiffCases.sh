#! /bin/bash


[[ "$#" -eq 1 ]] && main_casedir=$1
[[ $main_casedir = "" ]] && [[ -f submitCases.sh ]] && main_casedir=$(grep main_casedir= submitCases.sh | rev | cut -f1 -d= | rev | sed 's/"//g')

[[ $main_casedir = "" ]] && echo "Please submit a main_case_dir as argument" && echo "[OR]" && echo "Place a submitCases.sh in this directory with the entry 'main_casedir'" && exit 1

dirlist=""
[[ -d $main_casedir/0.generic ]] && dirlist="$dirlist 0.generic"
[[ -d $main_casedir/0 ]] && dirlist="$dirlist 0"
[[ -d $main_casedir/constant ]] && dirlist="$dirlist constant"
[[ -d $main_casedir/system ]] && dirlist="$dirlist system"
[[ -d $main_casedir/constant/triSurface ]] && dirlist="$dirlist constant/triSurface"

[[ $dirlist = "" ]] && echo "$main_casedir does not seem to have 0, constant, system etc!" && exit 1

for dir in $dirlist
do
    for i in `cd $main_casedir/$dir; find . -maxdepth 1 -type f`
    do
        # diff3 */$dir/$i && diffcmd="diff3"
        # diff */$dir/$i && diffcmd="diff"
        # [[ $diffcmd = "" ]] && [[ `$diffcmd */$dir/$i |wc -l` -gt 0 ]] && 

        [[ -e ~/miniconda3/bin/vim ]] \
            && ~/miniconda3/bin/vim -d */$dir/$i \
            || vim -d */$dir/$i
    done
done 
