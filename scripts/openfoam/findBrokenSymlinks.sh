#! /bin/bash

function askAndExecute()
{
    promptToUser="$1"
    commandIfYes="$2"
    commandIfNo="$3"

    read -p "$1 (y/N)?" choice
    case "$choice" in 
        y|Y )
            eval "$commandIfYes"
            return 0
            ;;
        n|N | "" )
            eval "$commandIfNo"
            return 1
            ;;
        * )
            printf "Invalid option! Asking again... "
            askAndExecute "$promptToUser" "$commandIfYes" "$commandIfNo"
            ;;
    esac
}

function removeEntities()
{
    entityType="$1"
    findCMD="$2"

    askAndExecute "Look for $entityType" "true" "return 1"

    [[ "$?" -eq 0 ]] || return 2
    [[ `eval "$findCMD" | wc -l` -gt 0 ]] || return 2
    echo "Found $entityType:"
    echo `eval $findCMD`

    askAndExecute "Remove $entityType" 'rm -rf `eval "$findCMD"`' "return 1"
    echo
}

echo "**********************************************************"
removeEntities 'broken symlinks' 'find . -type l -exec test ! -e {} \; -print'
echo "**********************************************************"
