#!/bin/bash

filename="0/k"
string_to_replace="internalField .*"
pre_string_to_replace_with="internalField         uniform "
post_string_to_replace_with=";"
lower_bound=1
upper_bound=1000
step=25

# filename="0/alphat"
# string_to_replace=" Prt .*" #"internalField .*"
# pre_string_to_replace_with=" Prt      0."
# post_string_to_replace_with=";"
# lower_bound=1
# upper_bound=9
# step=1

logfilename=`echo $filename | sed "s/\//_/g"`".log"
echo "" > $logfilename
for i in `seq $lower_bound $step $upper_bound`; do
    sed -i "s/$string_to_replace/$pre_string_to_replace_with$i$post_string_to_replace_with/g" $filename
    ./Allclean > /dev/null 2>&1
    ./Allrun > /dev/null 2>&1

    nIter=`tail -1000 log.rhoSimpleFoam | grep "^Time = " | tail -n 1 | awk '{print $NF}'`
    echo "$i $nIter" >> $logfilename
done
