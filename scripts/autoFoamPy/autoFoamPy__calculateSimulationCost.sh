#!/bin/bash

. $HOME/git/utilities/scripts/bash/bash_scripting_functions.sh

help_footer "This script calculates the sim cost of all sims in
current directory"
parse_for_help $@


logs=""
OVERALL_CPU_HR=0
for logDir in $(find . -type d -name logs); do
    CASE_CPU_HR=0
    for logFile in $(find $logDir -type f -name "*.log"); do
        n_CPUS=$(head -n 2 $logFile | grep "COMMAND TO EXECUTE" | grep -o " -n [0-9]*" | grep -o "[0-9]*")
        [[ $n_CPUS == "" ]] && n_CPUS=1 

        start_clock=$(head -n 1 $logFile | grep "START CLOCK :.*" -o | grep ": .*" -o | sed "s/: //g")
        [[ $start_clock == "" ]] && continue

        end_clock=$(tail -n 1 $logFile | grep "END CLOCK :.*" -o | grep ": .*" -o | sed "s/: //g")
        [[ $end_clock == "" ]] && continue

        sclock_time=$(date --date="$start_clock" +%s)

        run_time=$(echo `date --date="$start_clock" +%s` `date --date="$end_clock" +%s` | awk '{print ($2-$1)/3600}')
        [[ $run_time == "" ]] && continue

        cpu_hr=$(echo $n_CPUS $run_time | awk '{print $1*$2}')
        [[ $cpu_hr == "" ]] && continue

        logs="$logs\nstime : $sclock_time:$logFile : $n_CPUS CPUS : START $start_clock : END $end_clock: $run_time hrs : $cpu_hr CPU-hr"
        # echo "$logFile : $n_CPUS CPUS : START $start_clock : END $end_clock: $run_time hrs : $cpu_hr CPU-hr"
        CASE_CPU_HR=$(echo $CASE_CPU_HR $cpu_hr | awk '{print $1+$2}')
        OVERALL_CPU_HR=$(echo $OVERALL_CPU_HR $cpu_hr | awk '{print $1+$2}')
    done
    echo -e $logs | sort -k 2 | awk '{print}' | cut -f 3- -d ":"
    echo "${logDir%/*} : $CASE_CPU_HR CPU-hr"
    echo
done
echo
echo "OVERALL: $OVERALL_CPU_HR CPU-hr"
