#!/bin/bash

cwd=$PWD
[[ ! -f autoFoamPy_context.yaml ]] && die "context file does not exist in this dir!"

# HELPS WITH CLI
. /home/akucwh/git/utilities/scripts/bash/bash_scripting_functions.sh

# POPULATE HELP MESSAGE
add_to_help_options "--start" "-s" "<int>" "Start of DoE range"
add_to_help_options "--end" "-e" "<int>" "End of DoE range"
add_to_help_options "--pad" "-p" "<int>" "Padding for DoE numbering"
add_to_help_options "--render" "-r" "test/deploy" "autoFoamPy render mode"
help_footer "Generate OpenFOAM DOEs using autoFoamPy"
parse_for_help $@

# INIT INPUTS
padding=1
render_mode="deploy"

# PARSE INPUTS
arglist=""
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    --start | -s)
        shift
        start_id=$1
        ;;
    --end | -e)
        shift
        end_id=$1
        ;;
    --pad | -p)
        shift
        padding=$1
        ;;
    --render-mode | -r)
        shift
        render_mode=$1
        ;;
    *)
        die "invalid argument"
        ;;
    esac
    shift
done

# FINAL CHECKS
[[ $start_id ]] || die "Invalid start_id"
[[ $end_id ]] || die "Invalid end_id"
[[ $padding ]] || die "Invalid padding"
[[ $render_mode ]] || die "Invalid render mode"

doe_ids=`echo $(seq -f '%0'$padding'g' $start_id $end_id) | tr ' ' ','`
# ROUTINE
for i in $(seq -f '%0'$padding'g' $start_id $end_id); do
    [[ -d doe.$i ]] && die "doe.$i already exists!"
    mkdir doe.$i
    cd doe.$i
    doe_dir=$PWD
    sed "s/DOE_ID/$i/g" ../autoFoamPy_context.yaml > autoFoamPy_context.yaml
    mkdir pre
    cd pre
    ln -s ../../pre/templates .
    cd $doe_dir

    autoFoamPy autoFoamPy_context.yaml --render-mode=$render_mode

    sed "s/^doe_dir_list=.*/doe_dir_list=\`echo doe.{$doe_ids}\`/g" submitAllCases.sh > ../submitAllCases.sh
    cd $cwd
done
cd $cwd
chmod u+x submitAllCases.sh
