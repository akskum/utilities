import sys

x = []
y = []
z = []
i = 0
lastline = ""

with open(sys.argv[1], 'r') as f:
    for line in f:
        if "vertex" in line:
            split = line.split()
            x.append(float(split[1]))
            y.append(float(split[2]))
            z.append(float(split[3]))
            i = i + 1;

print("xmin " + str(round((min(x) - 1.0123), 4)) + ";")
print("xmax " + str(round((max(x) + 1.0123), 4)) + ";")
print("ymin " + str(round((min(y) - 1.0123), 4)) + ";")
print("ymax " + str(round((max(y) + 1.0123), 4)) + ";")
print("zmin " + str(round((min(z) - 1.0123), 4)) + ";")
print("zmax " + str(round((max(z) + 1.0123), 4)) + ";")
