#! /home/akucwh/miniconda3/envs/data_analysis/bin/python
# This scipt extracts values from .log files and .dat files (typically foam simulation data)

import os
import re
import subprocess
import sys

import numpy as np
import pandas as pd


# -----------------
# Exception classes
# -----------------


class TooManyLines(Exception):
    pass


class ExceedsThreshold(Exception):
    pass


# ---------
# Functions
# ---------


def get_dir_list_based_on_names(name_startsWith, basedir_absPath):
    dirNamesThatStartsWith = [
        dir_i
        for dir_i in next(os.walk(basedir_absPath))[1]
        if dir_i.startswith(name_startsWith)
    ]
    if name_startsWith != "study__":
        names = sorted(
            [
                dir_i
                for dir_i in dirNamesThatStartsWith
                if re.fullmatch("[0-9]*", dir_i.strip(name_startsWith))  # noqa
            ],
            key=lambda dir_i: int(dir_i.strip(name_startsWith)),
        )
        ids = [name.strip(name_startsWith) for name in names]
    else:
        names = dirNamesThatStartsWith
        ids = [name.replace(name_startsWith, "", 1) for name in names]

    # print(names)
    # print(ids)
    return [names, ids]


def run_routine(
    script_file,
    root_dir,
    inputFile_name,
    resultFile_name,
    merge_on_column,
    workDir_name_startsWith,
    caseDir_name_startsWith,
    EXTRACTION_INSTRUCTIONS,
    max_line_read,
    caseFolderColLabel,
    print_df=False,
    overwrite=False,
):
    scriptDir_absPath = os.path.abspath(os.path.dirname(script_file))
    rootDir_absPath = os.path.abspath(os.path.join(scriptDir_absPath, "./../../"))
    if root_dir:
        rootDir_absPath = root_dir
    preDir_absPath = os.path.abspath(os.path.join(rootDir_absPath, "./pre"))
    preDataDir_absPath = os.path.abspath(os.path.join(preDir_absPath, "./data"))
    postDir_absPath = os.path.abspath(os.path.join(rootDir_absPath, "./post"))
    postDataDir_absPath = os.path.abspath(os.path.join(postDir_absPath, "./data"))
    inputFile_absPath = os.path.abspath(
        os.path.join(preDataDir_absPath, inputFile_name)
    )
    resultsFile_absPath = os.path.abspath(
        os.path.join(postDataDir_absPath, resultFile_name)
    )

    [workDir_names, wd_ids] = get_dir_list_based_on_names(
        name_startsWith=workDir_name_startsWith, basedir_absPath=rootDir_absPath
    )
    if not len(wd_ids):
        [workDir_names, wd_ids] = [["."], [0]]

    print(workDir_names)
    if os.path.exists(inputFile_absPath):
        input_df = pd.read_csv(inputFile_absPath, delimiter=r"\s+")
    else:
        input_df = pd.DataFrame(data={merge_on_column: wd_ids})
    input_df[merge_on_column] = input_df[merge_on_column].astype(str)

    if os.path.exists(resultsFile_absPath) and not overwrite:
        output_df = pd.read_csv(resultsFile_absPath, delimiter=r"\s+")
        output_df[merge_on_column] = output_df[merge_on_column].astype(str)
    else:
        output_df = pd.DataFrame()

    for workDir_name, wd_id in zip(workDir_names, wd_ids):
        if str(wd_id) not in list(input_df[merge_on_column].astype(str)):
            continue

        wd_df = pd.DataFrame()

        workDir_absPath = os.path.abspath(os.path.join(rootDir_absPath, workDir_name))

        [caseDir_names, cd_ids] = get_dir_list_based_on_names(
            name_startsWith=caseDir_name_startsWith, basedir_absPath=workDir_absPath
        )
        for caseDir_name, cd_id in zip(caseDir_names, cd_ids):
            if not output_df.empty:
                if (str(wd_id), str(cd_id)) in zip(
                    list(output_df[merge_on_column].astype(str)),
                    list(output_df[caseFolderColLabel].astype(str)),
                ):
                    continue

            cd_df = pd.DataFrame()

            caseDir_absPath = os.path.abspath(
                os.path.join(workDir_absPath, caseDir_name)
            )
            if not os.path.exists(os.path.join(caseDir_absPath)):
                continue

            # cd_df[merge_on_column] = [int(wd_id)]
            cd_df[merge_on_column] = [str(wd_id)]
            cd_df[caseFolderColLabel] = [cd_id]
            print(f"Gathering data for case : {workDir_name}/{caseDir_name}")

            try:
                for instruction in EXTRACTION_INSTRUCTIONS:
                    if instruction["type"] == "area_histogram":
                        rawFile_absPath = os.path.abspath(
                            os.path.join(caseDir_absPath, instruction["file"])
                        )
                        if not os.path.exists(rawFile_absPath):
                            continue

                        histogram_df = pd.read_csv(
                            rawFile_absPath, delimiter=r"\s+", index_col=0
                        )
                        buckets_list = histogram_df.index.to_list()
                        histogram_df_cumsum = histogram_df.cumsum()
                        for x_name, presentation_name, min_threshold, max_threshold in [
                            x
                            for x in zip(
                                instruction["x_names"],
                                instruction["presentation_names"],
                                instruction["min_threshold"],
                                instruction["max_threshold"],
                            )
                            if x[0] in histogram_df_cumsum
                        ]:
                            x_cumsum = histogram_df_cumsum[x_name].to_list()
                            extr_val = np.interp(
                                instruction["threshold_area"], x_cumsum, buckets_list
                            )
                            if extr_val < min_threshold or extr_val > max_threshold:
                                raise ExceedsThreshold()
                            else:
                                cd_df[presentation_name] = [extr_val]

                    elif instruction["type"] in ["log_file", "CATParamFile"]:
                        if instruction["type"] == "log_file":
                            rawFile_absPath = os.path.abspath(
                                os.path.join(caseDir_absPath, instruction["file"])
                            )
                        elif instruction["type"] == "CATParamFile":
                            rawFile_absPath = os.path.abspath(
                                os.path.join(instruction["param_dir"], f"{wd_id}.txt")
                            )

                        if not os.path.exists(rawFile_absPath):
                            continue

                        if "awk_patterns" not in instruction:
                            instruction["awk_patterns"] = [
                                [] for _ in range(len(instruction["search_patterns"]))
                            ]

                        for (
                            awk_pattern,
                            search_pattern,
                            presentation_name,
                            min_threshold,
                            max_threshold,
                        ) in zip(
                            instruction["awk_patterns"],
                            instruction["search_patterns"],
                            instruction["presentation_names"],
                            instruction["min_threshold"],
                            instruction["max_threshold"],
                        ):
                            extracted_list = []
                            extr_val = None

                            started = False
                            ended = False

                            if instruction["extraction_method"] == "between_times":
                                sed_pattern = f'/^Time = {instruction["time_start"]}$/,/^Time = {instruction["time_end"]}$/{{/{search_pattern[0]}/p}}'
                                sed_cmd_to_run = [
                                    "sed",
                                    "-n",
                                    sed_pattern,
                                    rawFile_absPath,
                                ]
                                sed_cmd = subprocess.run(
                                    sed_cmd_to_run, stdout=subprocess.PIPE
                                )
                                cmd_stdout = sed_cmd.stdout
                            else:
                                cat_cmd_to_run = ["cat", rawFile_absPath]
                                cat_cmd = subprocess.run(
                                    cat_cmd_to_run, stdout=subprocess.PIPE
                                )
                                cmd_stdout = cat_cmd.stdout

                            for pat in awk_pattern:
                                if pat:
                                    awk_cmd_to_run = ["awk", f"{pat}"]
                                    awk_cmd = subprocess.run(
                                        awk_cmd_to_run,
                                        input=cmd_stdout,
                                        stdout=subprocess.PIPE,
                                    )
                                    cmd_stdout = awk_cmd.stdout

                            for pat in search_pattern:
                                grep_cmd_to_run = ["grep", "-o", f"{pat}"]
                                grep_cmd = subprocess.run(
                                    grep_cmd_to_run,
                                    input=cmd_stdout,
                                    stdout=subprocess.PIPE,
                                )
                                cmd_stdout = grep_cmd.stdout
                            extracted_list = cmd_stdout.decode("utf-8").split("\n")

                            last_n = 0
                            if instruction["extraction_method"] == "avg_last_n":
                                last_n = -instruction["last_n"]

                            extr_val = np.average(
                                [float(i.replace(",", ".")) for i in extracted_list[last_n:] if i]
                            )

                            if extr_val is not None:
                                if extr_val < min_threshold or extr_val > max_threshold:
                                    raise ExceedsThreshold()
                                else:
                                    cd_df[presentation_name] = [extr_val]

                    elif instruction["type"] == "operate":
                        for (
                            operation,
                            presentation_name,
                            min_threshold,
                            max_threshold,
                        ) in zip(
                            instruction["operation"],
                            instruction["presentation_names"],
                            instruction["min_threshold"],
                            instruction["max_threshold"],
                        ):
                            col_list = re.findall("{(.*?)}", operation)
                            continue_operation = True
                            for col in col_list:
                                if col not in cd_df.columns:
                                    continue_operation = False
                                    break

                            if continue_operation:
                                oper_val = float(
                                    eval(
                                        operation.replace("{", 'cd_df["').replace(
                                            "}", '"]'
                                        )
                                    )
                                )
                                if oper_val < min_threshold or oper_val > max_threshold:
                                    raise ExceedsThreshold()
                                else:
                                    cd_df[presentation_name] = oper_val

            except TooManyLines:
                cd_df = None
                print("Too many lines!")

            except ExceedsThreshold:
                cd_df = None
                print("Exceeds threshold!")

            if cd_df is not None:
                wd_df = wd_df.append(cd_df, ignore_index=True)
                print(cd_df)

        output_df = output_df.append(wd_df, ignore_index=True)

        result_df = input_df.merge(
            output_df, how="inner", on=merge_on_column, suffixes=("", "_drop")
        )
        result_df.drop(
            [col for col in result_df.columns if "drop" in col], axis=1, inplace=True
        )

        if print_df:
            print(result_df)

        result_df.to_csv(
            path_or_buf=resultsFile_absPath,
            sep=" ",
            index=False,
            float_format="%.4g",
            na_rep="NaN",
        )
