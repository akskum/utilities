import sys
import matplotlib.pyplot as plt

values = [];

with open(sys.argv[1], 'r') as f:
    for val in f.read().split():
        values.append(float(val))

iterations = len(values)

plt.plot(values)
plt.ylim(0.9*values[iterations-1], 1.1*values[iterations-1])
plt.xlabel('Iteration')
plt.ylabel('value')
plt.show()
