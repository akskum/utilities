import os
import sys

project_home = '/nsa/fluidcfd/00_Ongoing'
personal_home = '/nsa/fluidcfd/04_Personal/akucwh'
home = os.environ['HOME']
savedFile = ".fdSaved"
file = os.path.join(home, savedFile)

cwd = os.getcwd()
argv = sys.argv

n = len(argv)
if n == 1:
    flag = ''
else:
    flag = argv[1]
    if n > 2:
        mpos = argv[2]

def helpMessage():
    print("fd <flags>")
    print("no flag: go to last saved directory")
    print("usage flags:")
    print("\ts - store current working directory")
    print("\tr - go to the project root directory ({})".format(project_home))
    print("\tp - go to the project personal directory ({})".format(personal_home))
    print("\th - print this message")
    print("\tl - list saved directory")
    print("\tm - map current working directory to a key")
    print("\tl - go to directory mapped to key")

def saveDir():
    f = open(file, 'r')
    d = f.read()
    if d:
        dict = eval(d)
    f.close()
    f = open(file, 'w')
    if d:
        dict["saved"] = cwd
    else:
        dict = { "saved" : cwd }
    f.write("%s" % dict)
    f.close()

def gotoSavedDir():
    f = open(file, 'r')
    d = eval(f.read())
    f.close()
    print(d['saved'])

def listDir():
    f = open(file, 'r')
    d = eval(f.read())
    f.close()
    for i in d:
        print("%s : %s" % (i, d[i]))

def gotoDir():
    f = open(file, 'r')
    d = eval(f.read())
    f.close()
    print(d[mpos])

def mapDir():
    f = open(file, 'r')
    d = f.read()
    if d:
        dict = eval(d)
    f.close()
    f = open(file, 'w')
    if d:
        dict[mpos] = cwd
    else:
        dict = { mpos : cwd }
    f.write("%s" % dict)
    f.close()


if flag == 's':
    saveDir()
elif flag == 'h':
    helpMessage()
elif flag == 'r':
    print(project_home)
elif flag == 'p':
    print(personal_home)
elif flag == 'm':
    mapDir()
elif flag == 'g':
    gotoDir()
elif flag == 'l':
    listDir()
else:
    gotoSavedDir()
