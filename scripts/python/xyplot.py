import sys
import matplotlib.pyplot as plt

xValues = [];
yValues = [];

with open(sys.argv[1], 'r') as f:
    for line in f:
        x, y = line.split()
        xValues.append(float(x))
        yValues.append(float(y))

nValues = len(xValues)

plt.plot(xValues, yValues, 'bo-')
plt.ylim(0.98*min(yValues), 1.02*max(yValues))
plt.xlabel('X')
plt.ylabel('Y')
plt.show()
