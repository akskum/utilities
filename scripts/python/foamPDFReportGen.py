import os

import click
import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib.backends.backend_pdf import PdfPages
from PIL import Image

enum_order = {"PAGE": 0, "ROW": 1, "COLUMN": 2}


def get_cropped_image(image, crop):
    width, height = image.size
    xMin, xMax, yMin, yMax = crop[0], crop[1], crop[2], crop[3]
    left = xMin
    right = min(xMax, width)
    top = yMin
    bottom = min(yMax, height)

    cropped_image = image.crop((left, top, right, bottom))
    return cropped_image


@click.command()
@click.argument("filename")
@click.option("--grid", is_flag=True, default=False)
@click.option("--unlimited", is_flag=True, default=False)
def main(filename, grid, unlimited):
    yamlFile_name = filename

    # script_file = __file__
    # scriptDir_absPath = os.path.abspath(os.path.dirname(script_file))
    currentDir_absPath = os.getcwd()
    rootDir_absPath = os.path.abspath(os.path.join(currentDir_absPath, "./../../"))
    # preDir_absPath = os.path.abspath(os.path.join(rootDir_absPath, "./pre"))
    postDir_absPath = os.path.abspath(os.path.join(rootDir_absPath, "./post"))
    postPPTDir_absPath = os.path.abspath(os.path.join(postDir_absPath, "./ppt"))

    yamlFile_absPath = os.path.abspath(os.path.join(currentDir_absPath, yamlFile_name))
    with open(yamlFile_absPath, encoding="utf-8") as file_handle:
        yaml_dict = yaml.load(file_handle, Loader=yaml.SafeLoader)
    # print(yaml_dict)

    outFile_absPath = os.path.abspath(
        os.path.join(
            postPPTDir_absPath,
            yaml_dict.get("outFileName", f"PDF__{yamlFile_name.removesuffix('.yaml')}.pdf"),
        )
    )
    pp = PdfPages(outFile_absPath)

    graphic_absPath_fullList = []
    for workDir in yaml_dict["workDirs"]:
        graphic_absPath_workDirList = []
        for caseDir in yaml_dict["caseDirs"]:
            graphic_absPath_caseDirList = []
            for graphic in yaml_dict["graphics"]:
                workDirPathRelToRoot = workDir["workDirPathRelToRoot"]
                caseDirPathRelToWorkDir = caseDir["caseDirPathRelToWorkDir"]
                graphicPathRelToCaseDir = graphic["graphicPathRelToCaseDir"]
                graphicCrop = [
                    graphic["xMin"],
                    graphic["xMax"],
                    graphic["yMin"],
                    graphic["yMax"],
                ]
                graphicType = graphic["type"]

                graphic_absPath = os.path.abspath(
                    os.path.join(
                        os.path.join(
                            os.path.join(rootDir_absPath, workDirPathRelToRoot),
                            caseDirPathRelToWorkDir,
                        ),
                        graphicPathRelToCaseDir,
                    )
                )
                graphic_absPath_caseDirList.append(
                    [graphic_absPath, graphicCrop, graphicType]
                )
            graphic_absPath_workDirList.append(graphic_absPath_caseDirList)
        graphic_absPath_fullList.append(graphic_absPath_workDirList)
    # print(graphic_absPath_fullList)

    heightRatios_workDir = [i.get("heightRatio", 1) for i in yaml_dict["workDirs"]]
    heightRatios_caseDir = [i.get("heightRatio", 1) for i in yaml_dict["caseDirs"]]
    heightRatios_graphic = [i.get("heightRatio", 1) for i in yaml_dict["graphics"]]
    heightRatios = [heightRatios_workDir, heightRatios_caseDir, heightRatios_graphic]
    # print(heightRatios)

    widthRatios_workDir = [i.get("widthRatio", 1) for i in yaml_dict["workDirs"]]
    widthRatios_caseDir = [i.get("widthRatio", 1) for i in yaml_dict["caseDirs"]]
    widthRatios_graphic = [i.get("widthRatio", 1) for i in yaml_dict["graphics"]]
    widthRatios = [widthRatios_workDir, widthRatios_caseDir, widthRatios_graphic]
    # print(widthRatios)

    label_workDirList = [i["label"] for i in yaml_dict["workDirs"]]
    label_caseDirList = [i["label"] for i in yaml_dict["caseDirs"]]
    label_graphicList = [i["label"] for i in yaml_dict["graphics"]]
    label_fullList = [label_workDirList, label_caseDirList, label_graphicList]
    # print(label_fullList)

    (num_workDirs, num_caseDirs, num_graphics, temp) = np.array(
        graphic_absPath_fullList
    ).shape
    # print(
    #     f"(num_workDirs, num_caseDirs, num_graphics) = {np.array(graphic_absPath_fullList).shape[0:3]}"
    # )

    order = [
        enum_order[yaml_dict["order"]["workDir"]],
        enum_order[yaml_dict["order"]["caseDir"]],
        enum_order[yaml_dict["order"]["graphics"]],
    ]

    arranged_graphic_absPath_fullList = np.moveaxis(
        graphic_absPath_fullList, [0, 1, 2], order
    )
    (num_pages, num_rows, num_columns, temp) = np.array(
        arranged_graphic_absPath_fullList
    ).shape
    # print(
    #     f"(num_pages, num_rows, num_columns) = {np.array(arranged_graphic_absPath_fullList).shape[0:3]}"
    # )

    arranged_label_fulllist = [x for _, x in sorted(zip(order, label_fullList))]
    # print(arranged_label_fulllist)

    arranged_heightRatios = [x for _, x in sorted(zip(order, heightRatios))]
    # print(arranged_heightRatios)

    arranged_widthRatios = [x for _, x in sorted(zip(order, widthRatios))]
    # print(arranged_widthRatios)

    figsize=(16,9)
    if unlimited:
        fig_width = max(16, num_columns*7)
        fig_height = max(9, num_rows*5)
        figsize=(fig_width, fig_height)

    for page in range(num_pages):
        fig, ax = plt.subplots(
            num_rows,
            num_columns,
            figsize=figsize,
            dpi=300,
            gridspec_kw={
                "width_ratios": arranged_widthRatios[2],
                "height_ratios": arranged_heightRatios[1],
                "hspace": 0,
            },
        )
        for row in range(num_rows):
            for col in range(num_columns):
                with open(
                    arranged_graphic_absPath_fullList[page][row][col][0], "rb"
                ) as f:
                    if num_rows == 1 and num_columns == 1:
                        axi = ax
                    elif num_rows == 1 or num_columns == 1:
                        axi = ax[max(row, col)]
                    else:
                        axi = ax[row][col]

                    image = Image.open(f)
                    cropped_image = get_cropped_image(
                        image=image,
                        crop=arranged_graphic_absPath_fullList[page][row][col][1],
                    )

                    if (
                        arranged_graphic_absPath_fullList[page][row][col][2]
                        == "StretchColumns"
                    ):
                        gs = axi.get_gridspec()
                        axplot = fig.add_subplot(gs[row, :])
                    elif (
                        arranged_graphic_absPath_fullList[page][row][col][2]
                        == "StretchRows"
                    ):
                        gs = axi.get_gridspec()
                        axplot = fig.add_subplot(gs[:, col])
                    else:
                        axplot = axi

                    axplot.imshow(cropped_image)

                    if not grid:
                        try:
                            for axs in [axi, axplot]:
                                for spine in axs.spines.values():
                                    spine.set_visible(False)
                                axs.axes.xaxis.set_ticks([])
                                axs.axes.yaxis.set_ticks([])
                        except Exception as e:
                            pass

                    if col == 0:
                        axi.set_ylabel(
                            arranged_label_fulllist[1][row],
                            fontsize="large",
                            color="white",
                            fontweight="bold",
                            bbox=dict(boxstyle="square", facecolor="#041e42"),
                        )
                    if row == 0:
                        axi.set_xlabel(
                            arranged_label_fulllist[2][col],
                            fontsize="large",
                            color="white",
                            fontweight="bold",
                            bbox=dict(boxstyle="square", facecolor="#041e42"),
                        )
                        axi.xaxis.set_label_position("top")
        fig.suptitle(
            arranged_label_fulllist[0][page],
            fontsize="xx-large",
            fontweight="bold",
            color="white",
            bbox=dict(boxstyle="square", facecolor="#041e42"),
        )
        if not unlimited:
            fig.tight_layout()
        pp.savefig()
        # plt.show()
    pp.close()


if __name__ == "__main__":
    main()
