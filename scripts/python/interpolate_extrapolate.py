import numpy as np
import pandas as pd
import scipy
import matplotlib.pyplot as plt

# get x and y vectors
x = [0.1, 0.3, 0.7 , 1, 1.5, 2]
y = [10.6, 13.64, 16.38, 17.47, 18.79, 19.78]

# calculate polynomial
z = np.polyfit(x, y, 3)
f = np.poly1d(z)

# calculate new x's and y's
x_new = np.linspace(0, 2, 50)
y_new = f(x_new)

# Creating the dataset, and generating the plot
plt.plot(x, y, 'bo')
plt.plot(x_new, y_new)

plt.show()
