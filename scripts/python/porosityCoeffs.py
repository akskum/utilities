#!/home/sssnos/tensorflow/bin/python

import os
import sys

dpf = "dpf"
scr = "scr"
asc = "asc"
doc = "doc"
mtx = "mtx"
cas1 = "cas1"
twc = "twc"

alpha = {}
beta = {}


def get_coeffs(app):
    if app == mtx:
        alpha[dpf] = 3.2e+7
        beta[dpf] = 113.4
        
        alpha[scr] = 3.613e+7
        beta[scr] = 35.26
        
        alpha[asc] = 7.39e+7
        beta[asc] = 487.0
        
        alpha[doc] = 2.286e+7
        beta[doc] = 263.8

        alpha[twc] = 4.21e+7
        beta[twc] = 30.68

    elif app == cas1:
        alpha[dpf] = 3.16e+7
        beta[dpf] = 99.515
        
        alpha[scr] = 3.67e+7
        beta[scr] = 26.502
        
        alpha[asc] = 6.41e+7 
        beta[asc] = 80.81

substrates = [ dpf, scr, asc, doc, twc ]
applications = [ mtx, cas1 ]
q = raw_input("Substrate? {}: ".format(substrates))
s = raw_input("Application? {}: ".format(applications))

if not q in substrates:
    print("{} is not a valid argument. Exit.".format(q))
    exit()

if not s in applications:
    print("{} is not a valid argument. Exit.".format(s))
    exit()

get_coeffs(s)

print("alpha = {:.3e}".format(alpha[q]))
print("beta = {}".format(beta[q]))
