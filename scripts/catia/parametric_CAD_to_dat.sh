#!/bin/bash

. ~/bin/bash_scripting_functions.sh
[[ $CONDA_DEFAULT_ENV == "data_analysis" ]] || die "not sourced correct conda environment" 

python /home/akucwh/git/utilities/scripts/catia/convert_parametric_CADDesigns_to_dat.py $1
