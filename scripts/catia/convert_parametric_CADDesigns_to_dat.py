import click
import openpyxl
import pandas as pd
import xlrd


@click.command()
@click.argument("filename")
def main(filename):
    df = pd.read_excel(
        filename,
        sheet_name="DOE and Simulation",
        header=0,
        skiprows=51,
    )
    df = df.drop(
        ["Error occurred?", "Error number", "Error source", "Error description"], axis=1
    )
    df.rename(columns={"Geometric design name / Folder name": "%eval_id"}, inplace=True)
    df.columns = df.columns.str.replace("[", "_")
    df.columns = df.columns.str.replace("]", "")
    df.columns = df.columns.str.replace(" ", "_")
    df.reset_index(drop=True, inplace=True)

    df.to_csv("inputs.dat", sep=" ", index=False)


if __name__ == "__main__":
    main()
