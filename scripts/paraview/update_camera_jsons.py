import json
import sys

in_file1 = sys.argv[1]
in_file2 = sys.argv[2]
out_file = sys.argv[3]

with open(in_file1) as fin1:
    data1 = json.load(fin1)

with open(in_file2) as fin2:
    data2 = json.load(fin2)

data2.extend(data1)
out_inds = []
for el in data2:
    found_index = -1
    invalid_camera_name_endsWith = ("Horizontal", "Vertical")
    if not el["Name"].endswith(invalid_camera_name_endsWith):
        for i, dic in enumerate(data2):
            if dic["Name"] == el["Name"]:
                found_index = i
    if found_index not in out_inds and found_index != -1:
        out_inds.append(found_index)

if len(out_inds) > 0:
    out_list = [data2[i] for i in out_inds]
    with open(out_file, "w") as fout:
        json.dump(out_list, fout, indent=2)
