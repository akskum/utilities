import os

import paraview.servermanager
from paraview.simple import *
from renderFunctions import *

pxm = servermanager.ProxyManager()


ENTITY_TYPE_DICT = {
    "geometries": {
        "ext": ".stl",
        "paths": ["/resources", "/constant/triSurface"],
    },
    "pids": {
        "ext": ".stl",
        "paths": ["/resources", "/constant/triSurface"],
    },
    "boundaries": {
        "ext": ".vtp",
        "paths": ["/VTK/mesh/boundaries"],
    },
    "patches": {
        "ext": ".vtp",
        "paths": ["/VTK/mesh/patches"],
    },
    "faceZones": {
        "ext": ".vtp",
        "paths": ["/VTK/mesh/faceZones"],
    },
    "sampling_planes": {
        "ext": ".vtp",
        "paths": ["/VTK/mesh/sampling"],
    },
    "streamlines": {
        "ext": ".vtk",
        "paths": ["/VTK/mesh/sampling"],
    },
    "cellZones": {
        "ext": ".vtu",
        "paths": ["/VTK/mesh/cellZones"],
    },
    "internal": {
        "ext": ".vtu",
        "paths": ["/VTK/mesh/internal"],
    },
}


cwd = os.getcwd()


paths = {}
direxists = {}
file_paths = {}
file_names = {}
src_dict = {}
src_group_dict = {}
for entType in ENTITY_TYPE_DICT.keys():
    ext = ENTITY_TYPE_DICT[entType]["ext"]
    path_list = ENTITY_TYPE_DICT[entType]["paths"]
    for path in path_list:
        paths[entType] = cwd + path
        direxists[entType] = os.path.isdir(paths[entType])
        if direxists[entType]:
            break
    file_paths[entType] = []
    file_names[entType] = []
    src_dict[entType] = []
    src_group_dict[entType] = []

    if direxists[entType]:
        for file in os.listdir(paths[entType]):
            if file.endswith(ext):
                file_paths[entType].append(os.path.join(paths[entType], file))
                file_names[entType].append(os.path.splitext(file)[0])


def read_entity(entType, file_name=None, pid_name=None):
    ret = read_entities(entType, file_name_list=[file_name], pid_list=[pid_name])
    return ret[0]


def read_entities(entType, file_name_list=None, pid_list=None):
    src_list = []
    if not file_name_list:
        file_name_list = file_names[entType]
    if direxists[entType]:
        if entType == "geometries" or entType == "pids":
            src_list_preTransform = read_STL(entType, file_name_list, pid_list)
            src_list = []
            for src_i in src_list_preTransform:
                transform = Transform(Input=src_i)
                transform.Transform = "Transform"
                transform.Transform.Scale = [0.001, 0.001, 0.001]
                src_list.append(transform)
        else:
            for i in range(len(file_paths[entType])):
                file_name_i = file_names[entType][i]
                mod_file_name_i = f"{entType}__{file_name_i}"
                already_existing_src = None
                for src_i in src_dict[entType]:
                    src_i_name = pxm.GetProxyName("sources", src_i)
                    if src_i_name == mod_file_name_i:
                        already_existing_src = src_i
                        break
                if file_name_i in file_name_list:
                    if not already_existing_src:
                        file_path_i = file_paths[entType][i]
                        src_reader = None
                        if os.path.splitext(file_path_i)[1] == ".vtu":
                            src_reader = XMLUnstructuredGridReader(
                                FileName=[file_path_i]
                            )
                        elif os.path.splitext(file_path_i)[1] == ".vtp":
                            src_reader = XMLPolyDataReader(FileName=[file_path_i])
                        elif os.path.splitext(file_path_i)[1] == ".vtk":
                            src_reader = LegacyVTKReader(FileNames=[file_path_i])
                        if src_reader:
                            src_dict[entType].append(src_reader)
                            RenameSource(mod_file_name_i, src_reader)
                            src_list.append(src_reader)
                    else:
                        src_list.append(already_existing_src)
    return src_list


def read_STL(entType, file_name_list, pid_list=None):
    src_list = []
    for i in range(len(file_paths[entType])):
        file_name_i = file_names[entType][i]
        mod_file_name_i = f"geometries__{file_name_i}"
        this_stl = None
        for src_i in src_dict["geometries"]:
            src_i_name = pxm.GetProxyName("sources", src_i)
            if src_i_name == mod_file_name_i:
                this_stl = src_i
                break
        if file_name_i in file_name_list:
            file_path_i = file_paths["geometries"][i]
            if not this_stl:
                stl_reader = None
                if os.path.splitext(file_path_i)[1] == ".stl":
                    stl_reader = STLReader(
                        registrationName=mod_file_name_i, FileNames=[file_path_i]
                    )
                    src_dict["geometries"].append(stl_reader)
                    this_stl = stl_reader

            if entType == "geometries":
                src_list.append(this_stl)

            if entType == "pids":
                if not pid_list:
                    pid_list = []
                for pid, line in enumerate(
                    grep_and_return(
                        file=file_path_i,
                        string="^endsolid.*",
                        return_all=True,
                    )
                ):
                    split_line = line.split()
                    if len(split_line) == 2:
                        pid_name = split_line[1]
                        this_pid = None
                        for src_i in src_dict[entType]:
                            src_i_name = pxm.GetProxyName("sources", src_i)
                            if src_i_name == pid_name:
                                this_pid = src_i
                                break
                        if pid_list == [] or pid_name in pid_list:
                            if not this_pid:
                                threshold = Threshold(
                                    registrationName=pid_name, Input=this_stl
                                )
                                # threshold.Scalars = [
                                #     "CELLS",
                                #     "STLSolidLabeling",
                                # ]
                                threshold.ThresholdRange = [pid, pid]
                                src_dict[entType].append(threshold)
                                this_pid = threshold

                            src_list.append(this_pid)
    return src_list


def read_and_group_entities(
    entType, groupName=None, file_name_list=None, pid_list=None
):
    src_list = read_entities(
        entType=entType, file_name_list=file_name_list, pid_list=pid_list
    )
    if not groupName:
        if not file_name_list:
            groupName = "GROUP"
        else:
            raise Exception("supply a group name")
    groupName = f"{entType}__{groupName}"
    if direxists[entType]:
        already_exists = False
        for src_i in src_group_dict[entType]:
            src_i_name = pxm.GetProxyName("sources", src_i)
            if src_i_name == groupName:
                already_exists = True
                src_list.append(src_i)
                return src_i
        if not already_exists:
            src_group = GroupDatasets(Input=src_list)
            src_group_dict[entType].append(src_group)
            RenameSource(groupName, src_group)
            return src_group


def highlightPIDS(renderView, pid_group_list=None, opacity=None):
    highlightPIDSOrPatches(
        renderView=renderView,
        entType="pids",
        pid_group_list=pid_group_list,
        opacity=opacity,
    )
    pass


def highlightPatches(renderView, pid_group_list=None, opacity=None):
    highlightPIDSOrPatches(
        renderView=renderView,
        entType="boundaries",
        pid_group_list=pid_group_list,
        opacity=opacity,
    )
    pass


def highlightPIDSOrPatches(renderView, entType, pid_group_list=None, opacity=None):
    colorIter = resetRGBIter()
    generic_entType = "geometries" if entType == "pids" else entType
    display = ShowSolidColor(read_and_group_entities(entType=generic_entType), renderView)
    if opacity:
        display.Opacity = opacity
    else:
        display.Opacity = 0.1

    if not pid_group_list:
        return 0

    for entry in pid_group_list:
        groupName = list(entry.keys())[0]
        pid_list = entry[groupName]["pid_list"]
        opacity = entry[groupName]["opacity"] if "opacity" in entry[groupName] else 1
        colorRGB = (
            entry[groupName]["color"]
            if "color" in entry[groupName]
            else next(colorIter)
        )

        ents_prelim = read_and_group_entities(
            entType=entType, groupName=groupName, file_name_list=pid_list
        )
        if "clip" in entry[groupName]:
            clip = Clip(Input=ents_prelim)
            clip.ClipType = "Plane"
            clip.ClipType.Origin = entry[groupName]["clip"]["origin"]
            clip.HyperTreeGridClipper = "Plane"
            clip.HyperTreeGridClipper.Origin = entry[groupName]["clip"]["origin"]
            clip.ClipType.Normal = entry[groupName]["clip"]["normal"]
            clip.Invert = entry[groupName]["clip"]["invert"]
            ents_to_display = clip
        else:
            ents_to_display = ents_prelim

        display = ShowSolidColor(
            ents_to_display,
            renderView,
        )
        display.DiffuseColor = colorRGB
        display.Opacity = opacity
    pass


def tubed_streamline(file_name, radius=0.001):
    sl = read_entity(entType="streamlines", file_name=file_name)
    tube = Tube(Input=sl, Radius=radius)
    return tube
