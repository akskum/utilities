#!/bin/bash

echo""; echo "********************************************************************************"
echo "Starting script"; echo "-----------------"
currentDir=$PWD

stateInputFilePath=$1
caseInputPath=$2

stateInputPath=${stateInputFilePath%/*}
stateFileName=${stateInputFilePath##*/}
if [[ "$stateInputPath" = /* ]]
then
    cd "$stateInputPath"
    statePath=$PWD
else
    cd "$currentDir/$stateInputPath"
    statePath=$PWD
fi
stateFileFullPath=$statePath"/"$stateFileName

if [[ "$caseInputPath" = /* ]]
then
    cd "$caseInputPath"
    casePath=$PWD
else
    cd "$currentDir/$caseInputPath"
    casePath=$PWD
fi
echo "  case is located in : "$casePath;

cd $casePath || exit 1
[[ ! -d "post/videos" ]] && mkdir post/videos -p

if [ -f $stateFileFullPath ]
then
    echo "  state file is      : "$stateFileFullPath
    [ $MB ] && sed -i "/^MB =/{s/MB.*/MB = $MB/g}" $stateFileFullPath
    pvbatch ~/git/utilities/scripts/paraview/pyAnimate.py $stateFileFullPath
fi
