#! /bin/bash

dir=$PWD
foamToVTK -no-internal -name VTK/mesh/boundaries -overwrite -time 'none'
foamToVTK -no-internal -no-boundary -name VTK/mesh/solFaceZones -overwrite -faceZones '(".*_to_.*")' -time 'none'
foamToVTK -no-internal -no-boundary -name VTK/mesh/refFaceZones -overwrite -faceZones '("ref.*" "fZ.*" "ui.*" "cZ.*")' -time 'none'
#for cellZone in $(getCellZones.sh | tail -n +5)
#do
#    foamToVTK -no-boundary -name VTK_mesh -cellZone $cellZone -time 'none'
#done
cd $dir/VTK/mesh/boundaries
ln -s */boundary/*vtp .
cd $dir/VTK/mesh/solFaceZones
ln -s */*vtp .
cd $dir/VTK/mesh/refFaceZones
ln -s */*vtp .
