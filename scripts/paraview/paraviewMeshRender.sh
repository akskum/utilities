#! /bin/bash

[ "$#" -eq 0 ] && cp -f ~/apps/utilities/scripts/paraview/renderParaviewMesh_template.py . && paraview --state=renderParaviewMesh_template.py&

if [ "$#" -eq 1 ]
then
    if [ -f $1 ]
    then
       echo "" >> renderParaviewMesh_template.py
       echo "" >> renderParaviewMesh_template.py
       grep "renderView.*Camera" $1 >> renderParaviewMesh_template.py
       mv renderParaviewMesh_template.py renderParaviewMesh.py 
       paraview --state=renderParaviewMesh.py
    fi
fi
