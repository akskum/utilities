import os
import sys

from paraview.simple import *

sys.path.append("/home/akucwh/apps/utilities/scripts/paraview")
from renderFunctions import *

case_dir = os.getcwd()
case_name = os.path.basename(case_dir)

nArgs = len(sys.argv)
if nArgs != 3:
    print("Stop!!! You need to supply state file and picture name")
    exit(2)

stateFile = sys.argv[1]
pictureFilename = sys.argv[2]

path = pictureFilename
print("Using {} to create {}".format(stateFile, path))

# read state
LoadState(stateFile)
aLayout = GetLayout()
renderView = GetRenderView()

# RENDERING
# ---------
print(case_name)

# U_txt = grep_and_return(f"{case_dir}/system/changeDictionaryDict", "[0-9\-\.]* kg/h")
# T_txt = grep_and_return(f"{case_dir}/system/changeDictionaryDict", "[0-9\-\.]* degC")
# Qdos_txt = grep_and_return(f"{case_dir}/constant/reactingCloud1Properties", "[0-9\-\.]* g/min")
# op_cond_txt = U_txt + ', ' + T_txt + ', ' + Qdos_txt
#
# if len(op_cond_txt) > 5:
#     operating_cond_text = Text(registrationName='OperatingCondition')
#     operating_cond_text.Text = op_cond_txt
#
#     display = Show(operating_cond_text, renderView, 'TextSourceRepresentation')
#     display.FontSize = 30

# Render()
SaveScreenshot(
    path,
    layout=aLayout,
    ImageResolution=[3224, 1860],
    FontScaling="Scale fonts proportionally",
    OverrideColorPalette="",
    StereoMode="No change",
    TransparentBackground=0,
    # PNG options
    CompressionLevel="5",
)
