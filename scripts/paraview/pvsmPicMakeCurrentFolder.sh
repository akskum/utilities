#!/bin/bash

echo
echo "********************************************************************************"
echo "Starting script"
echo "---------------"

currentDir=$PWD

stateInputPath=$1
caseInputPath=$2

if [[ "$stateInputPath" = /* ]]
then
    cd "$stateInputPath"
    statePath=$PWD
else
    cd "$currentDir/$stateInputPath"
    statePath=$PWD
fi
echo "  state files are located in : "$statePath
cd $statePath
cd ../../
rootDir=$PWD
cd $currentDir

if [[ "$caseInputPath" = /* ]]
then
    cd "$caseInputPath"
    casePath=$PWD
else
    cd "$currentDir/$caseInputPath"
    casePath=$PWD
fi
echo "  case is located in : "$casePath; echo ""
caseBaseName=$(basename $casePath)

[[ `ls $statePath/*.pvsm 2>/dev/null` ]] || exit 1
cd $casePath || exit 1

casePathRelLinkName=${casePath##*$rootDir/}
casePathLinkBasename=`basename $rootDir/${casePathRelLinkName%%/*}`
casePathActualBasename=`basename $(readlink -f $rootDir/${casePathRelLinkName%%/*})`
casePathRelActualName=$(echo $casePathRelLinkName | sed "s/$casePathLinkBasename/$casePathActualBasename/g")

# dir_to_save_png="$rootDir/post/images/$casePathRelActualName"
dir_to_save_png="$rootDir/post/images/$casePathRelLinkName"
[[ ! -d $dir_to_save_png ]] && mkdir -p $dir_to_save_png

for stateFile in $statePath/*.pvsm
do
    [[ $stateFile == *_$caseBaseName.pvsm ]] || continue
    sed -i '/k.foam/{s:value=".*/k\.foam":value="./k.foam":g}' $stateFile
    sed -i '/VTK/{s:value=".*/VTK/:value="./VTK/:g}' $stateFile
    sed -i '/triSurface/{s:value=".*/constant/triSurface/:value="./constant/triSurface/:g}' $stateFile

    stateFileBaseName=$(basename $stateFile)
    [[ $stateFileBaseName == geom* ]] && [[ ! -d $casePath/constant/triSurface ]] && continue
    [[ $stateFileBaseName != geom* ]] && [[ ! -d $casePath/VTK ]] && continue

    pngFile="$dir_to_save_png/""${stateFileBaseName%%_$caseBaseName.pvsm}"".png"

    [[ -f $pngFile ]] && continue

    echo $stateFileBaseName $pngFile
    pvbatch ~/git/utilities/scripts/paraview/pvsmPic.py $stateFile $pngFile
done
