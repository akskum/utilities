#!/bin/bash

echo
echo "********************************************************************************"
echo "Starting script"
echo "---------------"

currentDir=$PWD

stateInputFilePath=$1
caseInputPath=$2

stateInputPath=${stateInputFilePath%/*}
stateFileName=${stateInputFilePath##*/}
if [[ "$stateInputPath" = /* ]]
then
    cd "$stateInputPath"
    statePath=$PWD
else
    cd "$currentDir/$stateInputPath"
    statePath=$PWD
fi
stateFileFullPath=$statePath"/"$stateFileName
echo "  state file is : "$stateFileFullPath
cd $statePath
cd ../../
rootDir=$PWD
cd $currentDir

if [[ "$caseInputPath" = /* ]]
then
    cd "$caseInputPath"
    casePath=$PWD
else
    cd "$currentDir/$caseInputPath"
    casePath=$PWD
fi
echo "  case is located in : "$casePath;
caseBaseName=$(basename $casePath)

cd $casePath || exit 1
casePathRel=${casePath##*$rootDir/}
dir_to_save_png="$rootDir/post/images/$casePathRel"
[[ ! -d $dir_to_save_png ]] && mkdir -p $dir_to_save_png

if [ -f $stateFileFullPath ]
then
    [ $MB ] && sed -i "/^MB =/{s/MB.*/MB = $MB/g}" $stateFileFullPath
    pvbatch /home/akucwh/git/utilities/scripts/paraview/pyPic.py $stateFileFullPath $dir_to_save_png
fi
