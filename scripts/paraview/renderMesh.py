import sys

sys.path.append("/home/akucwh/apps/utilities/scripts/paraview")
from groupMeshDataSets import *


# Import colormap
vtkCompositeIndexLUT = GetColorTransferFunction('vtkCompositeIndex')
vtkCompositeIndexPWF = GetOpacityTransferFunction('vtkCompositeIndex')
vtkCompositeIndexLUT.ApplyPreset('Random_100', True)
vtkCompositeIndexLUT.RescaleTransferFunction(1.0, 100.0)
vtkCompositeIndexPWF.RescaleTransferFunction(1.0, 100.0)


# display only boundaries
if(boundaries_flag):
    boundariesLayout = CreateLayout(name='boundaries_layout')
    renderView1 = CreateView('RenderView')
    boundariesLayout.AssignView(0, renderView1)

    boundariesDisplay = Show(boundaries, renderView1)
    boundariesDisplay.Representation = 'Surface'
    boundariesDisplay.ColorArrayName = ['CELLS', 'vtkCompositeIndex']
    boundariesDisplay.LookupTable = vtkCompositeIndexLUT
    boundariesDisplay.SetScalarBarVisibility(renderView1, False)
    boundariesDisplay.Opacity = 0.2
    boundariesDisplay.RescaleTransferFunctionToDataRange(1, 100)


# display transparent shaded boundaries, opaque colored faceZones between cellZones
solFaceZonesLayout = CreateLayout(name='solutionFaceZones_layout')
renderView2 = CreateView('RenderView')
solFaceZonesLayout.AssignView(0, renderView2)

if(boundaries_flag):
    boundariesDisplay = Show(boundaries, renderView2)
    boundariesDisplay.Representation = 'Surface'
    boundariesDisplay.Opacity = 0.1

if(solFaceZones_flag):
    solFaceZonesDisplay = Show(solFaceZones, renderView2)
    solFaceZonesDisplay.Representation = 'Surface'
    solFaceZonesDisplay.ColorArrayName = ['CELLS', 'vtkCompositeIndex']
    solFaceZonesDisplay.LookupTable = vtkCompositeIndexLUT
    solFaceZonesDisplay.SetScalarBarVisibility(renderView2, False)
    solFaceZonesDisplay.Opacity = 1
    solFaceZonesDisplay.RescaleTransferFunctionToDataRange(1, 100)


# display transparent shaded boundaries and faceZones between cellZones, opaque colored zones belonging to "ref" or "fZ"
refFaceZonesLayout = CreateLayout(name='refFaceZones_layout')
renderView3 = CreateView('RenderView')
refFaceZonesLayout.AssignView(0, renderView3)

if(boundaries_flag):
    boundariesDisplay = Show(boundaries, renderView3)
    boundariesDisplay.Representation = 'Surface'
    boundariesDisplay.Opacity = 0.1

if(solFaceZones_flag):
    solFaceZonesDisplay = Show(solFaceZones, renderView3)
    solFaceZonesDisplay.Representation = 'Surface'
    solFaceZonesDisplay.Opacity = 0.1

if(refFaceZones_flag):
    refFaceZonesDisplay = Show(refFaceZones, renderView3)
    refFaceZonesDisplay.Representation = 'Surface'
    refFaceZonesDisplay.Opacity = 1
    refFaceZonesDisplay.ColorArrayName = ['CELLS', 'vtkCompositeIndex']
    refFaceZonesDisplay.LookupTable = vtkCompositeIndexLUT
    refFaceZonesDisplay.SetScalarBarVisibility(renderView1, False)
    refFaceZonesDisplay.RescaleTransferFunctionToDataRange(1, 100)
