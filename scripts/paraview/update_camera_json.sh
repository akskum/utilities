#! /bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

# Routine
# -------

[[ $CONDA_DEFAULT_ENV == "data_analysis" ]] || die "not sourced correct conda environment"

[[ $# -ge 2 ]] || die "please provide atleast two json files"

[[ -f $1 ]] || die "$1 is not a file"
in_file1=`readlink -f $1`

[[ -f $2 ]] || echo "[]" > $2
in_file2=$2
out_file=$2

[[ $# -eq 3 ]] && out_file=$3

[[ -L $out_file ]] && die "output file is a symlink"
out_file=`readlink -f $out_file`

python /home/akucwh/git/utilities/scripts/paraview/update_camera_jsons.py $in_file1 $in_file2 $out_file
