import subprocess

from paraview.simple import *


# COMMON FUNCTIONS
# ----------------
def getSingleViewLayout(layoutName):
    layout = CreateLayout(name=layoutName)
    layout.PreviewMode = [1612, 930]
    renderView = CreateView("RenderView")
    layout.AssignView(0, renderView)
    return renderView


def rgb_to_out(v1, v2, v3):
    return [v1 / 255, v2 / 255, v3 / 255]


def grep_and_return(
    file, string, return_all=False, return_nth_line=1, return_nth_word=0
):
    completed_grep_cmd = subprocess.run(
        [
            "grep",
            "-o",
            string,
            file,
        ],
        capture_output=True,
    )
    if return_all:
        return completed_grep_cmd.stdout.decode("utf-8").split("\n")
    else:
        if return_nth_word == 0:
            return completed_grep_cmd.stdout.decode("utf-8").split("\n")[
                return_nth_line - 1
            ]
        else:
            return (
                completed_grep_cmd.stdout.decode("utf-8")
                .split("\n")[return_nth_line - 1]
                .split()[return_nth_word - 1]
            )


def grep_last_n(file, string, n=500, awk_str=None):
    grep_stdin = None
    if awk_str:
        awk_cmd = ["awk", awk_str, file]
        completed_awk_cmd = subprocess.run(awk_cmd, stdout=subprocess.PIPE)
        grep_stdin = completed_awk_cmd.stdout
    grep_command = ["grep", "-o", string]
    if grep_stdin:
        completed_grep_cmd = subprocess.run(
            grep_command, input=grep_stdin, stdout=subprocess.PIPE
        )
    else:
        grep_command.append(file)
        completed_grep_cmd = subprocess.run(grep_command, stdout=subprocess.PIPE)
    grep_output = completed_grep_cmd.stdout.decode("utf-8").splitlines()

    sumVal = 0
    for line in grep_output[-n:]:
        split_line = line.split(" ")
        sumVal = sumVal + float(split_line[-1])

    return sumVal / n


def displayValue(
    renderView,
    value,
    Text_location,
    txt_pre="",
    txt_format=".3g",
    txt_post="",
    regName=None,
    bold=0,
    fontsize=30,
):
    formatted_txt = f"{txt_pre}{value:{txt_format}}{txt_post}"
    if not regName:
        regName = txt_pre.replace(" = ", "")

    TextObj = Text(registrationName=f"{regName}")
    TextObj.Text = formatted_txt
    display = Show(TextObj, renderView, "TextSourceRepresentation")
    display.WindowLocation = "AnyLocation"
    display.Position = Text_location
    display.Shadow = 1
    display.FontSize = fontsize
    display.Bold = bold
    return display


def extractAndDisplayValueFromLog(
    renderView,
    logFile,
    grepTxt,
    lastN,
    Text_location,
    awk_str=None,
    txt_pre="",
    txt_format=".3g",
    txt_post="",
    regName=None,
    bold=0,
    fontsize=30,
    scaling=1,
):
    raw_txt = grep_last_n(logFile, grepTxt, lastN, awk_str=awk_str)
    scaled_number = float(raw_txt) * scaling
    display = displayValue(
        renderView=renderView,
        value=scaled_number,
        Text_location=Text_location,
        txt_pre=txt_pre,
        txt_format=txt_format,
        txt_post=txt_post,
        regName=regName,
        bold=bold,
        fontsize=fontsize,
    )
    return [display, scaled_number]


def calculateVectorPlaneComponents(Input):
    sampled_samplingPlane = CellDatatoPointData(
        registrationName=f"CellDataToPointData", Input=Input
    )
    sampled_samplingPlane.CellDataArraytoprocess = ["Uav"]
    generateSurfaceNormals = GenerateSurfaceNormals(
        registrationName="generateSurfaceNormals",
        Input=sampled_samplingPlane,
    )
    programmableFilter = ProgrammableFilter(
        registrationName="programmableFilter", Input=generateSurfaceNormals
    )
    programmableFilter.Script = """
    input0 = inputs[0]
    Uav = input0.PointData["Uav"]
    Normals = input0.PointData["Normals"]
    UavNormal = dot(Uav, Normals)*Normals
    UavInPlane = Uav - UavNormal
    output.PointData.append(Uav, "Uav")
    output.PointData.append(UavNormal, "UavNormal")
    output.PointData.append(UavInPlane, "UavInPlane")
    """
    return programmableFilter


def ShowSolidColor(*args, **kwargs):
    return Show(*args, **kwargs, ColorArrayName=["POINTS", ""])


def Cbar(display, LUT, view, orientation, title):
    colorBar = GetScalarBar(LUT, view)
    colorBar.Orientation = orientation
    colorBar.WindowLocation = "AnyLocation"
    colorBar.Position = [0.30443849445284316, 0.49354838709677423]
    colorBar.Title = title
    colorBar.TitleColor = [0.0, 0.0, 0.0]
    colorBar.TitleBold = 1
    colorBar.ComponentTitle = ""
    colorBar.TitleFontSize = 24
    colorBar.LabelColor = [0.0, 0.0, 0.0]
    colorBar.LabelFontSize = 19
    colorBar.ScalarBarLength = 0.3514455917394748
    colorBar.Visibility = 1
    display.SetScalarBarVisibility(view, True)


def makeAnnotation(annot_text, renderView):
    display = Show(annot_text, renderView, "TextSourceRepresentation")
    display.FontSize = 30
    display.Bold = 1
    display.Shadow = 1


def makeInjArrow(pos_vec, dir_vec):
    pointSource = PointSource(registrationName="InjDirPointSource")
    pointSource.Center = dir_vec
    calculator = Calculator(registrationName="InjDirCalculator", Input=pointSource)
    calculator.Function = "coords"
    glyph = Glyph(registrationName="InjDirGlyph", Input=calculator, GlyphType="Arrow")
    glyph.OrientationArray = ["POINTS", "Result"]
    glyph.ScaleArray = ["POINTS", "No scale array"]
    glyph.ScaleFactor = 0.1
    transformToOrigin = Transform(
        registrationName="InjDirTransformToOrigin", Input=glyph
    )
    transformToOrigin.Transform = "Transform"
    transformToOrigin.Transform.Translate = [-j for j in dir_vec]

    coneTransformList = []
    for xrot in [-20, 0, 20]:
        for yrot in [-20, 0, 20]:
            for zrot in [-20, 0, 20]:
                transform = Transform(
                    registrationName="InjDirTransformCone", Input=transformToOrigin
                )
                transform.Transform = "Transform"
                transform.Transform.Translate = [i for i in pos_vec]
                transform.Transform.Rotate = [xrot, yrot, zrot]
                coneTransformList.append(transform)
    transformGroup = GroupDatasets(Input=coneTransformList)
    return transformGroup


def makeColoredGlyphs(
    regName,
    Input,
    layoutName,
    state_data,
    renderView,
    ScaleFactor,
    varName,
    cmapName,
    OrientationVar="",
    ScaleVar="",
    Stride=0,
    maxPoints=0,
    key="scaled",
    opacityMapping=0,
    cellsOrPoints="CELLS",
    cmap_orient=None,
    lower_bounds=None,
    upper_bounds=None,
    glyphType="Arrow",
    linesAsTubes=False,
    lineWidth=1,
):
    glyphObj = makeGlyphs(
        regName=regName,
        Input=Input,
        ScaleFactor=ScaleFactor,
        OrientationVar=OrientationVar,
        ScaleVar=ScaleVar,
        Stride=Stride,
        maxPoints=maxPoints,
        glyphType=glyphType,
        linesAsTubes=linesAsTubes,
        lineWidth=lineWidth,
        renderView=renderView,
    )
    display = Show(glyphObj, renderView)
    LUT = LinearCmapWithCbar(
        cmap_orient=cmap_orient,
        layoutName=layoutName,
        state_data=state_data,
        display=display,
        key=key,
        opacityMapping=opacityMapping,
        varName=varName,
        cmapName=cmapName,
        cellsOrPoints=cellsOrPoints,
        lower_bounds=lower_bounds,
        upper_bounds=upper_bounds,
    )
    return glyphObj


def makeGlyphs(
    renderView,
    regName,
    Input,
    ScaleFactor,
    OrientationVar="",
    ScaleVar="",
    Stride=0,
    maxPoints=0,
    glyphType="Arrow",
    linesAsTubes=False,
    lineWidth=1,
):
    glyphObj = Glyph(registrationName=regName, Input=Input)
    glyphObj.OrientationArray = ["POINTS", OrientationVar]
    if ScaleVar:
        glyphObj.ScaleArray = ["POINTS", ScaleVar]
    glyphObj.ScaleFactor = ScaleFactor
    glyphObj.GlyphTransform = "Transform2"
    glyphObj.GlyphType = glyphType
    glyph3Ddisplay = GetDisplayProperties(glyphObj, view=renderView)
    glyph3Ddisplay.LineWidth = lineWidth
    glyph3Ddisplay.RenderLinesAsTubes = 1 if linesAsTubes else 0
    if Stride != 0:
        glyphObj.GlyphMode = "Every Nth Point"
        glyphObj.Stride = Stride
    else:
        if maxPoints != 0:
            glyphObj.GlyphMode = "Uniform Spatial Distribution (Bounds Based)"
            glyphObj.MaximumNumberOfSamplePoints = maxPoints
        else:
            glyphObj.GlyphMode = "All Points"
    return glyphObj


def LinearCmapWithCbar(
    layoutName,
    state_data,
    display,
    key,
    opacityMapping,
    varName,
    cmapName,
    cellsOrPoints,
    lower_bounds=None,
    upper_bounds=None,
    cmap_orient=None,
):
    LUT = LinearCmap(
        state_data,
        display,
        key,
        opacityMapping,
        varName,
        cmapName,
        cellsOrPoints,
        lower_bounds,
        upper_bounds,
    )
    if cmap_orient:
        if cmap_orient == "Both":
            cmap_orients = ["Horizontal", "Vertical"]
        else:
            cmap_orients = cmap_orient

        for cmap_orienti in cmap_orients:
            generateCbarView(
                cmap_orienti,
                layoutName,
                state_data,
                key,
                opacityMapping,
                varName,
                cmapName,
                cellsOrPoints,
                lower_bounds,
                upper_bounds,
            )
    return LUT


def generateCbarView(
    cmap_orient,
    layoutName,
    state_data,
    key,
    opacityMapping,
    varName,
    cmapName,
    cellsOrPoints,
    lower_bounds,
    upper_bounds,
):
    origLayoutName = layoutName
    if cmap_orient in ["Horizontal", "Vertical"]:
        layoutName = f"{origLayoutName}_cmap_{cmap_orient}"
        layout = CreateLayout(name=layoutName)
        renderView = CreateView("RenderView")
        layout.AssignView(0, renderView)
        assign_camera(None, layoutName, renderView)

        dummySphere = Sphere()
        dummyLocation = ExtractLocation(Input=dummySphere)
        display = Show(dummyLocation, renderView)
        LUT = LinearCmap(
            state_data,
            display,
            key,
            opacityMapping,
            varName,
            cmapName,
            cellsOrPoints,
            lower_bounds,
            upper_bounds,
        )
        Cbar(display, LUT, renderView, cmap_orient, varName)
        renderView.ViewSize = [1612, 930]


def find(dicList, keyList, valueList):
    for i, dic in enumerate(dicList):
        found_index = i
        for key, val in zip(keyList, valueList):
            if dic[key] != val:
                found_index = -1
        if found_index != -1:
            return found_index


def assign_camera(camera_data, layoutName, view):
    camera_ind = None

    if camera_data:
        camera_ind = find(camera_data, ["Name"], [layoutName])

    if camera_ind != None:
        camera_subdict = camera_data[camera_ind]
        view.ViewSize = camera_subdict["ViewSize"]
        view.Background = camera_subdict["Background"]
        view.CameraPosition = camera_subdict["CameraPosition"]
        view.CameraFocalPoint = camera_subdict["CameraFocalPoint"]
        view.CameraViewUp = camera_subdict["CameraViewUp"]
        view.CameraViewAngle = camera_subdict["CameraViewAngle"][0]
        view.CameraParallelScale = camera_subdict["CameraParallelScale"][0]
        view.CameraParallelProjection = camera_subdict["CameraParallelProjection"][0]
        view.CenterOfRotation = camera_subdict["CenterOfRotation"]
    else:
        ResetCamera(view)


def LinearCmap(
    state_data,
    display,
    key,
    opacityMapping,
    varName,
    cmapName,
    cellsOrPoints,
    lower_bounds=None,
    upper_bounds=None,
):
    LinearCmapLUT = GetColorTransferFunction(varName, display, separate=True)
    LinearCmapPWF = GetOpacityTransferFunction(varName, display, separate=True)

    varKey = key if key == "scaled" else varName
    rgb_cmap_ind = find(
        state_data["LinearCmapRGBPoints"], ["var_name", "cmap_name"], [varKey, cmapName]
    )
    rgb_list = state_data["LinearCmapRGBPoints"][rgb_cmap_ind]["items"]
    rgb_key_ind = find(rgb_list, ["key"], [key])
    RGB_POINTS = rgb_list[rgb_key_ind]["value"]
    if key == "scaled":
        ScaledRGBPoints = []
        for count, val in enumerate(RGB_POINTS):
            if count % 4 == 0:
                ScaledRGBPoints.append(
                    lower_bounds + val * (upper_bounds - lower_bounds)
                )
            else:
                ScaledRGBPoints.append(val)
        LinearCmapLUT.RGBPoints = ScaledRGBPoints
    else:
        LinearCmapLUT.RGBPoints = RGB_POINTS

    pwf_cmap_ind = find(
        state_data["LinearCmapPWFPoints"], ["var_name", "cmap_name"], [varKey, cmapName]
    )
    pwf_list = state_data["LinearCmapPWFPoints"][pwf_cmap_ind]["items"]
    pwf_key_ind = find(pwf_list, ["key"], [key])
    PWF_POINTS = pwf_list[pwf_key_ind]["value"]
    if key == "scaled":
        ScaledPWFPoints = []
        for count, val in enumerate(PWF_POINTS):
            if count % 4 == 0:
                ScaledPWFPoints.append(
                    lower_bounds + val * (upper_bounds - lower_bounds)
                )
            else:
                ScaledPWFPoints.append(val)
        LinearCmapPWF.Points = ScaledPWFPoints
    else:
        LinearCmapPWF.Points = PWF_POINTS

    LinearCmapLUT.AutomaticRescaleRangeMode = "Never"
    LinearCmapLUT.EnableOpacityMapping = opacityMapping
    LinearCmapLUT.UseLogScale = 0
    LinearCmapLUT.ColorSpace = "RGB"
    LinearCmapLUT.ScalarRangeInitialized = 1.0
    LinearCmapPWF.ScalarRangeInitialized = 1
    display.ColorArrayName = [cellsOrPoints, varName]
    display.LookupTable = LinearCmapLUT
    display.Representation = "Surface"
    display.UseSeparateColorMap = True
    return LinearCmapLUT


RGBcolors = [
    [1, 0, 103],
    [213, 255, 0],
    [255, 0, 86],
    [158, 0, 142],
    [14, 76, 161],
    [255, 229, 2],
    [0, 95, 57],
    [0, 255, 0],
    [149, 0, 58],
    [255, 147, 126],
    [164, 36, 0],
    [0, 21, 68],
    [145, 208, 203],
    [98, 14, 0],
    [107, 104, 130],
    [0, 0, 255],
    [0, 125, 181],
    [106, 130, 108],
    [0, 174, 126],
    [194, 140, 159],
    [190, 153, 112],
    [0, 143, 156],
    [95, 173, 78],
    [255, 0, 0],
    [255, 0, 246],
    [255, 2, 157],
    [104, 61, 59],
    [255, 116, 163],
    [150, 138, 232],
    [152, 255, 82],
    [167, 87, 64],
    [1, 255, 254],
    [255, 238, 232],
    [254, 137, 0],
    [189, 198, 255],
    [1, 208, 255],
    [187, 136, 0],
    [117, 68, 177],
    [165, 255, 210],
    [255, 166, 254],
    [119, 77, 0],
    [122, 71, 130],
    [38, 52, 0],
    [0, 71, 84],
    [67, 0, 44],
    [181, 0, 255],
    [255, 177, 103],
    [255, 219, 102],
    [144, 251, 146],
    [126, 45, 210],
    [189, 211, 147],
    [229, 111, 254],
    [222, 255, 116],
    [0, 255, 120],
    [0, 155, 255],
    [0, 100, 1],
    [0, 118, 255],
    [133, 169, 0],
    [0, 185, 23],
    [120, 130, 49],
    [0, 255, 198],
    [255, 110, 65],
    [232, 94, 190],
]


def resetRGBIter():
    decRGBIter = iter([rgb_to_out(i, j, k) for i, j, k in RGBcolors])
    return decRGBIter

def makeSphere(p1, p2):
    pointSource = PointSource(registrationName="CustomPoint")
    pointSource.Center = [(x+y)/2 for x,y in zip(p1,p2)]
    return pointSource
