#! /bin/bash

dir=$PWD
if [ "$#" -gt 0 ]
then
    rm -rf VTK/mesh/cellZones/*
    while [ "$#" -gt 0 ]
    do
        if [ $(getCellZones.sh | grep $1 | wc -l) -gt 0 ]
        then
            cellZone=$1
            foamToVTK -time 'none' -no-boundary -cellZone $cellZone -name VTK/mesh/cellZones
            shift
        fi
    done
fi
if [ -d $dir/VTK/mesh/cellZones ]
then
    cd $dir/VTK/mesh/cellZones
    for i in $(find . -type f -name internal.vtu); do j=${i%/*}; k=${j##*/}; ln -s $k/internal.vtu $k.vtu; done
fi
