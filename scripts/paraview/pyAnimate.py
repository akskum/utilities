import os
import sys
import importlib
import paraview.servermanager

nArgs = len(sys.argv)
if nArgs != 2:
    print("Stop!!! You need to supply state file")
    exit(2)

stateFileWithPath = sys.argv[1]

statePath = os.path.dirname(os.path.abspath(stateFileWithPath))
sys.path.append(os.path.abspath(statePath))
statePyFile = os.path.basename(os.path.abspath(stateFileWithPath))
stateFile = os.path.splitext(statePyFile)[0]

importCmd = "from {} import *".format(stateFile)
exec(importCmd)
# from example import *
# importlib.import_module(stateFile)
# __import__(stateFile, globals=globals())

layout_list = GetLayouts()

counter = 0
layoutNames = []
final_view_list = []
while len(final_view_list) < len(layout_list):
    counter = counter + 1
    for layout in layout_list:
        layout_view_list = GetViewsInLayout(layout_list[layout])
        trimmed_layout_view_list = [
            view for view in layout_view_list if view not in final_view_list
        ]
        if len(trimmed_layout_view_list) == 1:
            layoutNames.append(layout[0])
            final_view_list.append(trimmed_layout_view_list[0])

    if counter > 100:
        break


print("")

animScene = GetAnimationScene()
animScene.PlayMode = "Snap To TimeSteps"
for i in range(len(final_view_list)):
    path = "{}/post/videos/{}.png".format(os.getcwd(), layoutNames[i])
    print("Using {} to create {}".format(os.path.abspath(stateFileWithPath), path))
    if not os.path.exists(path):
        SaveAnimation(
            path,
            final_view_list[i],
            ImageResolution=[1612, 930],
            FontScaling="Scale fonts proportionally",
            OverrideColorPalette="",
            StereoMode="No change",
            TransparentBackground=0,
            # PNG options
            CompressionLevel="5",
        )
