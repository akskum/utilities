#!/bin/bash

# NEW WAY TO ADD NEW DEPENDENCY ARRAY
SPACING=${1:-1000}

echo
job_identifier="postProc"

list_jobIds_with_identifier=$(/usr/bin/squeue -u $USER -o "%300j %50i" | grep "\-\-$job_identifier" | awk '{print $NF}')
arr_running_jobids_with_postProc=($list_jobIds_with_identifier)
# echo "Here are the jobIds with the identifier $job_identifier in the name:"
# echo "${arr_running_jobids_with_postProc[@]}"


list_running_doe_with_postProc=$(/usr/bin/squeue -u $USER -o "%300j" | grep "\-\-$job_identifier" | awk '{print $NF}' | sed "s/--$job_identifier//g" | sort)
arr_running_cases_with_postProc=($list_running_doe_with_postProc)
n_running_cases_with_postProc="${#arr_running_cases_with_postProc[@]}"
# echo "${arr_running_cases_with_postProc[@]}"

for (( i=0; i<$n_running_cases_with_postProc; i++ ))
do
    echo && echo 

    depender_id=$((i))
    depender_job_names="${arr_running_cases_with_postProc[$depender_id]}"
    depender_jobids=$(/usr/bin/squeue -u $USER -o "%j %i" | grep "^$depender_job_names" | awk '{print $2}')

    dependee_id=$((i-$SPACING))

    dependency_to_add=""
    [[ $dependee_id -ge 0 ]] && \
        dependee_job_name="${arr_running_cases_with_postProc[$dependee_id]}--$job_identifier" && \
        dependency_to_add="$(/usr/bin/squeue -u $USER -o "%j %i" | grep "^$dependee_job_name " | awk '{print $2}')"

    for depender_jobid in $depender_jobids; do
        depender_job_name=$(/usr/bin/squeue -u $USER -o "%i %j" | grep "^$depender_jobid" | awk '{print $2}')
        depender_job_status=$(/usr/bin/squeue -u $USER -o "%i %t" | grep "^$depender_jobid" | awk '{print $2}')
        [[ $depender_job_status == "PD" ]] || continue

        list_curr_dep=$(/usr/bin/squeue -u $USER -o "%i %E" | grep "^$depender_jobid" | awk '{print $2}' | grep "[0-9]*" -o)
        arr_curr_dep=($list_curr_dep)

        deps_to_keep=( $(printf "%s\n" "${arr_curr_dep[@]}" "${arr_running_jobids_with_postProc[@]}" "${arr_running_jobids_with_postProc[@]}" | sort | uniq -u) )

        echo $depender_job_name" ("$depender_jobid") => add("$dependency_to_add") keep("${deps_to_keep[@]}")"


        dep_string=$(echo $dependency_to_add" "${deps_to_keep[@]})
        [[ $dep_string == " " ]] && dependency_string="" || dependency_string=$(echo "afterany:"$dep_string | sed "s/\s\+/:/g;s/:\+/:/g") 

        cmd_to_run="scontrol update job=$depender_jobid dependency=$dependency_string"

        echo $cmd_to_run
        $cmd_to_run
        echo
    done
done


# OLD WAY RELEASE ALL
# echo
# job_identifier="postProc"
#
# list_jobIds_with_identifier=$(/usr/bin/squeue -u $USER -o "%300j %50i" | grep "\-\-$job_identifier" | awk '{print $NF}')
# arr_running_jobids_with_postProc=($list_jobIds_with_identifier)
# echo
# for restricitiveJobID in $list_jobIds_with_identifier; do
#     restrictedJobIDs=$(/usr/bin/squeue -u $USER -o "%i %j %E" | grep "afterany:$restricitiveJobID" | awk '{print $1}')
#     if [[ $restrictedJobIDs ]]; then 
#         for restrictedJobID in $restrictedJobIDs; do
#            update_dep_string=""
#             nonRestrictiveJobIDs=$(scontrol show jobid -dd $restrictedJobID | grep "Dependency=.*" -o | grep "afterany:[0-9]*" -o | grep "[0-9]*" -o | grep -v "$restricitiveJobID" | awk '{print $1}')
#             echo $restrictedJobID" depends on $restricitiveJobID"
#             [[ $nonRestrictiveJobIDs ]] && echo "but also depends on "$nonRestrictiveJobIDs && update_dep_string=$(echo "afterany:"$nonRestrictiveJobIDs | sed "s/ /:/g")
#             echo scontrol update job=$restrictedJobID dependency="$update_dep_string"
#             echo
#         done
#     fi
# done
