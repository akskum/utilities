#!/usr/bin/env python

import subprocess
import os
from collections import namedtuple

finished_command = subprocess.run(
    [
        "/usr/bin/squeue",
        "-u",
        os.environ["USER"],
        "-o",
        "%i %j %E",
    ],
    capture_output=True,
    text=True
)

rows = finished_command.stdout.split("\n")
headers = [_h for _h in rows[0].split(" ") if _h]
SQUEUE = namedtuple("SQUEUE", headers)
JOBNAME = namedtuple("JOBNAME", ["PROJECT", "DOEID", "OPERTING_CONDITION", "OPERATION"])


import re
def parse_dep(dep_str):
    if dep_str=="(null)":
        return []
    # afterany:1463410(unfulfilled),afterany:1463411(unfulfilled),afterany:1463412(unfulfilled),afterany:1463413(unfulfilled)
    return [re.sub("[^0-9]", "", _d) for _d in dep_str.split(",")]

def parse_jobname(jobname):
    return JOBNAME(*jobname.split("--"))

interesting_rows = []
for _row in rows[1:]:
    _cols = [_c for _c in _row.split(" ") if _c]
    if len(_cols) != len(headers):
        continue
    _row = SQUEUE(*_cols)
    _row = _row._replace(DEPENDENCY = parse_dep(_row.DEPENDENCY))
    _row = _row._replace(NAME = parse_jobname(_row.NAME))
    interesting_rows.append(_row)

print(finished_command.stdout)
print(headers)
print(interesting_rows)




# heading = "SLURM NODE INFO"
# stdout = "=" * len(heading) + "\n" + heading + "\n" + "-" * len(heading)
# for _p in interesting_partitions:
#     rows = [_r for _r in interesting_rows if _r.PARTITION == _p]
#     stdout += "\n" + (_p + ":").ljust(15)
#     for _r in rows:
#         stdout+= (_r.AVAIL + "/" + _r.STATE + "(" + _r.NODES + ")").ljust(15)
# print(stdcapture_outputt)
