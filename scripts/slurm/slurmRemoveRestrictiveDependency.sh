#!/bin/bash

echo
job_identifier="postProc"

list_jobIds_with_identifier=$(/usr/bin/squeue -o "%300j %50i" -u $USER | grep "\-\-$job_identifier" | awk '{print $NF}')
echo "Here are the jobIds with the identifier $job_identifier in the name:"
echo $list_jobIds_with_identifier

echo
for restricitiveJobID in $list_jobIds_with_identifier; do
    restrictedJobIDs=$(/usr/bin/squeue -o "%50i %500E" | grep "afterany:$restricitiveJobID" | awk '{print $1}')
    if [[ $restrictedJobIDs ]]; then 
        for restrictedJobID in $restrictedJobIDs; do
            update_dep_string=""
            nonRestrictiveJobIDs=$(scontrol show jobid -dd $restrictedJobID | grep "Dependency=.*" -o | grep "afterany:[0-9]*" -o | grep "[0-9]*" -o | grep -v "$restricitiveJobID" | awk '{print $1}')
            echo $restrictedJobID" depends on $restricitiveJobID"
            [[ $nonRestrictiveJobIDs ]] && echo "but also depends on "$nonRestrictiveJobIDs && update_dep_string=$(echo "afterany:"$nonRestrictiveJobIDs | sed "s/ /:/g")
            scontrol update job=$restrictedJobID dependency="$update_dep_string"
            echo
        done
    fi
done
