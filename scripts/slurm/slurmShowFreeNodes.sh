#! /bin/bash


echo

display_with_header()
{
    echo "$1" | sed "s/./_/g"
    echo "$1"
    echo "$1" | sed "s/./=/g"
    out_message=$(echo $2 && echo "$2" | sed "s/\w/-/g" && echo "$3")
    echo "$out_message" | column -t -o "  |  "
    echo
    echo
}

# output_formats
fmt_split_nodes="%n"
fmt_short_node_status="%T"
fmt_nodename="%N"
fmt_n_sockets_per_node="%X"
fmt_n_cores_per_sockets="%Y"
fmt_allowed_groups="%g"
fmt_partition="%P"

SINFO_OUT=$(sinfo -o "\
$fmt_split_nodes \
$fmt_short_node_status \
$fmt_nodename \
$fmt_n_sockets_per_node \
$fmt_n_cores_per_sockets \
$fmt_allowed_groups \
")
header=$(echo "$SINFO_OUT" | head -n 1)

SINFO_OUT_allowed_idling_nodes=$(echo "$SINFO_OUT" | grep " all " | grep " idle ")
allowed_idling_nodes=$(echo "$SINFO_OUT_allowed_idling_nodes" | awk '{print $1}')

out_message_pre1=""
for node in $allowed_idling_nodes; do
    entry=$(echo "$SINFO_OUT_allowed_idling_nodes" | grep "$node ")
    n_sockets_per_node=$(echo "$entry" | awk '{print $4}')
    n_cores_per_socket=$(echo "$entry" | awk '{print $5}')
    n_cores_per_node=$(echo $n_sockets_per_node $n_cores_per_socket | awk '{print $1*$2}')

    RECORD_VALUES_OUT=$(sinfo -n $node -o "\
$fmt_partition \
$fmt_n_sockets_per_node \
$fmt_n_cores_per_sockets \
")
    partitions=$(echo "$RECORD_VALUES_OUT" | grep -v " 0 0 " | tail -n +2 | awk '{print $1}')
    partitions=$(echo $partitions | sed "s/ /,/g;s/\*//g")

    out_message_pre1="$out_message_pre1
$node $n_cores_per_node $partitions"
done
display_with_header "IDLE NODES" "NODENAME MAX_CORES_PER_NODE PARTITION_LIST" "$out_message_pre1"

n_cores_per_node_list=$(echo "$out_message_pre1" | awk '{print $2}' | sort | uniq)
out_message_pre2=""
for n_cores_per_node in $n_cores_per_node_list; do
    num_nodes=$(echo "$out_message_pre1" | grep "\S* $n_cores_per_node" | wc -l)
    out_message_pre2="$out_message_pre2
$n_cores_per_node $num_nodes"
done
display_with_header "GROUPED BY MAX_CORES_PER_NODE" "MAX_CORES_PER_NODE NUM_IDLE_NODES" "$out_message_pre2"

n_partition_list=$(echo "$out_message_pre1" | awk '{print $3}' | sort | uniq)
n_partition_list=$(echo $n_partition_list | sed "s/,/ /g;s/ /\n/g" | sort | uniq)
for partition in $n_partition_list; do
    num_nodes=$(sinfo -o "%D %t" -p $partition | tail -n +2 | grep " idle" | awk '{print $1}')
    num_nodes_total=$(sinfo -o "%D" -p $partition | tail -n +2)
    out_message_pre3="$out_message_pre3
$partition $num_nodes/$num_nodes_total"
done
display_with_header "GROUPED BY PARTITION" "PARTITION_NAME NUM_IDLE_NODES" "$out_message_pre3"
