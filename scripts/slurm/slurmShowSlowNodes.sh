#! /bin/bash


node_list_array=()
n_failed=0
for logFile in `find . -type f -name *log*`; do
    has_failed=`tail -n 5 $logFile | grep "SIGCONT" | wc -l`
    [[ $has_failed -eq 0 ]] && continue

    has_failed_due_to_time_limit=`tail -n 5 $logFile | grep "DUE TO TIME LIMIT" | wc -l`
    [[ $has_failed_due_to_time_limit -eq 0 ]] && continue

    echo "$logFile"

    nodelist_line=`head -n 5 $logFile | grep "NODELIST"`
    nodes_str=`echo $nodelist_line | awk '{print $NF}'`
    # echo "$nodes_str"

    out_of_brackets=`echo $nodes_str | sed "s/\[.*\]//g"`
    # echo "$out_of_brackets"

    inbrackets=`echo $nodes_str | grep -o "\[.*\]"`
    # echo "$inbrackets"

    node_list_clean=""
    if [[ "$inbrackets" != "" ]]; then
        nodes_list_wout_comma=`echo "$inbrackets" | sed "s/,/ /g;s/\[//g;s/\]//g"`
        for node_wo_comma in $nodes_list_wout_comma; do
            if [[ $node_wo_comma == *"-"* ]]; then
                node_upper_lower=`echo $node_wo_comma | sed "s/-/ /g"`
                node_array=($(echo "$node_upper_lower" | tr ' ' '\n'))
                for node in `seq "${node_array[0]}" "${node_array[1]}"`; do
                    node_list_clean="$node_list_clean $out_of_brackets$node"
                done
            else
                node_list_clean="$node_list_clean $out_of_brackets$node_wo_comma"
            fi
        done
    else
        node_list_clean=$out_of_brackets
    fi

    # echo "$node_list_clean"
    node_list_array+=($node_list_clean)
    ((n_failed+=1))

    # echo
done
echo "Nodes with failed jobs:"
echo "${node_list_array[@]}" | tr ' ' '\n' | sort | uniq -c | sort
echo "Number of failed jobs: $n_failed"
