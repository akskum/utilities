#!/usr/bin/env python

import subprocess
import os
from collections import namedtuple


if "PDC" in os.environ["HOST_ALIAS"]:
    command = ['/usr/bin/squeue', '-o', '"%u %t %D"', '-A', 'scania']

finished_command = subprocess.run(command, capture_output=True, text=True)

rows = [r.strip('"') for r in finished_command.stdout.split("\n")]
headers = [_h for _h in rows[0].split(" ") if _h]
SQUEUE = namedtuple("SQUEUE", headers)

parsed_rows = []
for _row in rows[1:]:
    _cols = [_c for _c in _row.split(" ") if _c]
    if len(_cols) != len(headers):
        continue
    parsed_rows.append(SQUEUE(*_cols))


usage=[]
for _u in set((r.USER for r in parsed_rows)):
    rows = [_r for _r in parsed_rows if _r.USER == _u]
    usage.append((_u, str(sum(int(_r.NODES) for _r in rows if _r.ST=="R")),str(sum(int(_r.NODES) for _r in rows if _r.ST!="R"))))

usage = sorted(usage, key=lambda _x: int(_x[1]), reverse=True)

heading = "SLURM USAGE STATS"
stdout = "=" * len(heading) + "\n" + heading + "\n" + "-" * len(heading) + "\n"
for _u in usage:
    stdout += (_u[0] + ":" + _u[1] + "," + _u[2]).ljust(15)
print(stdout)
