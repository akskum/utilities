#!/usr/bin/env python

import subprocess
import os
from collections import namedtuple

finished_command = subprocess.run("sinfo", capture_output=True, text=True)

interesting_partitions=[]
if "PDC" in os.environ["HOST_ALIAS"]:
    interesting_partitions=["scania"]

rows = finished_command.stdout.split("\n")
headers = [_h for _h in rows[0].split(" ") if _h]
SINFO = namedtuple("SINFO", headers)

interesting_rows = []
for _row in rows:
    _cols = [_c for _c in _row.split(" ") if _c]
    if len(_cols) != len(headers):
        continue
    _row = SINFO(*_cols)
    if _row.PARTITION in interesting_partitions:
        interesting_rows.append(_row)

heading = "SLURM NODE INFO"
stdout = "=" * len(heading) + "\n" + heading + "\n" + "-" * len(heading)
for _p in interesting_partitions:
    rows = [_r for _r in interesting_rows if _r.PARTITION == _p]
    stdout += "\n" + (_p + ":").ljust(15)
    for _r in rows:
        stdout+= (_r.AVAIL + "/" + _r.STATE + "(" + _r.NODES + ")").ljust(15)
print(stdout)
