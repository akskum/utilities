#!/bin/bash
#------------------------------------------------------------------------------
# Description
#   Creates a new set of job submit scripts that is tailored for the current folder 
#------------------------------------------------------------------------------
usage() {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat<<USAGE

Usage: ${0##*/}  <project> <variant> <doe> <condition> <meshtime> <simtime>
Creates a new set of job submit scripts that is tailored for the current folder 

USAGE
    exit 1
}

# parse options
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h | -help*)
        usage
        ;;
    -*)
        usage "unknown option: '$*'"
        ;;
    *)
        break
        ;;
    esac
done

[ $# -eq 4 ] || usage "Incorrect arguments specified"

# main routine
if [ ! -d  submit_scripts ]; then
    mkdir submit_scripts
fi

[[ $soldir == "" ]] && [[ ${PWD%akucwh*} != $PWD ]] && soldir="akucwh"
[[ $soldir == "" ]] && [[ ${PWD%SOL*} != $PWD ]] && soldir="SOL"
[[ $soldir == "" ]] && [[ ${PWD%sim*} != $PWD ]] && soldir="sim"

foamRoot=${PWD%$soldir*}$soldir
caseDir=${PWD#*/$soldir/}
scriptTemplatesDir=${PWD%$soldir*}.submit_scripts

for i in $scriptTemplatesDir/*_template.sh
do
(
    temp=`basename $i .sh`
    temp2=`echo ${temp%_template}".sh"`
    cp $i submit_scripts/$temp2
)
done

meshSlots=`grep numberOfSubdomains system/decomposeParDict.mesh | awk '{print $2}' | cut -d";" -f1`
meshNodes=`grep numberOfSubdomains system/decomposeParDict.mesh | awk '{print $2}' | cut -d";" -f1 | awk '{print $1/36}'`
meshTime=`grep timeout system/decomposeParDict.mesh | awk '{print $2}' | cut -d";" -f1`
simSlots=`grep numberOfSubdomains system/decomposeParDict.run | awk '{print $2}' | cut -d";" -f1`
simNodes=`grep numberOfSubdomains system/decomposeParDict.run | awk '{print $2}' | cut -d";" -f1 | awk '{print $1/36}'`
simTime=`grep timeout system/decomposeParDict.run | awk '{print $2}' | cut -d";" -f1`

sed -i "s/<PROJECTNR>/$1/g" submit_scripts/*.sh
sed -i "s/<VARIANT>/$2/g" submit_scripts/*.sh
sed -i "s/<DOE>/$3/g" submit_scripts/*.sh
sed -i "s/<FLOWCONDITION>/$4/g" submit_scripts/*.sh
sed -i "s/<MESHTIME>/$meshTime/g" submit_scripts/*.sh
sed -i "s/<MESHNODES>/$meshNodes/g" submit_scripts/*.sh
sed -i "s/<MESHSLOTS>/$meshSlots/g" submit_scripts/*.sh
sed -i "s/<SIMTIME>/$simTime/g" submit_scripts/*.sh
sed -i "s/<SIMNODES>/$simNodes/g" submit_scripts/*.sh
sed -i "s/<SIMSLOTS>/$simSlots/g" submit_scripts/*.sh
sed -i "s:<FOAM_ROOT>:$foamRoot:g" submit_scripts/*.sh
sed -i "s:<CASE_DIR>:$caseDir:g" submit_scripts/*.sh
sed -i "s:<PWD>:$foamRoot/$caseDir/submit_scripts:g" submit_scripts/*.sh
