#!/bin/bash

pathInBeskow="/cfs/scania/nobackup/a/akkumar/00_Ongoing/100_CAS1_2box_spray_F1/run/DF2006/skirt_doe/"
for i in 123 #{2..200}
do
(
    dir=$i
    if [ -d $dir ]; then
        echo $i
        rsync -trvl --update --exclude 'core*' --exclude 'dynam*' $i akkumar@beskow-scania.pdc.kth.se:$pathInBeskow
    fi
)
done
