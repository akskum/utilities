#!/bin/bash

# print the help info
usage () {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    # planned options:
    #   -da                 deletes intermediate ansa file if stl is generated successfully
    #   -dc                 deletes catia file if ansa is generated successfully
    cat <<USAGE

Usage: ${0##*/} [OPTIONS] ...
options:
  -d    | -delete  <pidnames>           deletes the pids
  -hi   | -hide    <pidnames>           hides the pids
  -f    | -fine    <pidnames>           pids where fine spacing is desired
  -ef   | -exfine  <pidnames>           pids where extra fine spacing is desired
  -a    | -ansaonly                     Generate only ansa file [requires geometry.CATPart]
  -s    | -stl                          Generate stl file [requires geometry.CATPart or geometry.ansa]
  -h    | -help                         help

Generates an .stl file from a .CATPart. There is no guarantee that a watertight mesh will be created. Also it is possible that some unmeshed macros do not make it to the STL. Check the log.

USAGE
if [ -f geometry.ansa ]
then
~/apps/utilities/scripts/ansa_scripts/list_pids.sh 2>/dev/null | sed -n '/^The PIDs in the ansa file are listed below/,/^Done/p;/^Done/q'
fi  
    exit 1
}

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

readInput()
{
    while [ "$#" -ge 2 ]; do echo -e "$1"; shift; done
    read input
    [ -z $input ] 2>/dev/null && input=$1
}

echo "Starting blockMeshDict and snappyHexMeshDict generator script"

systemFolder="./system"
#systemFolder="."
constantFolder="./constant"
#constantFolder="."
triSurfaceFolder="triSurface"
#triSurfaceFolder="."
bmdname=$systemFolder/blockMeshDict
shmdname=$systemFolder/snappyHexMeshDict

[ "$#" -ge 2 ] || die "Enter 2 arguments"
dobmd=false
doshmd=false
[ $1 == true ] && dobmd=true
[ $2 == true ] && doshmd=true

defaultstlname="geo.stl"
readInput "Default stl is "$defaultstlname"\nPress return key to accept [OR] Type in a stl name" $defaultstlname; stlname=$input;
while [ ! -f $constantFolder/$triSurfaceFolder/$stlname ]; do readInput "No stl named "$stlname" found. Retry with correct stl name" ""; stlname=$input; done
fullstlname=$constantFolder/$triSurfaceFolder/$stlname

echo "Fount stl file named "$fullstlname

# BEGIN BLOCKMESHDICT GENERATION
if [ $dobmd == true ]
then
    rm -rf $bmdname
    cp "$bmdname".template $bmdname

    echo "Performing bounding box analysis for "$fullstlname
    stlstats=$(python ~/git/utilities/scripts/python/stl_statistics.py $fullstlname | sed 's,x ,x=,g' | sed 's,n ,n=,g')
    eval $stlstats
    
    defaultdeltas=8
    readInput "Default cell size is "$defaultdeltas"\nPress return key to accept [OR] Type in a new positive integer value for cell size" $defaultdeltas
    while [ "$input" -le 0 ] 2>/dev/null; do readInput $input" is not a valid positive integer value. Please retry" ""; done
    [ "$input" -ge 0 ] 2>/dev/null && deltas=$input || deltas=$defaultdeltas
    echo "Current cell size is "$deltas; 
    ncellsx=$(echo $xmin $xmax $deltas | awk '{ print int( ($2 - $1) / $3 ) }')
    ncellsy=$(echo $ymin $ymax $deltas | awk '{ print int( ($2 - $1) / $3 ) }')
    ncellsz=$(echo $zmin $zmax $deltas | awk '{ print int( ($2 - $1) / $3 ) }')
    
    # Replacing entries in dictionary
    if [ $(grep "^xmin" $bmdname | wc -l) -gt 0 ]
    then
        echo "Replacing x,y,z min/max extents in "$bmdname
        sed -i "/^xmin/{s/xmin.*$/xmin\t$xmin;/g}" $bmdname
        sed -i "/^ymin/{s/ymin.*$/ymin\t$ymin;/g}" $bmdname
        sed -i "/^zmin/{s/zmin.*$/zmin\t$zmin;/g}" $bmdname
        sed -i "/^xmax/{s/xmax.*$/xmax\t$xmax;/g}" $bmdname
        sed -i "/^ymax/{s/ymax.*$/ymax\t$ymax;/g}" $bmdname
        sed -i "/^zmax/{s/zmax.*$/zmax\t$zmax;/g}" $bmdname
    
        echo "Replacing x,y,z delta values in "$bmdname
        sed -i "/^deltax/{s/deltax.*$/deltax\t$deltas;/g}" $bmdname
        sed -i "/^deltay/{s/deltay.*$/deltay\t$deltas;/g}" $bmdname
        sed -i "/^deltaz/{s/deltaz.*$/deltaz\t$deltas;/g}" $bmdname
    else
        echo "Replacing bounding box in "$bmdname
        foamDictionary $bmdname -entry vertices -set "\
            (\
                ($xmin $ymin $zmin)\
                ($xmax $ymin $zmin)\
                ($xmax $ymax $zmin)\
                ($xmin $ymax $zmin)\
                ($xmin $ymin $zmax)\
                ($xmax $ymin $zmax)\
                ($xmax $ymax $zmax)\
                ($xmin $ymax $zmax)\
            )" >/dev/null 2>&1
    
        echo "Replacing number of cells in "$bmdname
        foamDictionary $bmdname -entry blocks -set "\
            (\
            hex ( 0 1 2 3 4 5 6 7 ) \
            ( $ncellsx $ncellsy $ncellsz )\
            simpleGrading ( 1 1 1 )\
            )" >/dev/null 2>&1
    
        # Final procesing of blockMeshDict
        sed -i "/vertices/{s/(/\n(/g;s/);/\n);/g;s/( \n(/(\n\t(/g;s/) \n(/)\n\t(/g;}" $bmdname
    fi
fi
# END BLOCKMESHDICT GENERATION

# BEGIN SNAPPYHEXMESHDICT GENERATION
if [ $doshmd == true ]
then
    rm -rf $shmdname
    cp "$shmdname".template $shmdname

    echo "Replacing stl name in"$shmdname
    foamDictionary $shmdname -entry geometry -remove >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry -set {} >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.STLNAME -set {} >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.STLNAME.type -set triSurfaceMesh >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.STLNAME.name -set stl >/dev/null 2>&1
    sed -i "s/STLNAME/$stlname/g" $shmdname
    
    echo "Adding global refinement to "$shmdname foamDictionary $shmdname -entry geometry.refinementBox -set {} >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.refinementBox -set {} >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.refinementBox.max -set \
        " ( $xmax $ymax $zmax ) " >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.refinementBox.min -set \
        " ( $xmin $ymin $zmin ) " >/dev/null 2>&1
    foamDictionary $shmdname -entry geometry.refinementBox.type -set "searchableBox" >/dev/null 2>&1
    
    readInput "How many levels of global refinement? 0=no refinement, default 1" 1
    gref=$input
    foamDictionary $shmdname -entry castellatedMeshControls.refinementRegions -remove >/dev/null 2>&1
    foamDictionary $shmdname -entry castellatedMeshControls.refinementRegions -set {} >/dev/null 2>&1
    foamDictionary $shmdname -entry castellatedMeshControls.refinementRegions.refinementBox -set {}  >/dev/null 2>&1
    foamDictionary $shmdname -entry castellatedMeshControls.refinementRegions.refinementBox.levels -set "( (1e15 $gref) )" >/dev/null 2>&1
    foamDictionary $shmdname -entry castellatedMeshControls.refinementRegions.refinementBox.mode -set "inside" >/dev/null 2>&1
    
    foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions -remove >/dev/null 2>&1
    foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions -set {} >/dev/null 2>&1
    pidlist=""
    for pid in $(grep endsolid $fullstlname | awk '{print $2}')
    do
        foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions.$pid -set {} >/dev/null 2>&1
        printf "Enter refinement for $pid [default 2 3]"
        readInput "Enter refinement for $pid [default 2 3]" "2 3"
        lref=$input
        foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions.$pid.level -set "($lref)" >/dev/null 2>&1
        pidlist="${pid} ${pidlist}"
    done
    
    readInput "Which surfaces are patches?\n$pidlist" ""
    patchlist=$input
    
    for patch in $patchlist
    do
        foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions."$patch".patchInfo -set {} >/dev/null 2>&1
        foamDictionary $shmdname -entry castellatedMeshControls.refinementSurfaces.stl.regions."$patch".patchInfo.type -set patch >/dev/null 2>&1
    done
    
    readInput "Which surfaces need layers?\n$pidlist" ""
    layerlist=$input
    foamDictionary $shmdname -entry addLayersControls.layers -remove >/dev/null 2>&1
    foamDictionary $shmdname -entry addLayersControls.layers -set {} >/dev/null 2>&1
    for patch in $layerlist
    do
        foamDictionary $shmdname -entry addLayersControls.layers.stl_"$patch" -set {} >/dev/null 2>&1
        foamDictionary $shmdname -entry addLayersControls.layers.stl_"$patch".nSurfaceLayers -set 2 >/dev/null 2>&1
    done
    
    
    echo "Enter locations in mesh line by line followed by an empty line"
    line=""
    locinmesh=""
    while [[ $line != "END" ]] 2>/dev/null ; do locinmesh="${locinmesh} ${line}"; readInput "END"; line=$input; done
    foamDictionary $shmdname -entry castellatedMeshControls.locationsInMesh -set "( $locinmesh )" >/dev/null 2>&1
    sed -i "/locationsInMesh/{s/( ( (/(\n\t\t( (/g;s/) ( (/)\n\t\t( (/g;s/locationsInMesh (/locationsInMesh\n\t(/g;s/);/\n\t);/g;}" $shmdname
fi

exit 0
