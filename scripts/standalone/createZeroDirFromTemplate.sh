#!/bin/bash

rm 0 -rf
cp 0.generic 0 -rf
for field in `ls 0`
do
(
    for boundary in `foamDictionary -entry entry0 -keywords constant/polyMesh/boundary`
    do
    (
        foamDictionary -entry boundaryField.$boundary -add {} 0/$field >/dev/null 2>&1
        for keywords in `foamDictionary -entry boundaryField.* -keywords 0/$field`
        do
        (
           foamDictionary -entry boundaryField.$boundary.$keywords -add "`foamDictionary -entry boundaryField.*.$keywords -value 0/$field`" 0/$field >/dev/null 2>&1
        )
        done
    )
    done
    sed -i "s/\"\.\*\"/DEFAULTPATCHNAME/g" 0/$field
    foamDictionary -entry boundaryField.DEFAULTPATCHNAME -remove 0/$field >/dev/null 2>&1
)
done
changeDictionary
