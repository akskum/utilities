#!/bin/bash

currentDir=$PWD
sourceInputPath=$1
targetInputPath=$2

echo ""
echo ""
echo "############### Starting script ##############"
echo ""

if [[ "$sourceInputPath" = /* ]]
then
    cd "$sourceInputPath"
    sourcePath=$PWD
else
    cd "$currentDir/$sourceInputPath"
    sourcePath=$PWD
fi
echo "Source case is located in : "$sourcePath

if [[ "$targetInputPath" = /* ]]
then
    cd "$targetInputPath"
    targetPath=$PWD
else
    cd "$currentDir/$targetInputPath"
    targetPath=$PWD
fi
echo "Target target is located in : "$targetPath; echo ""

cd $targetPath
rm -rf 0.template
[ -f system/changeDictionaryDict ] && rm system/changeDictionaryDict

cd $sourcePath
rm -rf 0.template
[ -f system/changeDictionaryDict.template ] && rm system/changeDictionaryDict.template

cp 0 0.template -r
echo "Available source patches are:"

command=$([ -f constant/polyMesh/boundary ] && echo 'foamDictionary -entry entry0 -keywords constant/polyMesh/boundary' || [ ! -f 0/U ] || echo 'foamDictionary -entry boundaryField -keywords 0/U')
for boundary in `$command | sort`
do
(
    printf $boundary
    printf "\t"
)
done
echo ""; echo ""

echo -n -e "For default mappable BC, enter the name of ANY ONE source patch [followed by return key]\n"
read repPatch

for field in `ls 0.template`
do
(
    for boundary in `foamDictionary -entry boundaryField -keywords 0.template/$field`
    do
    (
        if [[ $boundary == $repPatch ]]; then
            foamDictionary -entry boundaryField.DEFAULTPATCHNAME -add {} 0.template/$field >/dev/null 2>&1
            for keywords in `foamDictionary -entry boundaryField.$boundary -keywords 0.template/$field`
            do
            (
                foamDictionary -entry boundaryField.DEFAULTPATCHNAME.$keywords -add "`foamDictionary -entry boundaryField.$boundary.$keywords -value 0.template/$field`" 0.template/$field >/dev/null 2>&1
            )
            done
            sed -i "s/DEFAULTPATCHNAME/\".*\"/g" 0.template/$field
        fi
        if [[ $boundary != ".*" ]]; then
            foamDictionary -entry boundaryField.$boundary -remove  0.template/$field >/dev/null 2>&1
        fi
    )
    done
)
done

cd $targetPath
echo "Available target patches are:"
command=$([ -f constant/polyMesh/boundary ] && echo 'foamDictionary -entry entry0 -keywords constant/polyMesh/boundary' || [ ! -f constant/triSurface/geo.stl ] || echo "grep endsolid constant/triSurface/geo.stl | awk '{print 'stl_'$2}'")
for boundary in `$command | sort`
do
(
    printf $boundary
    printf "\t"
)
done
echo ""; echo ""

echo -n -e "For non-default mappable BCs, enter the names of the source-target patch pairs [format: SOURCE_PATCH_1 TARGET_PATCH_1 SOURCE_PATCH_2 TARGET_PATCH_2 ....] [followed by return key]\n"
read specialPatch

cd $sourcePath
cp $FOAM_ETC/caseDicts/annotated/changeDictionaryDict system/changeDictionaryDict.template
for keywords in `foamDictionary -keywords system/changeDictionaryDict.template`
do
(
    if [[ $keywords != "FoamFile" ]]; then
        foamDictionary -entry $keywords -remove system/changeDictionaryDict.template >/dev/null 2>&1
    fi
)
done
for field in `ls 0`
do
(
    foamDictionary -entry $field -add {} system/changeDictionaryDict.template >/dev/null 2>&1
    foamDictionary -entry $field.internalField -add "`foamDictionary -entry internalField -value 0/$field`" system/changeDictionaryDict.template >/dev/null 2>&1
    foamDictionary -entry $field.boundaryField -add {} system/changeDictionaryDict.template >/dev/null 2>&1
)
done        
set junk $specialPatch
shift
while [ $# -ge 2 ]; do
    echo "Source target pair: $1 $2"
    for field in `ls 0`
    do
    (
        for boundary in `foamDictionary -entry boundaryField -keywords 0/$field`
        do
        (
            if [[ $boundary == $1 ]]; then
                foamDictionary -entry $field.boundaryField.REPLACEPATCHNAME -add {} system/changeDictionaryDict.template >/dev/null 2>&1
                for keywords in `foamDictionary -entry boundaryField.$boundary -keywords 0/$field`
                do
                (
                    foamDictionary -entry $field.boundaryField.REPLACEPATCHNAME.$keywords -add "`foamDictionary -entry boundaryField.$boundary.$keywords -value 0/$field`" system/changeDictionaryDict.template >/dev/null 2>&1
                )
                done
                sed -i "s/REPLACEPATCHNAME/$2/g" system/changeDictionaryDict.template
            fi
        )
        done        
    )
    done        
shift 2
done

mv $sourcePath/0.template $targetPath/.
mv $sourcePath/system/changeDictionaryDict.template $targetPath/system/changeDictionaryDict
cd $currentDir
