#!/bin/bash

# fatal error message
die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    echo
    echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

hard_coded_pid_name=""

[[ $# -ge 1 ]] || die "Please specify whether to -trim or -notrim by \"__\""

# parse options
unset optDebug optEnvName optStrip optVerbose
while [ "$#" -gt 0 ]
do
    case "$1" in
    -trim )
        trim="true"
        ;;
    -notrim )
        trim="false"
        ;;
    *)
        hard_coded_pid_name="$1"
        ;;
    esac
    shift
done

[[ $trim == "true" ]] || [[ $trim == "false" ]] || die "Please specify whether to -trim or -notrim by \"__\""

for stlfile in `find . -maxdepth 2 -type f -name "*.stl"`; do
    basenameSTL=`basename $stlfile`
    stlname=${basenameSTL%.stl}

    [[ $trim == "true" ]] && pidname=${stlname##*__}
    [[ $trim == "false" ]] && pidname=$stlname

    nEndSolids=`grep endsolid $stlfile | wc -l`
    [[ $nEndSolids -eq 1 ]] || continue

    [[ $hard_coded_pid_name == "" ]] || pidname=$hard_coded_pid_name
    echo "pidname is " $pidname

    nSolids=`grep "solid" $stlfile | wc -l`
    [[ $nSolids -eq 2 ]] || continue

    nPIDSolids=`grep "solid $pidname" $stlfile | wc -l`
    [[ $nPIDSolids -eq 2 ]] && continue

    echo "Renaming solids to \"$pidname\" in $stlfile"
    sed -i "s/solid.*/solid $pidname/g" $stlfile 

    nCorrectSolids=`grep "^solid $pidname" $stlfile | wc -l`
    nCorrectEndSolids=`grep "endsolid $pidname" $stlfile | wc -l`

    [[ $nCorrectSolids -eq 1 ]] || echo "Unsuccessful!"
    [[ $nCorrectEndSolids -eq 1 ]] || echo "Unsuccessful!"

    [[ $hard_coded_pid_name == "" ]] || new_stl_file_name=`dirname $stlfile`/$hard_coded_pid_name.stl
    [[ $new_stl_file_name == "" ]] || mv $stlfile $new_stl_file_name
    echo "Successful!"
    echo
done
