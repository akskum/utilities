#!/bin/bash

die()
{
    exec 1>&2
    echo
    echo "Error encountered:"
    while [ "$#" -ge 1 ]; do echo "    $1"; shift; done
    # echo
    # echo "See '${0##*/} -help' for usage"
    echo
    exit 1
}

# ---------------
# Checking inputs
# ---------------
[[ "$#" -ge 1 ]] || die "Please specify a stl file"

input_File=$1
[[ -f $input_File ]] || die "Please specify a valid stl file"

[[ $input_File == *.stl ]] || die "Please specify a valid stl file"

desired_pid=$2

line_len=`cat $input_File | wc -l | awk '{print $1 + 10}'`
for pid in `cat $input_File | grep "endsolid" | awk '{print $2}'`; do
    if [[ $pid == $desired_pid ]] || [[ $desired_pid == "" ]]; then 
      echo "Extracting $pid ..."
      #grep "^solid $pid" $input_File -A$line_len | grep "endsolid $pid" -B$line_len | grep -v ".*color.*" > "$pid".stl
      cat $input_File | sed -n "/^solid $pid/,/^endsolid $pid/p" > "$pid".stl
      echo "Done!"
      echo 
    fi
done
