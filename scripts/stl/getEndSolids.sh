#! /bin/bash

[[ "$#" -eq 0 ]] && files=`ls constant/triSurface/*` || files="$@"

echo
for file in $files
do
    if [[ ${file##*\.} == "stl" ]]
    then
        echo "$file"
        echo "$file" | sed "s/./_/g"
        echo $(grep "endsolid" $file | sed "s/endsolid //g" | sort | sed 's/\n/ /g')
        # grep "endsolid" $file | sed "s/endsolid //g" | sort | sed 's/^/    /g'
        echo
    fi
done

