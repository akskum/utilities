import sys, getopt
import matplotlib as mpl
#mpl.use('TkAgg')
from matplotlib import ticker
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('text', usetex=True)
import pandas as pd
from scipy.stats import spearmanr

scatter=False

filename = "results_sweep_all.dat"
doe_identifier = "id"
xnames = [ 'bot_right_corner_offset[mm]', 'top_right_corner_offset[mm]', 'top_left_corner_offset[mm]', 'bot_left_corner_offset[mm]' ]
ynames = [ 'Tvolc_in', 'Tcup', 'Tevap', 'dp' ]

argv = sys.argv
opts, args = getopt.getopt(argv[1:],"i:s")
for opt, arg in opts:
    if opt == "-i":
        filename = arg
    if opt == "-s":
        scatter = True

# ==========================
# program begins here
# ==========================

# read the files and create the variables
v = pd.read_csv(filename, sep="\s+")

nx = len(xnames)
ny = len(ynames)

if scatter:
    fig, ax = plt.subplots(ny, nx, sharex=False, sharey=False, figsize=(16,9))
else:
    fig, ax = plt.subplots(ny, nx, sharex=True, sharey=True, figsize=(16,9))
    #plt.ylim(-1, 1)
    #plt.xlim(-1, 1)

corrCoeffs = []
for j in range(ny):
    y = v[ynames[j]]
    cc = []
    for i in range(nx):
        x = v[xnames[i]]
        (corr, pval) = spearmanr(x, y)
        cc.append(corr)
    corrCoeffs.append(cc)
nColors = 100
vmin = (np.matrix(corrCoeffs)).min()
vmax = (np.matrix(corrCoeffs)).max()
cspace = np.linspace(0, 1, nColors)
colors = cm.RdBu(cspace)

for j in range(ny):
    y = v[ynames[j]]
    for i in range(nx):
        x = v[xnames[i]]
        corr = corrCoeffs[j][i]
        idx = int((nColors-1)*(corr-vmin)/(vmax-vmin))
        col = colors[idx]

        this_ax = ax[j][i]
        if scatter:
            this_ax.scatter(x,y,s=5)
        else:
            this_ax.bar(0,corr, color = col)
            this_ax.xaxis.set_ticks([])

        this_ax.grid()
        this_ax.set_title("corr = {:.2f}".format(corr))
        if j == ny-1:
            this_ax.set_xlabel(xnames[i])
        else:
            this_ax.xaxis.set_ticklabels([])

        if i == 0:
            this_ax.set_ylabel(ynames[j])
        else:
            this_ax.yaxis.set_ticklabels([])

        #print("{} {} {}".format(xnames[j],ynames[i],corr))

        plt.subplots_adjust(wspace=None, hspace=None)

plt.savefig("correlation_matrix.png")
plt.show()

