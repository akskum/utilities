import numpy as np
import pandas as pd

areaCutOff = 1.0e-5;
filename = "TAvprofile.dat"
xname  = [ "Temperature" ]

v = pd.read_csv(filename, sep="\s+")
names = v.keys()

x = np.array(v[xname])
y = []
for n in names:
    yn = v[n]
    area = 0.0
    for i, ai in enumerate(yn):
        area += ai
        if area > areaCutOff:
            if i == 0:
                T0 = x[i][0]
            else:
                T0 = x[i-1][0]
            T1 = x[i][0]
            T = (T1-T0)*(areaCutOff - area + ai)/ai + T0
            print("{} {}".format(n,T))
            break
