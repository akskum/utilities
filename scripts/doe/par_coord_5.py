import sys, getopt
import matplotlib as mpl
#mpl.use('TkAgg')
from matplotlib import ticker
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

import pandas as pd

filename = "result.dat"
doe_identifier = "id"
#id dp Tshell Tinj Tfunnel Tinlet Tdown
xnames = [ 'T_cone_volcano_MB1', 'T_cone_volcano_MB2', 'T_cone_volcano_MB3', 'T_evap_mixer_MB1', 'T_evap_mixer_MB2', 'T_evap_mixer_MB3', 'T_MixCh_MB1',  'T_MixCh_MB2',  'T_MixCh_MB3'] #, 'util_area_evap_MB1', 'util_area_evap_MB2', 'util_area_evap_MB3', 'ht_transfer_evap_MB1', 'ht_transfer_evap_MB2', 'ht_transfer_evap_MB3', 'dp_MB1' ,'dp_MB2' ,'dp_MB3' ]
nTicks = 11
num_ticks = {}
tickermarker = 'r_'
tickerwidth = 3
markersize = 15
showGrid = False
textAngle = 20
title='Skirt, evaporator and mixer DOE'
showTickers = False

# annotate the DOE id's either first or last
annotate = 'first'
#annotate = 'last'

prop_cycle = mpl.rcParams['axes.prop_cycle']
cols = prop_cycle.by_key()['color']
# if the min/max values exist we use those, otherwise we use min/max tickers
min_ticker = {}
max_ticker = {}

min_value = {}
max_value = {}

min_max = {}
hard_min_max = {}
ids2plot = 0
idUsed = []

argv = sys.argv
opts, args = getopt.getopt(argv[1:],"i:")
for opt, arg in opts:
    if opt == "-i":
        filename = arg

def initialize_tickers():
    #min_value["T_MixCh"] = 540.0
    #max_value["T_MixCh"] = 570.0
    min_value["T_evap_mixer_MB1"] = 480.0
    min_value["T_evap_mixer_MB2"] = 480.0
    min_value["T_evap_mixer_MB3"] = 480.0
    #max_value["T_evap_mixer"] = 520.0
    #min_value["T_cone_volcano_MB1"] = 460.0
    #min_value["T_cone_volcano_MB2"] = 460.0
    #min_value["T_cone_volcano_MB3"] = 460.0
    #max_value["T_cone_volcano1"] = 0.0
    #min_value["util_area_evap_MB1"] = 0.101
    #min_value["util_area_evap_MB2"] = 0.101
    #min_value["util_area_evap_MB3"] = 0.101
    #max_value["util_area_evap1"] = 0.0
    #min_value["ht_transfer_evap"] = 2.9
    #max_value["ht_transfer_evap1"] = 0.0
    #min_value["dp1"] = 0.0
    #max_value["dp1"] = 0.0
    pass
    return

def useHardCodedMinMax():
    hard_min_max["T_MixCh_MB1"] = [ 400.0, 610.0]
    hard_min_max["T_MixCh_MB2"] = [ 400.0, 610.0]
    hard_min_max["T_MixCh_MB3"] = [ 400.0, 610.0]
    hard_min_max["T_evap_mixer_MB1"] = [ 400.0, 610.0]
    hard_min_max["T_evap_mixer_MB2"] = [ 400.0, 610.0]
    hard_min_max["T_evap_mixer_MB3"] = [ 400.0, 610.0]
    hard_min_max["T_cone_volcano_MB1"] = [ 400.0, 610.0]
    hard_min_max["T_cone_volcano_MB2"] = [ 400.0, 610.0]
    hard_min_max["T_cone_volcano_MB3"] = [ 400.0, 610.0]
    #hard_min_max["util_area_evap_MB1"] = [ 0.09, 0.13]
    #hard_min_max["util_area_evap_MB2"] = [ 0.09, 0.13]
    #hard_min_max["util_area_evap_MB3"] = [ 0.09, 0.13]
    #hard_min_max["ht_transfer_evap"] = [ 2.4, 3.3]
    #hard_min_max["dp_MB1"] = [ 2100.0, 2700.0]
    #hard_min_max["dp_MB2"] = [ 2100.0, 2700.0]
    #hard_min_max["dp_MB3"] = [ 2100.0, 2700.0]
    return

def init_numTicks():
    for name in xnames:
        num_ticks.update( { name : nTicks } )
    num_ticks['dp1'] = 50

        
def filter_plot(variables, idx):

    global ids2plot, idUsed, min_value, max_value

    nc = np.size(cols)

    plt = True
    for i, name in enumerate(names):
        val = variables[name][idx]
        v_min = min_ticker[name]
        v_max = max_ticker[name]

        if name in min_value.keys():
            v_min = np.true_divide(min_value[name]-min_max[name][0], min_max[name][1]-min_max[name][0])
        if name in max_value.keys():
            v_max = np.true_divide(max_value[name]-min_max[name][0], min_max[name][1]-min_max[name][0])


        if v_min < v_max:
            if ( (val < v_min) | (val > v_max) ):
                plt = False
        else:
            if plt:
                plt = False
                if ( (val < v_max) | (val > v_min) ):
                    plt = True

    if plt:
        alpha = 1
        strplt = '-o'
        lineWidth = 1
        if idUsed[idx] == -1:
            idUsed[idx] = ids2plot
            ids2plot = ids2plot + 1
        col = cols[ idUsed[idx] % nc ]
        print(doe_id[idx])
    else:
        alpha = 0.2
        col = 'gray'
        strplt = '-'
        lineWidth = 0.5
        plt = True # set this to true to plot it anyway, 

    if doe_id[idx] == 122:
        alpha = 1
        col = 'k'
        strplt = '-o'
        lineWidth = 2

    return plt, alpha, col, strplt, lineWidth

# Set the tick positions and labels on y axis for each plot
# Tick positions based on normalised data
# Tick labels are based on original data
def set_ticks_for_axis(variables, dim, ax, ticks):
    min_val, max_val = min_max[names[dim]]
    val_range = max_val - min_val
    step = val_range / float(ticks-1)
    tick_labels = [round(min_val + step * i, 5) for i in range(ticks)]
#    norm_min = variables[names[dim]].min()
#    norm_range = np.ptp(variables[names[dim]])
#   override this
    norm_min = 0.0
    norm_range = 1.0
    norm_step = norm_range / float(ticks-1)
    ticks = [round(norm_min + norm_step * i, 5) for i in range(ticks)]
    ax.yaxis.set_ticks(ticks)
    ax.set_yticklabels(tick_labels)

# Get min, max and range for each column
# Normalize the data for each column
def normalize_data(variables, names):

    useHardCodedMinMax()
    hardKeys = hard_min_max.keys()

    for n in names:

        if n in hardKeys:
            min_max[n] = [hard_min_max[n][0], hard_min_max[n][1]]
        else:
            mi = variables[n].min()
            ma = variables[n].max()
            if ma == mi:
                ma = mi + 1.0e-3
            delta = 1.0e-2*(ma-mi)
            min_max[n] = [mi-delta, ma+delta]

        # rescale variables
        variables[n] = np.true_divide(variables[n]-min_max[n][0], min_max[n][1]-min_max[n][0])

# only plot the names, variables may contain many more 
def parallel_coordinates(variables, names, doe_id):

    # Plot each row
    nAxes = np.size(ax)
    for i, ai in enumerate(ax):
        # get the x and y coordinates for 'this' axis
        xp = [ x[i], x[i+1] ]
        y = []
        for idx, y1 in enumerate(variables[names[i]]):
            y.append([ y1, variables[names[i+1]][idx] ])

        nc = np.size(cols)
        nBins = 50
        annotate_bins = []
        da = 0.07
        for ix in range(nBins):
            annotate_bins.append(0.0)

        # the filter_plot routine controls which lines to plot
        for ii, yp in enumerate(y):
            pt, alpha, col, strplt, lineWidth = filter_plot(variables, ii)
            if pt:
                ai.plot(xp, yp, strplt, alpha=alpha, c=col, linewidth=lineWidth)
                if (i == 0) & (alpha > 0.9) & ( annotate == 'first'):
                    txt="{}".format(doe_id[ii])
#                    ai.annotate(txt, (xp[0],yp[0]))
                    idx = int(nBins*yp[0])
                    idx = min(idx, nBins-1)
                    #col = cols[ idUsed[ii] % nc ]
                    ai.annotate(txt, (xp[0]+da*annotate_bins[idx],yp[0]), color=col)
                    annotate_bins[idx] = annotate_bins[idx] + 1

                if ((i == nAxes-1) & (alpha > 0.9) & ( annotate == 'last') ):
                    txt="{}".format(doe_id[ii])
                    rlx = 0.08
                    xPos = xp[1] + rlx*(xp[0]-xp[1])
                    ai.annotate(txt, (xPos,yp[1]))

        # print tickers
        if showTickers:
            name = names[i]
            name1 = names[i+1]
            ai.plot(x[i], min_ticker[name],tickermarker,markersize=markersize,mew=tickerwidth)
            ai.plot(x[i], max_ticker[name],tickermarker,markersize=markersize,mew=tickerwidth)
            ai.plot(x[i+1], min_ticker[name1],tickermarker,markersize=markersize,mew=tickerwidth)
            ai.plot(x[i+1], max_ticker[name1],tickermarker,markersize=markersize,mew=tickerwidth)

        ai.set_xlim([x[i], x[i+1]])
        ai.set_ylim(-0.002, 1.002)
        if showGrid:
            ai.grid()

    for dim, axi in enumerate(ax):
        axi.spines['bottom'].set_visible(False)
        axi.spines['top'].set_visible(False)
        axi.xaxis.set_major_locator(ticker.FixedLocator([dim]))
        axi.set_xticklabels([names[dim]], rotation=textAngle)
        set_ticks_for_axis(variables, dim, axi, ticks=num_ticks[xnames[dim]])

    # Move the final axis' ticks to the right-hand side
    axi = plt.twinx(ax[-1])
    axi.spines['bottom'].set_visible(False)
    axi.spines['top'].set_visible(False)
    axi.xaxis.set_major_locator(ticker.FixedLocator([x[-2], x[-1]]))
    axi.set_xticklabels([names[-2], names[-1]])
    set_ticks_for_axis(variables, -1, axi, ticks=num_ticks[xnames[-1]])
    if ("T_" in xnames[-1]):
        axi.spines['right'].set_color('red')
    if ("T_" in xnames[-2]):
        axi.spines['left'].set_color('red')

    for dim, axi in enumerate(ax):
        if ("T_" in xnames[dim]):
            axi.spines['left'].set_color('red')

# Remove space between subplots
    plt.subplots_adjust(wspace=0)
    plt.suptitle(title)
    plt.savefig("plot_par.png")
    plt.show()

# ==========================
# program begins here
# ==========================

# read the files and create the variables to pass to the plot routine
v = pd.read_csv(filename, sep="\s+")
vars = {}

# we need the names variable to get the order correct
names = []
doe_id = []

for i in range(len(v[xnames[0]])):
    idUsed.append(-1)

for j in range(np.size(xnames)):
    name = xnames[j]
    names.append(name)
    vars.update( { name : v[name] } )

for j,v in enumerate(v[doe_identifier]):
    doe_id.append(v)

for i in range(np.size(names)):
    min_ticker.update( { names[i] : 0.0 } )
    max_ticker.update( { names[i] : 1.0 } )

normalize_data(vars, names)
initialize_tickers()
init_numTicks()

x = [i for i in range(np.size(names))]
    
fig, ax = plt.subplots(1, len(names)-1, sharey=False, figsize=(16,9))
parallel_coordinates(vars, names, doe_id)
