#!/bin/bash

case=m700
for i in 123 #{1..200}
do
(
    dir=$i/$case
    if [ -d $dir ]; then
	    cd $dir
	    if [ -d 10000 ]; then
            echo $i
	        python ../../readProfile.py > TAvcut.dat
	    fi
    fi
)
done
