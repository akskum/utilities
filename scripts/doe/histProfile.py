import numpy as np
import pandas as pd

areaCutOff = 1.0e-5;
filename = "TAvprofile.dat"
created_filename = "TAvprofile_filtered.dat"
xname  = [ "Temperature" ]
ynames = [ "stl_Wall_Inner_Evap_Mixer"]
v = pd.read_csv(filename, sep="\s+")

x = np.array(v[xname])
y = []

inlet_temp = 593
margin_max_temp = 590
min_allowable_temp = 490

for n in ynames:
    yn = v[n]
    full_area = 0
    util_area = 0
    heat_transfer_value = 0
    for i, ai in enumerate(yn):
        full_area += ai
        if x[i][0] < margin_max_temp:
            util_area += ai
        if x[i][0] < inlet_temp:
            heat_transfer_value += ai*(inlet_temp - x[i][0])
    print("util_area_{} {}".format(n,util_area))
    print("ht_transfer_{} {}".format(n,heat_transfer_value))
