#!/bin/bash

#ynames = [ "stl_CFD_Shell", "stl_CFD_Doser", "stl_funnel", "stl_Inlet_Volume", "stl_HV_HGC_DownPipe" ]
result="result_MB2.dat"
echo "id dp T_MixCh T_evap_mixer T_cone_volcano util_area_evap ht_transfer_evap" > $result
case=m700
for i in {1..200}
do
(
    dir=$i
    if [ -d $dir ]; then
	file=$dir/$case/histCut.dat
	file2=$dir/$case/simpleReactingParcelAvFoam.log
	if [ -f $file ]; then 
	    dp=`grep 'INFO: dp' $file2 | tail -5000 | awk '{sum+=$5}END{print sum/NR}'`

	    t1=`grep -w 'stl_Wall_Turn_Chamber_Refined' $file | awk '{print $2}'`
	    t2=`grep -w 'stl_Wall_Inner_Evap_Mixer' $file | awk '{print $2}'`
	    t3=`grep -w 'stl_Wall_cone' $file | awk '{print $2}'`
	    t4=`grep -w 'util_area_stl_Wall_Inner_Evap_Mixer' $file | awk '{print $2}'`
	    t5=`grep -w 'ht_transfer_stl_Wall_Inner_Evap_Mixer' $file | awk '{print $2}'`

	    str=`printf "%.3i %.2f %.2f %.2f %.2f %.4f %.4f\n" $i $dp $t1 $t2 $t3 $t4 $t5`
            echo $str
            echo $str >> $result
	fi
    fi
)
done
