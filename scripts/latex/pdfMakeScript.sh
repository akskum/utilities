#! /bin/bash

latexfile=$1
latexfileorig=$latexfile
latexfile=${latexfile%%.*}
latexfile=${latexfile##*/}

latexfile_dir=$(dirname `readlink -f $latexfileorig`)
root_dir=`readlink -f $latexfile_dir/../../`

outfile=$latexfile

[ ! -f "$latexfileorig" ] && exit
[ -f "$outfile" ] && [ "$outfile" -nt "$latexfileorig" ] && exit

pdflatex "\def\MainFolder{$root_dir}\input{$latexfileorig}"

rm *aux *log *snm *nav *out *toc *png
mv $latexfile.pdf $latexfile_dir/$latexfile.pdf

if [[ $2 == "png" ]]
then
    pdftoppm -r 300 $latexfile.pdf $latexfile -png
fi
