clear()
fname = 'jet.json';
fid = fopen(fname); 
raw = fread(fid,inf);
str = char(raw'); 
fclose(fid); 
val = jsondecode(str);

argb_val = val.RGBPoints;
rgb0 = [];
rgb1 = [];
rgb2 = [];
for index = 1:length(argb_val)
    if rem(index, 4) == 0
        rgb0(end+1) = argb_val(index-0);
        rgb1(end+1) = argb_val(index-1);
        rgb2(end+1) = argb_val(index-2);
    end
end

rgb_val = [rgb0', rgb1', rgb2'];

surf(peaks)
colormap(rgb_val)
colorbar
% rgbplot(rgb_val)
